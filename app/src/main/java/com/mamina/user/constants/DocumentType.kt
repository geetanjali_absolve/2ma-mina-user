package com.mamina.user.constants

/**
 * Created by Rohit Singh on 18,March,2021
 * Absolve Tech company,
 * Mohali, India.
 */

// below class is for sign up purpose
object DocumentType {
    /*val listOfDocument: ArrayList<Document>
        get() {
            listOfDocument.addAll(arrayListOf(Document("ID Book", "1"), Document("ID Card", "2"), Document("Passport", "3"), Document("License", "4")))
        }*/

    val list = arrayListOf<Document>(
            Document("ID Book", "1"), // show 1 field
            Document("ID Card", "2"), // show 2 fields
            Document("Passport", "3"), // show 1 field
            Document("License", "4") // show 1 field
    )
}

data class Document(val title: String, val value: String)