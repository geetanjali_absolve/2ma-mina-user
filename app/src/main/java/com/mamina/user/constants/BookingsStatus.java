package com.mamina.user.constants;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Retention(RetentionPolicy.SOURCE)
@IntDef({BookingsStatus.ASSIGNED, BookingsStatus.UNASSIGNED, BookingsStatus.PAST})
public @interface BookingsStatus {
    int ASSIGNED = 2;
    int UNASSIGNED = 1;
    int PAST = 6;
}
