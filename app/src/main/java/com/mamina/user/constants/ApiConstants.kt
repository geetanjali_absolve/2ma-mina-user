package com.mamina.user.constants

import com.mamina.user.BuildConfig

/*
 * Created by Rohit Singh on 09,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
object ApiConstants {
    // below is the API base URL
    const val BASE_URL ="http://2mamina-dev.us-west-2.elasticbeanstalk.com/"

    const val BASE_FILE = "http://2mamina-dev.us-west-2.elasticbeanstalk.com/"
    const val BASE_FILE1 = "http://2mamina-dev.us-west-2.elasticbeanstalk.com/"

    // login URLs
    const val LOGIN_URL: String = "api/Auth/UserLogin"

    // individual sign up
    const val INDIVIDUAL_SIGN_UP: String = "api/Auth/RegisterIndividualUser"

    // individual sign up
    const val BUSINESS_SIGN_UP: String = "api/Auth/RegisterBusinessUser"

    // resend phone otp
    const val RESEND_PHONE_OTP: String = "api/Auth/ResendPhoneOtp"

    // resend email otp
    const val RESEND_EMAIL_OTP: String = "/api/Auth/ResendEmailCode"

    // verify phone otp
    const val VERIFY_PHONE: String = "api/Auth/VerifyPhone"

    // verify phone otp
    const val VERIFY_EMAIL: String = "/api/Auth/VerifyEmail"

    // forget password
    const val FORGET_PASSWORD: String = "api/Auth/ForgotPassword"

    // reset password
    const val RESET_PASSWORD: String = "/api/Auth/ResetPassword"

    // home category
    const val GET_PRICE_DETAIL: String = "api/Request/GetTypePriceDetails"

    // distance between latitude longitude and time
    const val REQUEST_DISTANCE_PRICE: String = "api/Request/RequestDistanceLatLon"

    // request parcel delivery
    const val REQUEST_DELIVERY = "/api/Request/DeliveryRequest"
    const val REQUEST_PARCEL = "/api/Request/ParcelDetails"
    const val REQUEST_PARCEL_DELIVERY = "/api/Request/RequestDeliveryByUser"

    //get user orders
    const val GET_USER_ORDERS = "/api/Orders/GetUserOrders"

    //cancel user order
    const val CANCEL_USER_ORDER = "/api/Orders/OrderCancelByUser"

    //get delivery status
    const val GET_REQUEST_DETAILS = "/api/Request/GetRequestDetails"

    // get user notification
    const val GET_USER_NOTIFICATION = "/api/Notification/GetUserNotifications"

    // delete user notification
    const val DELETE_USER_NOTIFICATION = "/api/Notification/DeleteUserNotification"

    //get individual user profile
    const val GET_INDIVIDUAL_PROFILE = "/api/Admin/GetIndiviualUserInfo"

    //update individual user profile
    const val UPDATE_INDIVIDUAL_PROFILE = "/api/Admin/UpdateIndividualUserInfo"

    //change password
    const val CHANGE_PASSWORD = "/api/Auth/ChangePassword"

    //rate driver
    const val RATE_DELIVERY = "/api/Driver/AddDriverRatings"

    //site policy
    const val SITE_POLICY = "/api/Admin/SitePolicy"

    //terms condition
    const val TERMS_CONDITIONS = "/api/Admin/TermsConditions"

    /* do not touch the below keys */

    const val KEY_CONTENT_TYPE = "Content-Type"
    const val KEY_ACCEPT = "Accept"
    const val KEY_AUTHORIZATION = "Authorization"

    const val VAL_CONTENT_TYPE = "application/json"
    const val VAL_ACCEPT = "application/x-www-form-urlencoded"
}