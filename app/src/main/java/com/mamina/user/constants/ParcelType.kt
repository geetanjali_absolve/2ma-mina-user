package com.mamina.user.constants

/**
 * Created by Rohit Singh on 26,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
object ParcelType {
    const val NORMAL_DELIVERY = 495
    const val EXPRESS_DELIVERY = 349
    const val BAKKIE_DELIVERY = 978
}