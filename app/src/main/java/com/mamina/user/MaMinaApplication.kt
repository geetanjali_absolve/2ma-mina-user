package com.mamina.user

import android.app.Application
import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.ktx.messaging
import com.mamina.WebService
import com.mamina.user.constants.PayPalConstants.PAYPAL_CLIENT_ID
import com.mamina.user.network.ServiceGenerator
import com.mamina.user.utils.SharedPrefsHelper
import com.paypal.checkout.PayPalCheckout
import com.paypal.checkout.config.CheckoutConfig
import com.paypal.checkout.config.Environment
import com.paypal.checkout.config.SettingsConfig
import com.paypal.checkout.createorder.CurrencyCode
import com.paypal.checkout.createorder.UserAction

/**
 * Created by Rohit Singh on 16,March,2021
 * Absolve Tech company,
 * Mohali, India.
 */
class MaMinaApplication: Application() {
    private var sharedPrefs: SharedPrefsHelper? = null

    override fun onCreate() {
        super.onCreate()
//        FacebookSdk.sdkInitialize(this)

        sharedPrefs = (applicationContext as MaMinaApplication).getSharedPrefInstance()

        Firebase.messaging.isAutoInitEnabled = true
        Firebase.analytics.setAnalyticsCollectionEnabled(true)

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(
                        "Firebase Registration:",
                        "Fetching FCM registration token failed",
                        task.exception
                )
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result

            // Log and toast
            //val msg = getString(R.string.msg_token_fmt, token)
            Log.d("Token Message:", token.toString())
            sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_DEVICE_ID, token.toString())?.build()
            //Toast.makeText(baseContext, token, Toast.LENGTH_SHORT).show()
        })

        PayPalCheckout.setConfig(
                checkoutConfig = CheckoutConfig(
                        application = this,
                        clientId = PAYPAL_CLIENT_ID,
                        environment = Environment.SANDBOX,
                        returnUrl = "${BuildConfig.APPLICATION_ID}://paypalpay",
                        currencyCode = CurrencyCode.USD,
                        userAction = UserAction.PAY_NOW,
                        settingsConfig = SettingsConfig(
                                loggingEnabled = true,
                                shouldFailEligibility = false
                        )
                )
        )
    }

    fun getWebServiceInstance(): WebService {
        return getRetrofitInstance().createService(WebService::class.java)
    }
    // @use DI
    fun getRetrofitInstance(): ServiceGenerator {
        return ServiceGenerator.getInstance(this)
    }

    // @use DI
    fun getSharedPrefInstance(): SharedPrefsHelper {
        return SharedPrefsHelper.getInstance(this)
    }
}