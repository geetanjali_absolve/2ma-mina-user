package com.mamina.user.ui.bookings.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BookingInfoList (
        @SerializedName("orderId") var orderId: Int,
        @SerializedName("driverId") var driverId: Int,
        @SerializedName("request") var request: RequestInfo,
        @SerializedName("senderName") var senderName: String,
        @SerializedName("senderPhoneNo") var senderPhoneNo: String,
        @SerializedName("deliveryType") var deliveryType: String,
        @SerializedName("price") var price: Int,
        @SerializedName("parcelName") var parcelName: String,
        @SerializedName("parcelNotes") var parcelNotes: String,
        @SerializedName("parcelImgBefore") var parcelImgBefore: String,
        @SerializedName("parcelImgAfter") var parcelImgAfter: String,
        @SerializedName("orderCreatedDate") var orderCreatedDate: String,
        @SerializedName("orderCreatedTime") var orderCreatedTime: String,
        @SerializedName("orderStatus") var orderStatus: String,
        @SerializedName("lastRequestStatusId") var lastRequestStatusId: Int,
        @SerializedName("lastRequestStatus") var lastRequestStatus: String,
        @SerializedName("isDriverRated") var isDriverRated: Boolean,
) : Parcelable