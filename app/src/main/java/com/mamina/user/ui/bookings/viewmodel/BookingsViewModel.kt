package com.mamina.user.ui.bookings.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.mamina.WebService
import com.mamina.user.MaMinaApplication
import com.mamina.user.models.response.WebResponse
import com.mamina.user.ui.bookings.model.BookingInfoList
import com.mamina.user.ui.bookings.model.BookingsResponse
import com.mamina.user.ui.bookings.repository.AssignedBookingsRepository

class BookingsViewModel(application: Application) : AndroidViewModel(application) {
    private var webService: WebService
    var isLoading: MutableLiveData<Boolean>
    var resultBookingsAssigned: MutableLiveData<WebResponse<BookingsResponse>>
    var resultBookingsUnassigned: MutableLiveData<WebResponse<BookingsResponse>>
    var resultBookingsPast: MutableLiveData<WebResponse<BookingsResponse>>
    var headerMap: HashMap<String, String>
    var isListEmpty: MutableLiveData<Boolean>
    var listOfAssignedBookings: MutableLiveData<MutableList<BookingInfoList>>
    var listOfUnassignedBookings: MutableLiveData<MutableList<BookingInfoList>>
    var listOfPastBookings: MutableLiveData<MutableList<BookingInfoList>>
    private var config: PagedList.Config
    private lateinit var itemDataSourceFactory: AssignedBookingsRepository.BookingsDataSourceFactory
    private lateinit var liveDataSource: LiveData<AssignedBookingsRepository>
    lateinit var bookingsPagedList: LiveData<PagedList<BookingInfoList>>

    init {
        webService = (application as MaMinaApplication).getWebServiceInstance()
        isLoading = MutableLiveData()
        headerMap = HashMap()
        isListEmpty = MutableLiveData()
        resultBookingsAssigned = MutableLiveData()
        resultBookingsUnassigned = MutableLiveData()
        resultBookingsPast = MutableLiveData()
        listOfAssignedBookings = MutableLiveData()
        listOfUnassignedBookings = MutableLiveData()
        listOfPastBookings = MutableLiveData()
        config = PagedList.Config.Builder()
                .setEnablePlaceholders(true)
                .setInitialLoadSizeHint(10)
                .setPageSize(10)
                .build()
    }

    fun fetchBookings(requestStatusId: Int) {
        itemDataSourceFactory = AssignedBookingsRepository.BookingsDataSourceFactory(webService, headerMap, requestStatusId)
        liveDataSource = itemDataSourceFactory.userLiveDataSource
        bookingsPagedList = LivePagedListBuilder(itemDataSourceFactory, config)
                .setBoundaryCallback(object : PagedList.BoundaryCallback<BookingInfoList>() {
                    override fun onZeroItemsLoaded() {
                        super.onZeroItemsLoaded()
                        // Handle empty initial load here
                        isListEmpty.value = true
                        isLoading.value = false
                    }

                    override fun onItemAtEndLoaded(itemAtEnd: BookingInfoList) {
                        super.onItemAtEndLoaded(itemAtEnd)
                        // Here you can listen to last item on list

                    }

                    override fun onItemAtFrontLoaded(itemAtFront: BookingInfoList) {
                        super.onItemAtFrontLoaded(itemAtFront)
                        // Here you can listen to first item on list
                        isLoading.value = false
                    }
                })
                .build()
    }


/*
    @ExperimentalCoroutinesApi
    fun fetchUserBookings(@BookingsStatus type: Int, requestStatusId: Int, page: Int) {
        when (type) {
            BookingsStatus.ASSIGNED -> {
                viewModelScope.launch {
                    repository.fetchAssignedBookings(headerMap, requestStatusId, page, listOfAssignedBookings)
                            .onStart {
                                */
/* emit loading state *//*

                                isLoading.value = true
                            }
                            .onCompletion {
                                //isLoading.value = false
                            }
                            .catch { exception ->
                                */
/* emit error state *//*

                                resultBookingsAssigned.value =
                                        WebResponse(Status.OTHER_ERROR, null, exception.message)
                            }
                            .collect {

                            }
                }
            }
            BookingsStatus.UNASSIGNED -> {
                viewModelScope.launch {
                    repository.fetchAssignedBookings(headerMap, requestStatusId, page, listOfUnassignedBookings)
                            .onStart {
                                */
/* emit loading state *//*

                                isLoading.value = true
                            }
                            .onCompletion {
                                // isLoading.value = false
                            }
                            .catch { exception ->
                                */
/* emit error state *//*

                                resultBookingsAssigned.value =
                                        WebResponse(Status.OTHER_ERROR, null, exception.message)
                            }
                            .collect {

                            }
                }
            }
            BookingsStatus.PAST -> {
                viewModelScope.launch {
                    repository.fetchAssignedBookings(headerMap, requestStatusId, page, listOfPastBookings)
                            .onStart {
                                */
/* emit loading state *//*

                                isLoading.value = true
                            }
                            .onCompletion {
                                //  isLoading.value = false
                            }
                            .catch { exception ->
                                */
/* emit error state *//*

                                resultBookingsAssigned.value =
                                        WebResponse(Status.OTHER_ERROR, null, exception.message)
                            }
                            .collect {

                            }
                }
            }
        }
    }
*/
}