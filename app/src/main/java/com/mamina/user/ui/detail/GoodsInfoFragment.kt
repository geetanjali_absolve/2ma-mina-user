package com.mamina.user.ui.detail

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.ContentResolver
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.mamina.user.MaMinaApplication
import com.mamina.user.MainActivity
import com.mamina.user.R
import com.mamina.user.constants.ApiConstants
import com.mamina.user.databinding.GoodsInfoFragmentBinding
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.ui.utils.MessageType
import com.mamina.user.utils.*
import com.yalantis.ucrop.UCrop
import kotlinx.android.synthetic.main.goods_info_fragment.*
import java.io.File


class GoodsInfoFragment : BaseFragment() {
    private var _binding: GoodsInfoFragmentBinding? = null
    private val binding get() = _binding!!
    private var imageBeforePacking: String = ""
    private var imageAfterPacking: String = ""
    private var imageType: String = ""
    private var parcelName: String = ""
    private var parcelNotes: String = ""
    private lateinit var goodsInfoViewModel: GoodsInfoViewModel
    private var sharedPrefs: SharedPrefsHelper? = null
    var showPictureDialog: ShowImageDialog? = null
    private val REQUEST_IMAGE_CAPTURE: Int = 200
    private val REQUEST_GALLERY_IMAGE: Int = 210
    private var fileName: String? = null
    private var snackBarPermission: Snackbar? = null
    private var setBitmapMaxWidthHeight: Boolean = false
    private val ASPECT_RATIO_X = 1
    private var ASPECT_RATIO_Y: Int = 1
    private var bitmapMaxWidth: Int = 1000
    private var bitmapMaxHeight: Int = 1000
    private val IMAGE_COMPRESSION = 80


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        _binding = GoodsInfoFragmentBinding.inflate(inflater, container, false)
        val view = binding.root

        sharedPrefs = (context?.applicationContext as MaMinaApplication).getSharedPrefInstance()
        goodsInfoViewModel = ViewModelProvider(this).get(GoodsInfoViewModel::class.java)
        showPictureDialog = ShowImageDialog(requireActivity())

        initObservers(view)

        binding.withoutPackingLayout1.setOnClickListener {
            imageType = Constants.withoutPacking
            if (Build.VERSION.SDK_INT >= 23) {
                Dexter.withActivity(activity)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                        .withListener(multiplePermissionsListener).check()
            } else {
                openImagePopupMenu()
            }
        }

        binding.withPackingLayout1.setOnClickListener {
            imageType = Constants.withPacking
            if (Build.VERSION.SDK_INT >= 23) {
                Dexter.withActivity(activity)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                        .withListener(multiplePermissionsListener).check()
            } else {
                openImagePopupMenu()
            }
        }


        binding.goodsInfoSubmitBtn.setOnClickListener {
            parcelName = binding.goodsInfoParcelCategorySpinner.text.toString()
            parcelNotes = binding.goodsInfoParcelSubcategorySpinner.text.toString()

            if (TextUtils.isEmpty(parcelName) || TextUtils.isEmpty(parcelNotes)
                    || imageAfterPacking == "" || imageBeforePacking == "") {
                Snackbar.make(binding.goodsInfoSubmitBtn, getString(R.string.required_alert),
                        Snackbar.LENGTH_SHORT).show()
            } else {
                val accessToken = sharedPrefs?.read(SharedPrefsHelper.KEY_ACCESS_TOKEN, "").toString()
                val headerMap = HashMap<String, String>()
                headerMap[ApiConstants.KEY_AUTHORIZATION] = "Bearer $accessToken"

                val requestId = CommonUtils.prepareTextPart("0")
                val parcel_name = CommonUtils.prepareTextPart(parcelName)
                val parcel_notes = CommonUtils.prepareTextPart(parcelNotes)
                val imgBefore = CommonUtils.prepareFilePart(imageBeforePacking, "ImgBeforePacking")
                val imgAfter = CommonUtils.prepareFilePart(imageAfterPacking, "ImgAfterPacking")
                val senderLat = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.SenderLat)
                val senderLong = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.SenderLong)
                val senderPlaceId = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.SenderPlaceId)
                val senderAddress = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.SenderAddress)
                val receiverName = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.ReceiverName)
                val receiverEmail = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.ReceiverEmail)
                val receiverPlaceId = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.ReceiverPlaceId)
                val receiverAddress = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.ReceiverAddress)
                val dialCode = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.DialCode)
                val phone = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.ReceiverMobileNumber)
                val receiverLat = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.ReceiverLat)
                val receiverLong = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.ReceiverLong)
                val deliveryTypeId = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.DeliveryTypeId.toString())
                val deliveryTime = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.TotalDeliveryTime)
                val deliveryDistance = CommonUtils.prepareTextPart((activity as MainActivity).orderRequest.TotalDeliveryDistance)

                binding.parcelLoading.root.visibility = View.VISIBLE
                goodsInfoViewModel.requestParcelDelivery(headerMap, requestId, parcel_name, parcel_notes, senderLat,
                        senderLong, senderPlaceId, senderAddress, receiverName, receiverEmail, receiverPlaceId, receiverAddress, dialCode, phone,
                        receiverLat, receiverLong, deliveryTypeId, deliveryTime, deliveryDistance, imgBefore, imgAfter)
            }
        }

        return view
    }

    private fun initObservers(view: View) {
        goodsInfoViewModel.resultRequestParcelDelivery.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    binding.parcelLoading.root.visibility = View.GONE
                    showSuccessRequestDialog(it.data?.message, view)
                }
                Status.AUTHORISATION -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.AUTHORISATION_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.BAD_REQUEST -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.NOT_FOUND -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.OTHER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.SERVER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                else -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
            }
        })
    }

    companion object {
        private val TAG = GoodsInfoFragment::class.java.simpleName

         fun newInstance(): GoodsInfoFragment {
            return GoodsInfoFragment()
        }
    }

    private var multiplePermissionsListener: MultiplePermissionsListener =
            object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        openImagePopupMenu()
                    }
                    if (report.isAnyPermissionPermanentlyDenied) {
                        snackBarPermission = Snackbar.make(
                                view!!,
                                getString(R.string.title_camera_and_storage_access),
                                Snackbar.LENGTH_INDEFINITE
                        )
                                .setAction(getString(R.string.action_settings)) {
                                    // open system setting screen ...
                                    val intent = Intent()
                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                    val uri = Uri.fromParts("package", activity?.packageName, null)
                                    intent.data = uri
                                    activity?.startActivity(intent)
                                }
                        snackBarPermission?.show()
                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                        permissions: MutableList<PermissionRequest>,
                        token: PermissionToken,
                ) {
                    token.continuePermissionRequest()
                }
            }

    private fun openImagePopupMenu() {
        val dialog = Dialog(requireContext())
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Choose Image Source")
        builder.setItems(arrayOf<CharSequence?>("Camera", "Gallery")) { dialogInterface, i ->
            when (i) {
                0 -> openCamera()
                1 -> openGallery()
            }
        }
        builder.show()
        dialog.dismiss()
    }

    private fun openCamera() {
        fileName = "IMG_" + System.currentTimeMillis().toString() + ".jpg"
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath())
        if (activity?.packageManager?.let { intent.resolveActivity(it) } != null) {
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
        }
    }

    private fun getCacheImagePath(): Uri? {
        val path = File(activity?.externalCacheDir, "camera")
        if (!path.exists()) path.mkdirs()
        val image = File(path, fileName)
        return activity?.let {
            FileProvider.getUriForFile(
                    it,
                    "com.mamina.user.provider",
                    image
            )
        }
    }

    private fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, REQUEST_GALLERY_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_IMAGE_CAPTURE -> if (resultCode == Activity.RESULT_OK) {
                cropImage(getCacheImagePath())
            }
            REQUEST_GALLERY_IMAGE -> if (resultCode == Activity.RESULT_OK) {
                val imageUri = data?.data
                cropImage(imageUri)
            }
            UCrop.REQUEST_CROP -> if (resultCode == Activity.RESULT_OK) {
                handleUCropResult(data)
            }
            UCrop.RESULT_ERROR -> {
                val cropError = UCrop.getError(data!!)
                //  Log.e("TAG", "Crop error: $cropError")
                showSnackbarError("Crop error: $cropError")
            }
        }
    }

    private fun cropImage(sourceUri: Uri?) {
        val destinationUri = Uri.fromFile(
                File(
                        activity?.cacheDir,
                        queryName(activity?.contentResolver!!, sourceUri!!)
                )
        )
        val options = UCrop.Options()
        options.setCompressionQuality(IMAGE_COMPRESSION)
        options.setToolbarColor(ContextCompat.getColor(requireContext(), R.color.primary_color))
        options.setStatusBarColor(ContextCompat.getColor(requireContext(), R.color.primary_color))
        options.setActiveWidgetColor(ContextCompat.getColor(requireContext(),
                R.color.primary_color))
        options.withAspectRatio(ASPECT_RATIO_X.toFloat(), ASPECT_RATIO_Y.toFloat())
        if (setBitmapMaxWidthHeight) options.withMaxResultSize(bitmapMaxWidth, bitmapMaxHeight)
        UCrop.of(sourceUri, destinationUri).withOptions(options).start(requireActivity(), this)
    }

    private fun handleUCropResult(data: Intent?) {
        if (data == null) {
            return
        }
        val resultUri = UCrop.getOutput(data)
        Log.e("TAG", "Crop : $resultUri")

        if (imageType == Constants.withoutPacking) {
            imageBeforePacking = resultUri?.path.toString()
            binding.withoutPackingImg1Plus.setImageURI(resultUri)
        } else {
            imageAfterPacking = resultUri?.path.toString()
            binding.withPackingImg1Plus.setImageURI(resultUri)
        }
    }

    private fun queryName(resolver: ContentResolver, uri: Uri): String? {
        val returnCursor: Cursor = resolver.query(uri, null, null, null, null)!!
        val nameIndex: Int = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        val name: String = returnCursor.getString(nameIndex)
        returnCursor.close()
        return name
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
        if (snackBarPermission != null) {
            if (snackBarPermission!!.isShown) {
                snackBarPermission?.dismiss()
            }
        }
    }

    override fun getRootView(): View? {
        return cl_parent
    }

    fun showSuccessRequestDialog(message: String?, view: View) {
        val builder = androidx.appcompat.app.AlertDialog.Builder(requireContext())
        //set title for alert dialog
        builder.setTitle(message)
        //set message for alert dialog
//        builder.setMessage(message)

        //performing positive action
        builder.setPositiveButton("Ok") { dialogInterface, which ->
            dialogInterface.dismiss()
            Navigation.findNavController(view).navigate(R.id.action_nav_goods_info_to_nav_home)
        }
        // Create the AlertDialog
        val alertDialog: androidx.appcompat.app.AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

}