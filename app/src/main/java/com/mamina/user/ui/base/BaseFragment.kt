package com.mamina.user.ui.base

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.drawable.Drawable
import android.media.ExifInterface
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.mamina.user.MaMinaApplication
import com.mamina.user.R
import com.mamina.user.ui.InitialActivity
import com.mamina.user.ui.utils.MessageType
import com.mamina.user.utils.SharedPrefsHelper
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Rohit Singh on 11,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
abstract class BaseFragment : Fragment() {
    private var sharedPrefs: SharedPrefsHelper? = null
    private var mActivity: BaseActivity? = null
    private var loadingView: View? = null
    public val CAMERA_REQUEST_CODE = 42389
    public val GALLERY_REQUEST_CODE = 324
    lateinit var imageFilePath: String

    fun isNetworkConnected(): Boolean {
//return baseActivity != null && baseActivity.
        return true
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity) {
            mActivity = context
        }

        sharedPrefs = (context.applicationContext as MaMinaApplication).getSharedPrefInstance()
    }

    fun setLoadingView(view: View) {
        loadingView = view
    }

    fun showLoading() {
        loadingView?.visibility = View.VISIBLE
    }

    fun hideLoading() {
        loadingView?.visibility = View.GONE
    }
    fun hideKeyboard(view: View) {
        val inputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showSnackbarMessage(message: String, @ColorRes colorId: Int = R.color.primary_color) {
        val snackBar = Snackbar.make(view?.rootView!!, message, Snackbar.LENGTH_LONG)
        snackBar.view.setBackgroundColor(ContextCompat.getColor(requireActivity(), colorId))
        snackBar.show()
    }

    fun showSnackbarError(message: String,@ColorRes colorId: Int = R.color.danger_color) {
        val snackBar = Snackbar.make(view?.rootView!!, message, Snackbar.LENGTH_LONG)
        snackBar.view.setBackgroundColor(ContextCompat.getColor(requireActivity(), colorId))
        snackBar.show()
    }

    fun showSnackBar(messageType: Int, message: String?, context: Context) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        /*val snackbar = Snackbar.make(requireView(), "", 3000)
        // inflate the custom snackbar view
        val maminaSnackBarView = layoutInflater.inflate(R.layout.mamina_snackbar_layout, null)
        val snackBarTitleTv = maminaSnackBarView.findViewById<TextView?>(R.id.snackbar_title_tv)
        val snackBarDescriptionTv = maminaSnackBarView.findViewById<TextView?>(R.id.snackbar_desc_tv)
        val snackBarImg = maminaSnackBarView.findViewById<ImageView?>(R.id.snackbar_image)
        snackBarImg.setImageDrawable(getDrawable(messageType))
        snackBarTitleTv.text = getTitle(messageType)
        snackBarDescriptionTv.text = message

        // change the layout of the snackbar
        val snackbarLayout = snackbar.view

        // set padding of all corners as 0
        snackbarLayout.setPadding(0, 0, 0, 0)

        // set the snackbar at top of the screen
        *//*val snackBarParams = snackbar.view.layoutParams as FrameLayout.LayoutParams //FrameLayout.LayoutParams
        snackBarParams.gravity = Gravity.TOP
        snackBarParams.setMargins(32, 56, 32, 16)
        snackbar.view.layoutParams = snackBarParams*//*
        //        snackbar.getView().setVisibility(View.GONE);
//        snackbarLayout.addView(maminaSnackBarView, 0)
        snackbar.show()*/

    }


    fun showUnauthorizedUserErrorDialog(){
        val builder = androidx.appcompat.app.AlertDialog.Builder(requireContext())
        //set title for alert dialog
        //  builder.setTitle(R.string.dialogTitle)
        //set message for alert dialog
        builder.setMessage(R.string.alert_session_expired)
        builder.setIcon(android.R.drawable.ic_dialog_alert)

        //performing positive action
        builder.setPositiveButton("Ok"){dialogInterface, which ->
            dialogInterface.dismiss()
            sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_IS_SIGN_IN, false)?.build()
            val intent = Intent(activity, InitialActivity::class.java)
            startActivity(intent)
            activity?.finish()
        }
        // Create the AlertDialog
        val alertDialog: androidx.appcompat.app.AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    fun showAlertDialog(context: Context, message: String) {
        // Create an alert builder
        // Create an alert builder
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setTitle("")

        // set the custom layout
        val customLayout: View = layoutInflater
                .inflate(
                        R.layout.alert_dialog_layout,
                        null)
        builder.setView(customLayout)

        val textView = customLayout.findViewById<TextView>(R.id.dialog_title)
        textView.text = message

        // add a button
        builder
                .setPositiveButton(
                        "OK"
                ) { dialog, which -> // send data from the
                    // AlertDialog to the Activity
                    dialog.dismiss()
                }

        // create and show
        // the alert dialog
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }



    abstract fun getRootView(): View?

    // snackbar title color
    fun getTextColor(messageType: Int): Int {
        when (messageType) {
            MessageType.SUCCESS -> ResourcesCompat.getColor(resources, R.color.success_text_color, null)
            MessageType.INFO -> ResourcesCompat.getColor(resources, R.color.info_color, null)
            MessageType.ERROR -> ResourcesCompat.getColor(resources, R.color.danger_color, null)
        }
        return 0
    }

    // snackbar icon
    fun getDrawable(messageType: Int) {
        var drawable: Drawable
        when (messageType) {
            MessageType.SUCCESS -> ResourcesCompat.getDrawable(resources, R.drawable.ic_check_box, null)
            MessageType.INFO -> ResourcesCompat.getDrawable(resources, R.drawable.ic_info, null)
            MessageType.ERROR -> ResourcesCompat.getDrawable(resources, R.drawable.maps_sv_error_icon, null)
        }
    }

    // snackbar
    fun getTitle(messageType: Int): String {
        when (messageType) {
            MessageType.SUCCESS -> return "Success"
            MessageType.INFO -> return "Info"
            MessageType.ERROR -> return "Error"
        }
        return "Success"
    }

    fun getPickImageChooserIntent() {
        val dialog = Dialog(requireContext())
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Choose Image Source")
        builder.setItems(arrayOf<CharSequence?>("Gallery", "Camera")) { dialogInterface, i ->
            when (i) {
                0 -> {
                    val galleryIntent = Intent(Intent.ACTION_GET_CONTENT)
                    galleryIntent.type = "image/*"
                    val chooser = Intent.createChooser(galleryIntent, "Choose a picture")
                    startActivityForResult(chooser, GALLERY_REQUEST_CODE)
                }
                1 ->                         // intent for camera
                    try {
                        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE)
//                        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//                        if (cameraIntent.resolveActivity(requireActivity().packageManager) != null) {
//                            var photoFile: File? = null
//                            try {
//                                photoFile = createImageFile(requireActivity())
//                            } catch (e: IOException) {
//                                e.printStackTrace()
//                            }
//
//                            if (photoFile != null) {
//                                val photoURI = FileProvider.getUriForFile(requireActivity(), "com.mamina.user.fileprovider", photoFile)
//                                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
//                                startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE)
//                            }
//                        }
                    } catch (securityException: SecurityException) {
                        Toast.makeText(activity, "Please enable the camera permission to proceed", Toast.LENGTH_SHORT).show()
                        checkPermission("Base Fragment")
                    }
            }
        }
        builder.show()
        dialog.dismiss()
    }


    fun checkPermission(tag: String) {
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) requestPermissions(arrayOf<String?>(Manifest.permission.CAMERA), CAMERA_REQUEST_CODE) else {
            Log.d(tag, "checkPermission: Camera permission granted")
            getPickImageChooserIntent()
        }

    }

    @Throws(IOException::class)
    fun createImageFile(activity: Activity): File {

        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName: String = "JPEG_" + timeStamp + "_"
        val storageDir: File? = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if (!storageDir?.exists()!!) storageDir.mkdirs()
        val imageFile = File.createTempFile(imageFileName, ".jpg", storageDir)
        imageFilePath = imageFile.absolutePath
        return imageFile
    }

    fun getFilename(): String {
        val file = File(Environment.getExternalStorageDirectory().path, "MyFolder/Images")
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.absolutePath + "/" + System.currentTimeMillis() + ".jpg"

    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
                source, 0, 0, source.width, source.height,
                matrix, true
        )
    }

    fun rotateImages(file: String, bitmap: Bitmap): Bitmap? {

        val ei = ExifInterface(file)
        val orientation = ei.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
        )
        var rotatedBitmap: Bitmap? = null
        when (orientation) {

            ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap = rotateImage(bitmap, 90F)

            ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap = rotateImage(bitmap, 180F)

            ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap = rotateImage(bitmap, 270F)

            ExifInterface.ORIENTATION_NORMAL -> bitmap
            else -> rotatedBitmap = bitmap
        }
        return rotatedBitmap
    }
}