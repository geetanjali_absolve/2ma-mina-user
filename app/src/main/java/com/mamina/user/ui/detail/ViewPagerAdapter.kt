package com.mamina.user.ui.detail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mamina.user.R
import com.mamina.user.constants.ApiConstants
import java.util.*


internal class ViewPagerAdapter(context: Context, images: ArrayList<String>) : PagerAdapter() {
    // Context object
    var context: Context
    // Array of images
    var images: ArrayList<String>

    override fun getCount(): Int {
        // return the number of images
        return images.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as LinearLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        // inflating the item.xml
        val itemView: View = LayoutInflater.from(context).inflate(R.layout.item_pager, container, false)

        // referencing the image view from the item.xml file
        val imageView: ImageView = itemView.findViewById(R.id.imageViewMain) as ImageView

        // setting the image in the imageView

        Glide.with(context)
                .load(ApiConstants.BASE_FILE + images[position])
                .centerInside()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView)
        //imageView.setImageResource(images[position])

        // Adding the View
        Objects.requireNonNull(container).addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }

    // Viewpager Constructor
    init {
        this.context = context
        this.images = images
    }
}