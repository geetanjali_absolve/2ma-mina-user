package com.mamina.user.ui.bookings.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mamina.user.R
import com.mamina.user.ui.bookings.model.BookingInfoList
import kotlinx.android.synthetic.main.item_bookings.view.*

class AssignedBookingAdapter(var type: String) : PagedListAdapter<BookingInfoList, AssignedBookingAdapter.MiViewHolder>(ITEM_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MiViewHolder {
        return MiViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.item_bookings,
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: MiViewHolder, pos: Int) {
        var item = getItem(pos)
        val viewHolder = holder
        item?.let { itt ->
            viewHolder.bindView(itt)
        }

        if (type == "Completed") {
            holder.itemView.img_arrow_forward.visibility = View.GONE
        } else {
            holder.itemView.img_arrow_forward.visibility = View.VISIBLE
        }

    }

    class MiViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindView(model: BookingInfoList) {

            itemView.tv_order_no.text = "Order No.${model.orderId}"
            itemView.tv_order_dateTime.text = "${model.orderCreatedDate}, ${model.orderCreatedTime}"
            itemView.tv_sender_address.text = model.request.senderAddress
            itemView.tv_receiver_address.text = model.request.receiverAddress
            itemView.tv_order_duration.text = model.request.totalDeliveryTime
            itemView.tv_order_distance.text = model.request.totalDeliveryDistance
            itemView.tv_order_type.text = model.deliveryType
            itemView.tv_order_status.text = model.orderStatus

            if (model.orderId == 0) {
                itemView.tv_order_dateTime.visibility = View.INVISIBLE
                itemView.tv_order_status.visibility = View.INVISIBLE
                itemView.img_arrow_forward.visibility = View.INVISIBLE
                itemView.tv_status_title.visibility = View.INVISIBLE
            } else {
                itemView.tv_order_dateTime.visibility = View.VISIBLE
                itemView.tv_order_status.visibility = View.VISIBLE
                itemView.img_arrow_forward.visibility = View.VISIBLE
                itemView.tv_status_title.visibility = View.VISIBLE
            }

//            if (model.isDriverRated) {
//                itemView.tv_ratings.visibility = View.GONE
//            } else {
//                itemView.tv_ratings.visibility = View.VISIBLE
//            }

            itemView.img_arrow_forward.setOnClickListener {
                val actionBookingsToOrderDetailFragment = BookingsFragmentDirections.actionNavBookingsToNavOrderDetail(model)
                actionBookingsToOrderDetailFragment.bookingInfoList = model
                Navigation.findNavController(it).navigate(actionBookingsToOrderDetailFragment)
            }

            itemView.tv_ratings.setOnClickListener {
                val actionPastBookingsToRatingsFragment = AssignedBookingsFragmentDirections.actionNavCompletedToNavRatings(model.orderId.toString(), model.driverId.toString())
//                it.findNavController().navigate(actionPastBookingsToRatingsFragment)
                Navigation.createNavigateOnClickListener(actionPastBookingsToRatingsFragment)
            }
        }
    }

    companion object {
        private val ITEM_COMPARATOR = object : DiffUtil.ItemCallback<BookingInfoList>() {
            override fun areItemsTheSame(
                    oldItem: BookingInfoList,
                    newItem: BookingInfoList
            ): Boolean = oldItem.orderId == newItem.orderId

            override fun areContentsTheSame(
                    oldItem: BookingInfoList,
                    newItem: BookingInfoList
            ): Boolean = newItem == oldItem
        }
    }

}