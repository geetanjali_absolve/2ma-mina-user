package com.mamina.user.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.mamina.user.R
import com.mamina.user.ui.base.BaseFragment
import kotlinx.android.synthetic.main.settings_fragment.*

class SettingsFragment : BaseFragment() {
    private lateinit var viewModel: SettingsViewModel

    override fun getRootView(): View? {
        return cl_root_settings
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SettingsViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.settings_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ll_change_password.setOnClickListener {
            Navigation.findNavController(view)
                    .navigate(R.id.action_nav_settings_to_nav_change_password)
        }

        ll_terms_condition.setOnClickListener {
            val actionNavSettingsToNavTerms = SettingsFragmentDirections.actionNavSettingsToTermsConditions("Terms")
            Navigation.findNavController(view)
                    .navigate(actionNavSettingsToNavTerms)
        }

        ll_privacy_policy.setOnClickListener {
            val actionNavSettingsToNavTerms = SettingsFragmentDirections.actionNavSettingsToTermsConditions("Privacy")
            Navigation.findNavController(view)
                    .navigate(actionNavSettingsToNavTerms)
        }
        llInvite.setOnClickListener {
            findNavController().navigate(SettingsFragmentDirections.actionNavSettingsToInviteFriends())
        }
        llSuport.setOnClickListener {
            findNavController().navigate(SettingsFragmentDirections.actionNavSettingsToSupport())
        }
    }

}