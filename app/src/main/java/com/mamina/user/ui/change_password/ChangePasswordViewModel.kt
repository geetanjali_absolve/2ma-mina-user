package com.mamina.user.ui.change_password

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.mamina.WebService
import com.mamina.user.MaMinaApplication
import com.mamina.user.models.response.GeneralResponse
import com.mamina.user.models.response.WebResponse
import com.mamina.user.repository.remote.InitialRepository
import com.mamina.user.ui.base.BaseViewModel
import com.mamina.user.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class ChangePasswordViewModel(application: Application) : BaseViewModel(application) {
    var webService: WebService = (application as MaMinaApplication).getWebServiceInstance()
    var initialRepository: InitialRepository = InitialRepository(webService)
    var resultChangePasswordMLiveData: MutableLiveData<WebResponse<GeneralResponse>> =
            MutableLiveData()

    @ExperimentalCoroutinesApi
    fun changePassword(
            header: Map<String, String>,
            oldPassword: String,
            newPassword: String,
    ) {
        val json = JsonObject()
        json.addProperty("oldPassword", oldPassword)
        json.addProperty("newPassword", newPassword)

        viewModelScope.launch {
            initialRepository.changePassword(
                    header, json)
                    .onStart {
                    }
                    .onCompletion {
                    }
                    .catch { exception ->
                        Log.i(TAG, "Exception:" + exception.message)
                        resultChangePasswordMLiveData.value =
                                WebResponse(Status.NOT_FOUND, null, exception.message)
                    }
                    .collect {
                        if (it.status)
                            resultChangePasswordMLiveData.value = WebResponse(Status.SUCCESS, it, null)
                        else
                            resultChangePasswordMLiveData.value =
                                    WebResponse(Status.SERVER_ERROR, null, it.message)
                    }
        }
    }

    companion object {
        private val TAG = ChangePasswordViewModel::class.java.simpleName
    }
}