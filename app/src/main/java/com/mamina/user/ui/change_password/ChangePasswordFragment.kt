package com.mamina.user.ui.change_password

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.mamina.user.MaMinaApplication
import com.mamina.user.R
import com.mamina.user.constants.ApiConstants
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.utils.SharedPrefsHelper
import com.mamina.user.utils.Status
import kotlinx.android.synthetic.main.change_password_fragment.*

class ChangePasswordFragment : BaseFragment() {
    private lateinit var viewModel: ChangePasswordViewModel
    val headerMap = HashMap<String, String>()
    private var sharedPrefs: SharedPrefsHelper? = null
    private var accessToken: String = ""

    override fun getRootView(): View? {
        return cl_root_change_password
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ChangePasswordViewModel::class.java)
        sharedPrefs = (context?.applicationContext as MaMinaApplication).getSharedPrefInstance()

        accessToken =
                sharedPrefs?.builder()?.read(SharedPrefsHelper.KEY_ACCESS_TOKEN, "").toString()
        headerMap[ApiConstants.KEY_AUTHORIZATION] = "Bearer $accessToken"

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.change_password_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setLoadingView(change_password_loading)

        initObserver()

        btn_change_password.setOnClickListener {
            hideKeyboard(it)
            if (TextUtils.isEmpty(old_password_edit_text.text.toString()) ||
                    TextUtils.isEmpty(new_password_edit_text.text.toString()) ||
                    TextUtils.isEmpty(confirm_password_edit_text.text.toString())
            ) {
                showSnackbarError("Please fill all the required fields!")
            } else if (confirm_password_edit_text.text.toString() != new_password_edit_text.text.toString()) {
                showSnackbarError("New Password and Confirm Password should be same!")
            } else {
                showLoading()
                viewModel.changePassword(headerMap,
                        old_password_edit_text.text.toString().trim(),
                        new_password_edit_text.text.toString().trim())
            }
        }

    }

    private fun initObserver() {

        viewModel.resultChangePasswordMLiveData.observe(viewLifecycleOwner, {
            hideLoading()
            when (it.status) {
                Status.SUCCESS -> {
                    Log.i("Change Pass Fragment", "Response:${it.data}")
                    showAlertDialog(requireContext(), it.data?.message!!)
                }
                Status.AUTHORISATION -> showSnackbarError(it.errorMsg.toString())
                Status.AUTHORISATION_ERROR -> showSnackbarError(it.errorMsg.toString())
                Status.BAD_REQUEST -> showSnackbarError(it.errorMsg.toString())
                Status.NOT_FOUND -> showSnackbarError(it.errorMsg.toString())
                Status.OTHER_ERROR -> showSnackbarError(it.errorMsg.toString())
                Status.SERVER_ERROR -> showSnackbarError(it.errorMsg.toString())
                else -> showSnackbarError(it.errorMsg.toString())
            }

        })
    }

}