package com.mamina.user.ui.signup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.mamina.user.R

class ChooseSignUpFragment : Fragment() {
    private lateinit var mViewModel: ChooseSignUpViewModel

    // buttons
    private lateinit var businessButton: Button
    private lateinit var individualButton: Button

    // click listener
    private val chooseSignUpToBusinessAction: View.OnClickListener? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.choose_sign_up_fragment, container, false)
        // initialisation
        businessButton = view.findViewById(R.id.choose_sign_up_business_btn)
        individualButton = view.findViewById(R.id.choose_sign_up_individual_btn)
        //        chooseSignUpToBusinessAction = Navigation.createNavigateOnClickListener(R.id.action_choose_sign_up_fragment_to_sign_up_fragment);
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel = ViewModelProvider(this).get(ChooseSignUpViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        businessButton.setOnClickListener {
            val action = ChooseSignUpFragmentDirections.actionChooseSignUpFragmentToSignUpFragment()
            action.isSignUpType = false
            Navigation.findNavController(view).navigate(action)
        }

        individualButton.setOnClickListener {
            val action = ChooseSignUpFragmentDirections.actionChooseSignUpFragmentToSignUpFragment()
            action.isSignUpType = true
            Navigation.findNavController(view).navigate(action)
        }
    }

    companion object {
        fun newInstance(): ChooseSignUpFragment {
            return ChooseSignUpFragment()
        }
    }
}