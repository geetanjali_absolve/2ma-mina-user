package com.mamina.user.ui.detail

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mamina.WebService
import com.mamina.user.MaMinaApplication
import com.mamina.user.models.response.DeliveryRequestResponse
import com.mamina.user.models.response.WebResponse
import com.mamina.user.repository.remote.InitialRepository
import com.mamina.user.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody

class GoodsInfoViewModel(application: Application) : AndroidViewModel(application) {
    private var initialRepository: InitialRepository
    private var webService: WebService
    var resultRequestParcelDelivery: MutableLiveData<WebResponse<DeliveryRequestResponse>>

    companion object {
        private val TAG = GoodsInfoViewModel::class.java.simpleName
    }

    init {
        webService = (application as MaMinaApplication).getWebServiceInstance()
        initialRepository = InitialRepository(webService)
        resultRequestParcelDelivery = MutableLiveData()
    }


    @ExperimentalCoroutinesApi
    fun requestParcelDelivery(
            header: Map<String, String>,
            requestId: RequestBody,
            parcelName: RequestBody,
            parcelNotes: RequestBody,
            senderLat: RequestBody,
            senderLong: RequestBody,
            senderPlaceId: RequestBody,
            senderAddress: RequestBody,
            receiverName: RequestBody,
            receiverEmail: RequestBody,
            receiverPlaceId: RequestBody,
            receiverAddress: RequestBody,
            dialCode: RequestBody,
            phone: RequestBody,
            receiverLat: RequestBody,
            receiverLong: RequestBody,
            deliveryType: RequestBody,
            deliveryTime: RequestBody,
            distance: RequestBody,
            imgBefore: MultipartBody.Part,
            imgAfter: MultipartBody.Part
    ) {
        viewModelScope.launch {
            initialRepository.requestParcelDelivery(header,
                    requestId, parcelName, parcelNotes, senderLat, senderLong, senderPlaceId, senderAddress, receiverName,
                    receiverEmail, receiverPlaceId, receiverAddress, dialCode, phone, receiverLat, receiverLong, deliveryType,
                    deliveryTime, distance, imgBefore, imgAfter
            )
                    .onStart {
//                        isLoading.value = true
//                        isViewEnable.value = false
                    }
                    .onCompletion {
//                        isLoading.value = false
//                        isViewEnable.value = true
                    }
                    .catch { exception ->
//                        / emit error state /
                        Log.i(GoodsInfoViewModel.TAG, "Exception:" + exception.message)
                        resultRequestParcelDelivery.value = WebResponse(Status.OTHER_ERROR, null, exception.message)
                    }
                    .collect {
                        Log.i(GoodsInfoViewModel.TAG, "requestParcelDelivery: $it")
                        resultRequestParcelDelivery.value = WebResponse(it.code, it, it.message)//WebResponse(it.code, it.data, it.message)
                    }
        }

    }

}