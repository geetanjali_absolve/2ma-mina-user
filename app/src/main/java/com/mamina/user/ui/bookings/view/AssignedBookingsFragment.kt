package com.mamina.user.ui.bookings.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.mamina.user.MaMinaApplication
import com.mamina.user.R
import com.mamina.user.constants.ApiConstants
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.ui.bookings.viewmodel.BookingsViewModel
import com.mamina.user.utils.SharedPrefsHelper
import kotlinx.android.synthetic.main.fragment_assigned_bookings.*

class AssignedBookingsFragment : BaseFragment() {
    private var sharedPrefs: SharedPrefsHelper? = null
    private lateinit var bookingsViewModel: BookingsViewModel
    private lateinit var assignedBookingAdapter: AssignedBookingAdapter
    private var requestId: Int = 0


    override fun getRootView(): View? {
        return cl_parent_assigned
    }

    companion object {
        fun newInstance() = AssignedBookingsFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sharedPrefs = (context.applicationContext as MaMinaApplication).getSharedPrefInstance()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        assignedBookingAdapter = AssignedBookingAdapter("Assigned")
        val accessToken = sharedPrefs?.builder()?.read(SharedPrefsHelper.KEY_ACCESS_TOKEN, "").toString()
        val headerMap = HashMap<String, String>()
        headerMap[ApiConstants.KEY_AUTHORIZATION] = "Bearer $accessToken"

        bookingsViewModel = ViewModelProvider(this).get(BookingsViewModel::class.java)
        bookingsViewModel.headerMap = headerMap

        requestId = (parentFragment as BookingsFragment).type

        bookingsViewModel.fetchBookings(requestId)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_assigned_bookings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObserver()
        initRecyclerView()
    }

    private fun initRecyclerView() {
        rv_assigned_bookings?.apply {
            layoutManager =
                    LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            adapter = assignedBookingAdapter
        }

    }

    private fun initObserver() {
        bookingsViewModel.isLoading.observe(viewLifecycleOwner,
                Observer {
                    if (it) progress_bar?.show()
                    else progress_bar?.hide()
                })

        bookingsViewModel.isListEmpty.observe(viewLifecycleOwner,
                Observer {
                    if (it)
                        group_empty_view?.visibility = View.VISIBLE
                    //rv_assigned_bookings?.visibility = View.GONE
                })

        bookingsViewModel.bookingsPagedList.observe(viewLifecycleOwner,{
            assignedBookingAdapter.submitList(it)
        })

        /*bookingsViewModel.resultBookingsAssigned.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> bookingsViewModel.listOfAssignedBookings.value = it.data?.data?.dataList
                Status.SERVER_ERROR -> {
                    if (it.errorMsg == "Unauthorised User") {
                        showUnauthorizedUserErrorDialog()
                    } else {
                        showSnackBar(MessageType.ERROR, it.errorMsg.toString(), requireContext())
                    }
                }
            }
        })*/

        /*bookingsViewModel.listOfAssignedBookings.observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                group_empty_view.visibility = View.GONE
                assignedBookingAdapter.submitList(it)
            } else {
                group_empty_view.visibility = View.VISIBLE
                assignedBookingAdapter.submitList(null)
            }
        })*/
    }

}