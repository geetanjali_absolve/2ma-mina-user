package com.mamina.user.ui.forget

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.mamina.user.databinding.ForgetFragmentBinding
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.ui.utils.MessageType
import com.mamina.user.utils.Status

class ForgetFragment : BaseFragment() {
    private lateinit var mViewModel: ForgetViewModel
    private var _binding: ForgetFragmentBinding? = null
    private val binding get() = _binding!!

    override fun getRootView(): View? {
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProvider(this).get(ForgetViewModel::class.java)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = ForgetFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setLoadingView(binding.forgotLoading.root)

        initObserver(view)

        binding.forgetBtn.setOnClickListener {
            hideKeyboard(it)
            if (TextUtils.isEmpty(binding.forgetEmailEditText.text.toString())) {
                showSnackbarError("Please enter your registered Email Address!")
            } else {
                showLoading()
                mViewModel.forgotPassword(binding.forgetEmailEditText.text.toString())
            }
        }

    }

    private fun initObserver(view: View) {
        mViewModel.forgotPasswordMLiveData.observe(viewLifecycleOwner, {
            hideLoading()
            when (it.status) {
                Status.SUCCESS -> {
                    Log.i("OTP Fragment", "Response:${it.data}")
                    showSnackbarMessage(it.data?.message!!)
                    findNavController().navigate(ForgetFragmentDirections.actionForgetFragmentToResetFragment(binding.forgetEmailEditText.text.toString()))
                }
                Status.AUTHORISATION -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.AUTHORISATION_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.BAD_REQUEST -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.NOT_FOUND -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.OTHER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.SERVER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                else -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
            }
        })
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}