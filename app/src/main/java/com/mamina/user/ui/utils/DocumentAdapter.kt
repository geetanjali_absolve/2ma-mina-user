package com.mamina.user.ui.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.mamina.user.R
import com.mamina.user.constants.Document
import com.mamina.user.ui.profile.ProfileFragment
import com.mamina.user.ui.signup.SignUpFragment

/**
 * Created by Rohit Singh on 18,March,2021
 * Absolve Tech company,
 * Mohali, India.
 */

class CustomDropDownAdapter(val context: Context, var listItemsTxt: ArrayList<Document>, var type: String) : BaseAdapter() {

    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemRowHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.dropdown_layout, parent, false)
            vh = ItemRowHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemRowHolder
        }

        // setting adapter item height programatically.
        val params = view.layoutParams
        params.height = 60
        view.layoutParams = params

        vh.dropdownLabel.text = listItemsTxt[position].title//listItemsTxt[0].listOfDocument[].title
        if (type == "SignUp") {
            SignUpFragment.proofType = listItemsTxt[position].title
        } else {
            ProfileFragment.proofType = listItemsTxt[position].title
        }
//        vh.label.text = listItemsTxt[0].listOfDocument.//listItemsTxt[position].listOfDocument
        return view
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return listItemsTxt.size
    }

    private class ItemRowHolder(row: View?) {
        val dropdownLabel: TextView = row?.findViewById(R.id.dropdown_menu_item) as TextView
    }
}