package com.mamina.user.ui.bookings.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.mamina.user.R
import com.mamina.user.ui.bookings.viewmodel.BookingsViewModel
import kotlinx.android.synthetic.main.fragment_bookings.*

class BookingsFragment : Fragment() {

    private lateinit var assignedBookingsFragment: AssignedBookingsFragment
    private lateinit var unassignedBookingsFragment: UnassignedBookingsFragment
    private lateinit var pastBookingsFragment: PastBookingsFragment
    private lateinit var bookingsViewModel: BookingsViewModel
    var type: Int = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        assignedBookingsFragment = AssignedBookingsFragment.newInstance()
        unassignedBookingsFragment = UnassignedBookingsFragment.newInstance()
        pastBookingsFragment = PastBookingsFragment.newInstance()
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        bookingsViewModel = ViewModelProvider(this).get(BookingsViewModel::class.java)
        return inflater.inflate(R.layout.fragment_bookings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tv_assigned.isSelected = true
        tv_unassigned.isSelected = false
        tv_past.isSelected = false

        showAssignedBookingFragment()

        tv_assigned?.setOnClickListener {
            tv_assigned.isSelected = true
            tv_unassigned.isSelected = false
            tv_past.isSelected = false
            type = 2
            showAssignedBookingFragment()
        }

        tv_unassigned?.setOnClickListener {
            tv_unassigned.isSelected = true
            tv_assigned.isSelected = false
            tv_past.isSelected = false
            type = 1
            showUnassignedBookingsFragment()
        }

        tv_past?.setOnClickListener {
            tv_assigned.isSelected = false
            tv_unassigned.isSelected = false
            tv_past.isSelected = true
            type = 6
            showPastBookingsFragment()
        }

    }

    private fun showAssignedBookingFragment() {
        val ft = childFragmentManager.beginTransaction()
        if (assignedBookingsFragment.isAdded)
            ft.show(assignedBookingsFragment)
        else ft.add(R.id.fl_container, assignedBookingsFragment)
        if (unassignedBookingsFragment.isAdded) ft.hide(unassignedBookingsFragment)
        if (pastBookingsFragment.isAdded) ft.hide(pastBookingsFragment)
        ft.commit()
    }

    private fun showUnassignedBookingsFragment() {
        val ft = childFragmentManager.beginTransaction()
        if (unassignedBookingsFragment.isAdded) ft.show(unassignedBookingsFragment)
        else ft.add(R.id.fl_container, unassignedBookingsFragment)
        if (assignedBookingsFragment.isAdded) ft.hide(assignedBookingsFragment)
        if (pastBookingsFragment.isAdded) ft.hide(pastBookingsFragment)
        ft.commit()
    }

    private fun showPastBookingsFragment() {
        val ft = childFragmentManager.beginTransaction()
        if (pastBookingsFragment.isAdded) ft.show(pastBookingsFragment)
        else ft.add(R.id.fl_container, pastBookingsFragment)
        if (assignedBookingsFragment.isAdded) ft.hide(assignedBookingsFragment)
        if (unassignedBookingsFragment.isAdded) ft.hide(unassignedBookingsFragment)
        ft.commit()
    }

}