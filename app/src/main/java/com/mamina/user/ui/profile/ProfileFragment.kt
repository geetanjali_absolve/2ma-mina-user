package com.mamina.user.ui.profile

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.DatePicker
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import com.auth0.android.jwt.JWT
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.mamina.user.MaMinaApplication
import com.mamina.user.R
import com.mamina.user.constants.ApiConstants
import com.mamina.user.constants.DocumentType
import com.mamina.user.models.response.GetIndividualUserDetailsResponse
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.ui.utils.BaseTextWatcher
import com.mamina.user.ui.utils.CustomDropDownAdapter
import com.mamina.user.utils.CommonUtils
import com.mamina.user.utils.SharedPrefsHelper
import com.mamina.user.utils.Status
import com.yalantis.ucrop.UCrop
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.File
import java.util.*
import kotlin.collections.HashMap

class ProfileFragment : BaseFragment() {
    private lateinit var profileViewModel: ProfileViewModel
    private var sharedPrefs: SharedPrefsHelper? = null
    private val REQUEST_IMAGE_CAPTURE: Int = 200
    private val REQUEST_GALLERY_IMAGE: Int = 210
    private var fileName: String? = null
    private var snackBarPermission: Snackbar? = null
    private var setBitmapMaxWidthHeight: Boolean = false
    private val ASPECT_RATIO_X = 1
    private var ASPECT_RATIO_Y: Int = 1
    private var bitmapMaxWidth: Int = 1000
    private var bitmapMaxHeight: Int = 1000
    private val IMAGE_COMPRESSION = 80
    private var profilePicPath: String? = ""
    private var frontImage: String? = ""
    private var rearImage: String? = ""
    private var selectionType: String? = ""
    private var accessToken: String? = ""
    private var userId: String = ""
    var proofTypeId = 1
    val headerMap = HashMap<String, String>()

    companion object {
        var proofType = "ID Book"
    }

    override fun getRootView(): View? {
        return sv_root_profile
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        sharedPrefs = (context?.applicationContext as MaMinaApplication).getSharedPrefInstance()
        accessToken = sharedPrefs?.read(SharedPrefsHelper.KEY_ACCESS_TOKEN, "").toString()
        headerMap[ApiConstants.KEY_AUTHORIZATION] = "Bearer $accessToken"
        val jwt = JWT(accessToken!!)

        userId = jwt.getClaim("UserId").asString().toString()

        Log.i("TAG", "onCreate: $userId")
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setLoadingView(update_details_loading)

        initObserver()

        ccp_country.registerCarrierNumberEditText(update_phone_edit_text)

        update_phone_edit_text.addTextChangedListener(object : BaseTextWatcher() {
            override fun onTextChanged(start: Int, before: Int, count: Int, s: CharSequence?) {
                if (ccp_country.isValidFullNumber) {
                    update_phone_edit_text.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.ic_tick_green,
                            0
                    )
                } else {
                    update_phone_edit_text.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.ic_tick_red,
                            0
                    )
                }
            }
        })

        val proofAdapter = CustomDropDownAdapter(requireActivity(), DocumentType.list, "Profile")
        update_proof_spinner.adapter = proofAdapter

        update_proof_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(viewGroup: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (DocumentType.list[position].value == "2") {
                    rl_proof2.visibility = View.VISIBLE
                } else {
                    rl_proof2.visibility = View.GONE
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        update_dob.setOnClickListener {
            showDatePicker()
        }

        update_profile_image.setOnClickListener {
            selectionType = "Profile"
            if (Build.VERSION.SDK_INT >= 23) {
                Dexter.withActivity(activity)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                        .withListener(multiplePermissionsListener).check()
            } else {
                openImagePopupMenu()
            }
        }

        rl_proof1.setOnClickListener {
            selectionType = "Front"
            if (Build.VERSION.SDK_INT >= 23) {
                Dexter.withActivity(activity)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                        .withListener(multiplePermissionsListener).check()
            } else {
                openImagePopupMenu()
            }
        }

        rl_proof2.setOnClickListener {
            selectionType = "Back"
            if (Build.VERSION.SDK_INT >= 23) {
                Dexter.withActivity(activity)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                        .withListener(multiplePermissionsListener).check()
            } else {
                openImagePopupMenu()
            }
        }

        showLoading()
        profileViewModel.getIndividualUserProfile(headerMap, userId)

        update_submit_button.setOnClickListener {
            if (TextUtils.isEmpty(update_fname.text.toString().trim()) ||
                    TextUtils.isEmpty(update_lname.text.toString().trim()) ||
                    TextUtils.isEmpty(update_email.text.toString().trim()) ||
                    TextUtils.isEmpty(update_dob.text.toString().trim()) ||
                    profilePicPath == "" ||
                    frontImage == "") {
                showSnackbarError("Please fill all the required fields!")
            } else if (proofTypeId == 2) {
                if (rearImage == "")
                    showSnackbarError("Please upload image of proof")
            } else {
                proofTypeId = when (proofType) {
                    "ID Book" -> {
                        1
                    }
                    "ID Card" -> {
                        2
                    }
                    "Passport" -> {
                        3
                    }
                    else -> {
                        4
                    }
                }

                val fname = CommonUtils.prepareTextPart(update_fname.text.toString().trim())
                val lname = CommonUtils.prepareTextPart(update_lname.text.toString().trim())
                val dial = CommonUtils.prepareTextPart(ccp_country.selectedCountryCode)
                val phone = CommonUtils.prepareTextPart(update_phone_edit_text.text.toString().trim())
                val email = CommonUtils.prepareTextPart(update_email.text.toString().trim())
                val dob = CommonUtils.prepareTextPart(update_dob.text.toString().trim())
                val user = CommonUtils.prepareTextPart("1")
                val idProofTypeId = CommonUtils.prepareTextPart(proofTypeId.toString())
                val idProof = CommonUtils.prepareFilePart(frontImage.toString(), "IDProof")
                val profilePic = CommonUtils.prepareFilePart(profilePicPath.toString(), "ProfilePic")
                val idCardBack = CommonUtils.prepareFilePart(rearImage.toString(), "IDCardBack")

                if (rearImage == "") {
                    profileViewModel.updateIndividualProfile(headerMap, fname, lname, dial, phone, email, dob, user, idProofTypeId,
                            idProof, profilePic, null)
                } else {
                    profileViewModel.updateIndividualProfile(headerMap, fname, lname, dial, phone, email, dob, user, idProofTypeId,
                            idProof, profilePic, idCardBack)
                }

            }
        }

    }

    private fun initObserver() {
        profileViewModel.getIndividualDetailsMLiveData.observe(viewLifecycleOwner, {
            hideLoading()
            when (it.status) {
                Status.SUCCESS -> {
                    Log.i("Personal Details", "Response:${it.data}")
                    setIndividualUserDetails(it.data?.data)
                }
                Status.AUTHORISATION -> showSnackbarError(it.errorMsg.toString())
                Status.AUTHORISATION_ERROR -> showSnackbarError(it.errorMsg.toString())
                Status.BAD_REQUEST -> showSnackbarError(it.errorMsg.toString())
                Status.NOT_FOUND -> showSnackbarError(it.errorMsg.toString())
                Status.OTHER_ERROR -> showSnackbarError(it.errorMsg.toString())
                Status.SERVER_ERROR -> showSnackbarError(it.errorMsg.toString())
                else -> showSnackbarError(it.errorMsg.toString())
            }
        })

        profileViewModel.updateIndividualProfileMLiveData.observe(viewLifecycleOwner, {
            hideLoading()
            when (it.status) {
                Status.SUCCESS -> {
                    Log.i("Personal Details", "Response:${it.data}")
                    showSnackbarMessage(it.data?.message!!)
                }
                Status.AUTHORISATION -> showSnackbarError(it.errorMsg.toString())
                Status.AUTHORISATION_ERROR -> showSnackbarError(it.errorMsg.toString())
                Status.BAD_REQUEST -> showSnackbarError(it.errorMsg.toString())
                Status.NOT_FOUND -> showSnackbarError(it.errorMsg.toString())
                Status.OTHER_ERROR -> showSnackbarError(it.errorMsg.toString())
                Status.SERVER_ERROR -> showSnackbarError(it.errorMsg.toString())
                else -> showSnackbarError(it.errorMsg.toString())
            }
        })
    }

    private fun setIndividualUserDetails(data: GetIndividualUserDetailsResponse.Data?) {
        update_fname.setText(data?.firstName)
        update_lname.setText(data?.lastName)
        update_email.setText(data?.email)
        update_phone_edit_text.setText(data?.phoneNumber)
        update_dob.setText(data?.dob)

        profilePicPath = data?.profilePic

        Log.d("TAG", "setIndividualUserDetails: "+ApiConstants.BASE_FILE + data?.profilePic)

        update_profile_image?.let {
            Glide.with(this)
                    .load(ApiConstants.BASE_FILE + data?.profilePic)
                    .centerCrop()
                    .placeholder(R.drawable.profile)
                    .into(it)
        }

        if (data?.idTypeId == 2) {
            rl_proof2.visibility = View.VISIBLE
            frontImage = data.idProof
            rearImage = data.idCardBack
            img_front_side?.let {
                Glide.with(this)
                        .load(ApiConstants.BASE_FILE + data.idProof)
                        .centerInside()
                        .into(it)
            }
            img_rear_side?.let {
                Glide.with(this)
                        .load(ApiConstants.BASE_FILE + data.idCardBack)
                        .centerInside()
                        .into(it)
            }
        } else {
            rl_proof2.visibility = View.GONE
            frontImage = data?.idProof
            img_front_side?.let {
                Glide.with(this)
                        .load(ApiConstants.BASE_FILE + data?.idProof)
                        .centerInside()
                        .into(it)
            }
        }

    }

    private fun showDatePicker() {
        val initYear: Int
        val initMonth: Int
        val initDay: Int
        val c = Calendar.getInstance()
        val selectedDob: String = update_dob?.text.toString()
        if (TextUtils.isEmpty(selectedDob)) {
            initYear = c[Calendar.YEAR]
            initMonth = c[Calendar.MONTH]
            initDay = c[Calendar.DAY_OF_MONTH]
        } else {
            val userSelected = selectedDob.split("-".toRegex()).toTypedArray()
            initYear = userSelected[2].toInt()
            initDay = userSelected[0].toInt()
            initMonth = userSelected[1].toInt() - 1
        }
        val datePickerDialog: DatePickerDialog
        datePickerDialog = DatePickerDialog(
                requireActivity(), R.style.PickerStyle,
                { view1: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                    val dayOfMonthh = String.format(Locale.getDefault(), "%02d", dayOfMonth)
                    val monthOfYearr = String.format(Locale.getDefault(), "%02d", monthOfYear + 1)
                    val x = "$dayOfMonthh-$monthOfYearr-$year"
                    update_dob?.setText(x)
                    update_dob?.setCompoundDrawablesWithIntrinsicBounds(
                            R.drawable.ic_calendar,
                            0,
                            R.drawable.ic_tick_green,
                            0
                    )
                },
                initYear,
                initMonth,
                initDay
        )
        datePickerDialog.datePicker.maxDate = c.timeInMillis - 568025136000L // -18yrs
        datePickerDialog.setOnShowListener {
            val greenColor = ContextCompat.getColor(activity as Context, R.color.primary_color)
            datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(greenColor)
            datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(greenColor)
        }
        datePickerDialog.show()
    }


    private var multiplePermissionsListener: MultiplePermissionsListener =
            object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        openImagePopupMenu()
                    }
                    if (report.isAnyPermissionPermanentlyDenied) {
                        snackBarPermission = Snackbar.make(
                                getRootView()!!,
                                getString(R.string.title_camera_and_storage_access),
                                Snackbar.LENGTH_INDEFINITE
                        )
                                .setAction(getString(R.string.action_settings)) {
                                    // open system setting screen ...
                                    val intent = Intent()
                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                    val uri = Uri.fromParts("package", activity?.packageName, null)
                                    intent.data = uri
                                    activity?.startActivity(intent)
                                }
                        snackBarPermission?.show()
                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                        permissions: MutableList<PermissionRequest>,
                        token: PermissionToken,
                ) {
                    token.continuePermissionRequest()
                }
            }

    private fun openImagePopupMenu() {
        val dialog = Dialog(requireContext())
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Choose Image Source")
        builder.setItems(arrayOf<CharSequence?>("Camera", "Gallery")) { dialogInterface, i ->
            when (i) {
                0 -> openCamera()
                1 -> openGallery()
            }
        }
        builder.show()
        dialog.dismiss()
    }

    private fun openCamera() {
        fileName = "IMG_" + System.currentTimeMillis().toString() + ".jpg"
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath())
        if (activity?.packageManager?.let { intent.resolveActivity(it) } != null) {
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
        }
    }

    private fun getCacheImagePath(): Uri? {
        val path = File(activity?.externalCacheDir, "camera")
        if (!path.exists()) path.mkdirs()
        val image = File(path, fileName)
        return activity?.let {
            FileProvider.getUriForFile(
                it,
                "com.mamina.user.provider",
                image
            )
        }
    }

    private fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, REQUEST_GALLERY_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_IMAGE_CAPTURE -> if (resultCode == Activity.RESULT_OK) {
                cropImage(getCacheImagePath())
            }
            REQUEST_GALLERY_IMAGE -> if (resultCode == Activity.RESULT_OK) {
                val imageUri = data?.data
                cropImage(imageUri)
            }
            UCrop.REQUEST_CROP -> if (resultCode == Activity.RESULT_OK) {
                handleUCropResult(data)
            }
            UCrop.RESULT_ERROR -> {
                val cropError = UCrop.getError(data!!)
                //  Log.e("TAG", "Crop error: $cropError")
                showSnackbarError("Crop error: $cropError")
            }
        }
    }

    private fun cropImage(sourceUri: Uri?) {
        val destinationUri = Uri.fromFile(
                File(
                        activity?.cacheDir,
                        queryName(activity?.contentResolver!!, sourceUri!!)
                )
        )
        val options = UCrop.Options()
        options.setCompressionQuality(IMAGE_COMPRESSION)
        options.setToolbarColor(ContextCompat.getColor(requireContext(), R.color.primary_color))
        options.setStatusBarColor(ContextCompat.getColor(requireContext(), R.color.primary_color))
        options.setActiveWidgetColor(ContextCompat.getColor(requireContext(),
                R.color.primary_color))
        options.withAspectRatio(ASPECT_RATIO_X.toFloat(), ASPECT_RATIO_Y.toFloat())
        if (setBitmapMaxWidthHeight) options.withMaxResultSize(bitmapMaxWidth, bitmapMaxHeight)
        UCrop.of(sourceUri, destinationUri).withOptions(options).start(requireActivity(), this)
    }

    private fun handleUCropResult(data: Intent?) {
        if (data == null) {
            return
        }
        val resultUri = UCrop.getOutput(data)
        Log.e("TAG", "Crop : $resultUri")

        when (selectionType) {
            "Profile" -> {
                profilePicPath = resultUri?.path
                update_profile_image?.let {
                    Glide.with(this)
                            .load(resultUri)
                            .centerCrop()
                            .placeholder(R.drawable.profile)
                            .into(it)
                }
            }
            "Front" -> {
                frontImage = resultUri?.path
                img_front_side.setImageURI(resultUri)
            }
            "Back" -> {
                rearImage = resultUri?.path
                img_rear_side.setImageURI(resultUri)
            }
        }
    }

    private fun queryName(resolver: ContentResolver, uri: Uri): String? {
        val returnCursor: Cursor = resolver.query(uri, null, null, null, null)!!
        val nameIndex: Int = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        val name: String = returnCursor.getString(nameIndex)
        returnCursor.close()
        return name
    }

    override fun onDestroyView() {
        if (snackBarPermission != null) {
            if (snackBarPermission!!.isShown) {
                snackBarPermission?.dismiss()
            }
        }
        super.onDestroyView()
    }

}