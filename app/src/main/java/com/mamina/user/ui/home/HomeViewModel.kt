package com.mamina.user.ui.home

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mamina.WebService
import com.mamina.user.MaMinaApplication
import com.mamina.user.models.response.PriceDetail
import com.mamina.user.models.response.WebResponse
import com.mamina.user.repository.remote.InitialRepository
import com.mamina.user.ui.base.BaseViewModel
import com.mamina.user.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class HomeViewModel(application: Application) : BaseViewModel(application) {
    private var initialRepository: InitialRepository
    private var webService: WebService = (application as MaMinaApplication).getWebServiceInstance()
    var priceDetailMutableLiveData: MutableLiveData<WebResponse<PriceDetail>>? = null

    init {
        initialRepository = InitialRepository(webService)
        priceDetailMutableLiveData = MutableLiveData()
    }

    companion object {
        private val TAG = HomeViewModel::class.java.simpleName
    }

    @ExperimentalCoroutinesApi
    fun getPriceDetail(map: Map<String, String>) {
        if(priceDetailMutableLiveData?.value == null)
            viewModelScope.launch {
            initialRepository.getPriceDetail(map)
                    .onStart {

                    }.onCompletion {

                    }.catch { exception ->
                        priceDetailMutableLiveData?.value = WebResponse(Status.OTHER_ERROR, null, exception.message)
                    }.collect {
                        priceDetailMutableLiveData?.value = WebResponse(it.code, it, it.message)
                    }
        }
    }


}