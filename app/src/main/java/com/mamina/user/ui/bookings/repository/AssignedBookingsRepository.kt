package com.mamina.user.ui.bookings.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.mamina.WebService
import com.mamina.user.ui.bookings.model.BookingInfoList
import com.mamina.user.ui.bookings.model.BookingsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class AssignedBookingsRepository(private var webService: WebService, private var headers: HashMap<String, String>,
                                 var requestStatusId: Int) : PageKeyedDataSource<Long, BookingInfoList>() {
    override fun loadInitial(params: LoadInitialParams<Long>, callback: LoadInitialCallback<Long, BookingInfoList>) {
        webService.fetchUserOrders(headers, requestStatusId, 1).enqueue(object :
                Callback<BookingsResponse> {
            override fun onResponse(call: Call<BookingsResponse>, response: Response<BookingsResponse>) {
                if (response.isSuccessful && response.body() != null) {
                    val result = response.body()
                    if (result?.data?.dataList != null)
                        callback.onResult(result.data.dataList, null, 2)
                }
            }

            override fun onFailure(call: Call<BookingsResponse>, error: Throwable) {}
        })
    }

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, BookingInfoList>) {
        webService.fetchUserOrders(headers, requestStatusId, params.key.toInt()).enqueue(object :
                Callback<BookingsResponse> {
            override fun onResponse(call: Call<BookingsResponse>, response: Response<BookingsResponse>) {
                if (response.isSuccessful && response.body() != null) {
                    val result = response.body()
                    if (result?.data?.dataList != null)
                        callback.onResult(result.data.dataList, params.key + 1)
                }
            }

            override fun onFailure(call: Call<BookingsResponse>, error: Throwable) {}
        })
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, BookingInfoList>) {}


    class BookingsDataSourceFactory(val webService: WebService, var headers: HashMap<String, String>, var requestStatusId: Int)
        : DataSource.Factory<Long, BookingInfoList>() {
        val userLiveDataSource = MutableLiveData<AssignedBookingsRepository>()

        override fun create(): DataSource<Long, BookingInfoList> {
            val userDataSource = AssignedBookingsRepository(webService, headers, requestStatusId)
            userLiveDataSource.postValue(userDataSource)
            return userDataSource
        }
    }
}


/*
(val webService: WebService, val isLoading: MutableLiveData<Boolean>) {

    @ExperimentalCoroutinesApi
    suspend fun fetchAssignedBookings(token: Map<String, String>, requestStatusId: Int, page: Int,
                                  listOfBookings: MutableLiveData<MutableList<BookingInfoList>>): Flow<Unit> {
        return flow {
            val result = webService.fetchUserOrders(token, requestStatusId, page).enqueue(object : Callback<BookingsResponse> {
                override fun onResponse(
                        call: Call<BookingsResponse>,
                        response: Response<BookingsResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        val result = response.body()
                        if (result?.data?.dataList != null) {
                            isLoading.value = false
                            listOfBookings.value = result.data.dataList
                        }
                    }
                }

                override fun onFailure(call: Call<BookingsResponse>, t: Throwable) {
                    val errorMsg: String? = ErrorHandler.reportError(t)
                }
            })
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

}*/
