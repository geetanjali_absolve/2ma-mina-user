package com.mamina.user.ui.login

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mamina.WebService
import com.mamina.user.MaMinaApplication
import com.mamina.user.models.request.LoginRequest
import com.mamina.user.models.response.LoginResponse
import com.mamina.user.models.response.WebResponse
import com.mamina.user.repository.remote.InitialRepository
import com.mamina.user.ui.base.BaseViewModel
import com.mamina.user.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class LoginViewModel(application: Application) : BaseViewModel(application) {
    var webService: WebService = (application as MaMinaApplication).getWebServiceInstance()
    var initialRepository: InitialRepository = InitialRepository(webService)

    var userMutableLiveData: MutableLiveData<WebResponse<LoginResponse>> = MutableLiveData()

    @ExperimentalCoroutinesApi
    fun loginResponseLiveData(email: String, password: String, deviceType: String, deviceToken: String) {
        viewModelScope.launch {
            initialRepository.login(LoginRequest(email, password, deviceType, deviceToken))
                    .onStart {

                    }.onCompletion {

                    }.catch { exception ->
                        userMutableLiveData.value = WebResponse(Status.OTHER_ERROR, null, exception.message)
                    }.collect {
                        userMutableLiveData.value = WebResponse(it.code!!, it, it.message)//WebResponse(it.code, it.data, it.message)
                    }
        }
    }

    companion object {
        private val TAG = LoginViewModel::class.java.simpleName
    }

}