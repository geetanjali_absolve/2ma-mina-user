package com.mamina.user.ui.signup

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mamina.WebService
import com.mamina.user.MaMinaApplication
import com.mamina.user.models.response.SignUpBusinessUserResponse
import com.mamina.user.models.response.SignUpIndividualResponse
import com.mamina.user.models.response.WebResponse
import com.mamina.user.repository.remote.InitialRepository
import com.mamina.user.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody

class SignUpViewModel(application: Application) : AndroidViewModel(application) {
    private var initialRepository: InitialRepository
    private var webService: WebService
    var resultSignUpIndividualUser: MutableLiveData<WebResponse<SignUpIndividualResponse>>
    var resultSignUpBusinessUser: MutableLiveData<WebResponse<SignUpBusinessUserResponse>>

    companion object {
        private val TAG = SignUpViewModel::class.java.simpleName
    }

    init {
        webService = (application as MaMinaApplication).getWebServiceInstance()
        initialRepository = InitialRepository(webService)
        resultSignUpIndividualUser = MutableLiveData()
        resultSignUpBusinessUser = MutableLiveData()
    }

    @ExperimentalCoroutinesApi
    fun signUpIndividually(firstName: RequestBody,
                           lastName: RequestBody,
                           dial: RequestBody,
                           phone: RequestBody,
                           email: RequestBody,
                           dob: RequestBody,
                           pass: RequestBody,
                           user: RequestBody,
                           deviceType: RequestBody,
                           deviceToken: RequestBody,
                           idProofType: RequestBody,
                           idProof: MultipartBody.Part,
                           idCardBook: MultipartBody.Part?) {
        viewModelScope.launch {
            initialRepository.signUp(
                    firstName,
                    lastName,
                    dial,
                    phone,
                    email,
                    dob,
                    pass,
                    user,
                    deviceType,
                    deviceToken,
                    idProofType,
                    idProof,
                    idCardBook)
                    .onStart {
//                        isLoading.value = true
//                        isViewEnable.value = false
                    }
                    .onCompletion {
//                        isLoading.value = false
//                        isViewEnable.value = true
                    }
                    .catch { exception ->
//                        / emit error state /
                        Log.i(TAG, "Exception:" + exception.message)
                        resultSignUpIndividualUser.value = WebResponse(Status.OTHER_ERROR, null, exception.message)
                    }
                    .collect {
                        Log.i(TAG, "signupIndividually: $it")
                        resultSignUpIndividualUser.value = WebResponse(it.code, it, it.message)//WebResponse(it.code, it.data, it.message)
                    }
        }
    }

    @ExperimentalCoroutinesApi
    fun signUpBusiness(firstName: RequestBody,
                       lastName: RequestBody,
                       dial: RequestBody,
                       phone: RequestBody,
                       email: RequestBody,
                       pass: RequestBody,
                       website: RequestBody,
                       contactPerson: RequestBody,
                       licenseNo: RequestBody,
                       vat: RequestBody,
                       externalNumber: RequestBody,
                       user: RequestBody,
                       deviceType: RequestBody,
                       deviceToken: RequestBody,
                       licenseFile: MultipartBody.Part,
                       vatFile: MultipartBody.Part,
                       commerceFile: MultipartBody.Part,
                       agreementFile: MultipartBody.Part) {
        viewModelScope.launch {
            initialRepository.signUpBusiness(
                    firstName,
                    lastName,
                    dial,
                    phone,
                    email,
                    pass,
                    website,
                    contactPerson,
                    licenseNo,
                    vat,
                    externalNumber,
                    user,
                    deviceType,
                    deviceToken,
                    licenseFile,
                    vatFile,
                    commerceFile,
                    agreementFile)
                    .onStart {
//                        isLoading.value = true
//                        isViewEnable.value = false
                    }
                    .onCompletion {
//                        isLoading.value = false
//                        isViewEnable.value = true
                    }
                    .catch { exception ->
//                        / emit error state /
                        Log.i(TAG, "Exception:" + exception.message)
                        resultSignUpBusinessUser.value = WebResponse(Status.OTHER_ERROR, null, exception.message)
                    }
                    .collect {
                        Log.i(TAG, "signupBusiness: $it")
                        resultSignUpBusinessUser.value = WebResponse(it.code, it, it.message)//WebResponse(it.code, it.data, it.message)
                    }
        }

    }

}