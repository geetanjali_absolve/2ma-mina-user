package com.mamina.user.ui.detail

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import com.mamina.user.MaMinaApplication
import com.mamina.user.MainActivity
import com.mamina.user.R
import com.mamina.user.constants.ApiConstants
import com.mamina.user.databinding.ParcelDetailFragmentBinding
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.ui.utils.BaseTextWatcher
import com.mamina.user.ui.utils.MessageType
import com.mamina.user.utils.Constants
import com.mamina.user.utils.SharedPrefsHelper
import com.mamina.user.utils.Status
import kotlinx.android.synthetic.main.parcel_detail_fragment.*

class ParcelDetailFragment : BaseFragment() {

    private var sharedPrefs: SharedPrefsHelper? = null
    private lateinit var binding: ParcelDetailFragmentBinding
    private lateinit var mViewModel: ParcelDetailViewModel
    override fun getRootView(): View? {
        return parent_layout
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
//        val view = inflater.inflate(R.layout.parcel_detail_fragment, container, false)
        binding = ParcelDetailFragmentBinding.inflate(inflater, container, false)
        val view = binding.root

        Log.d(TAG, "onCreateView: this")
        setLoadingView(binding.parcelLoading.root)
        mViewModel = ViewModelProvider(this).get(ParcelDetailViewModel::class.java)
        sharedPrefs = (context?.applicationContext as MaMinaApplication).getSharedPrefInstance()
        val accessToken = sharedPrefs?.read(SharedPrefsHelper.KEY_ACCESS_TOKEN, "").toString()
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[ApiConstants.KEY_AUTHORIZATION] = "Bearer $accessToken"
//        hashMap["Authorization"] = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOiI4NjM3NjQ4NC00ODUwLTRiZTAtODY0ZC0zZjM0NDM4YWJlZGUiLCJEZXZpY2VUb2tlbiI6IjEyMzQ1Njc4IiwibmJmIjoxNjE2MzI2ODMxLCJleHAiOjE2MTY0MTMyMzEsImlhdCI6MTYxNjMyNjgzMX0.w8RccXUfdSBTwuDn44LPQBU-OoPElk9uyavMOJm7cMs"
        initObservers(view)

//        binding.pDetailSenderAddressTv.text = arguments.getParcelable<ParcelInfoData>("parcelInfoData").

        //Country Code and Phone number code
        binding.ccpCountry.registerCarrierNumberEditText(binding.pDetailPhoneEditText)

        binding.pDetailPhoneEditText.addTextChangedListener(object : BaseTextWatcher() {
            override fun onTextChanged(start: Int, before: Int, count: Int, s: CharSequence?) {
                if (binding.ccpCountry.isValidFullNumber) {
                    binding.pDetailPhoneEditText.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_tick_green,
                        0
                    )
                } else {
                    binding.pDetailPhoneEditText.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_tick_red,
                        0
                    )
                }
            }
        })

        binding.pDetailSenderAddressTv.text = (activity as MainActivity).orderRequest.SenderAddress
        binding.pDetailRecAddressTv.text = (activity as MainActivity).orderRequest.ReceiverAddress
        binding.pDetailAddressEditText.setText((activity as MainActivity).orderRequest.ReceiverAddress)

        mViewModel.getDistancePrice(
            hashMap,
            (activity as MainActivity).orderRequest.SenderLat,
            (activity as MainActivity).orderRequest.SenderLong,
            (activity as MainActivity).orderRequest.ReceiverLat,
            (activity as MainActivity).orderRequest.ReceiverLong,
            (activity as MainActivity).orderRequest.DeliveryTypeId
        )

        //check delivery type
        when ((activity as MainActivity).orderRequest.DeliveryTypeId) {
            1 -> {
                Constants.normal.also { binding.pDetailTypeValueTv.text = it }
                (activity as MainActivity).orderRequest.deliveryPrice.toString()
                    .also { binding.pDetailPriceTv.text = "R$it" }
                (activity as MainActivity).orderRequest.description.also {
                    binding.pDetailDeliveryTypeTv.text = it
                }
            }
            2 -> {
                Constants.express.also { binding.pDetailTypeValueTv.text = it }
                (activity as MainActivity).orderRequest.deliveryPrice.toString()
                    .also { binding.pDetailPriceTv.text = "R$it" }
                (activity as MainActivity).orderRequest.description.also {
                    binding.pDetailDeliveryTypeTv.text = it
                }
            }
            else -> {
                Constants.bakkie.also { binding.pDetailTypeValueTv.text = it }
                (activity as MainActivity).orderRequest.deliveryPrice.toString()
                    .also { binding.pDetailPriceTv.text = "R$it" }
                (activity as MainActivity).orderRequest.description.also {
                    binding.pDetailDeliveryTypeTv.text = it
                }
            }
        }

        binding.pDetailSubmitBtn.setOnClickListener {
            val receiverName = binding.pDetailNameEditText.text.toString()
            val receiverEmail = binding.loginEmailEditText.text.toString()
            val receiverPhone = binding.pDetailPhoneEditText.text.toString()
            val receiverAddress = binding.pDetailAddressEditText.text.toString()

            if (TextUtils.isEmpty(receiverName) || TextUtils.isEmpty(receiverPhone) || TextUtils.isEmpty(
                    receiverAddress
                )
            ) {
                Snackbar.make(
                    binding.pDetailSubmitBtn,
                    getString(R.string.required_alert),
                    Snackbar.LENGTH_SHORT
                ).show()
            } else {
                (activity as MainActivity).orderRequest.ReceiverEmail = receiverEmail
                (activity as MainActivity).orderRequest.DialCode =
                    binding.ccpCountry.selectedCountryCode
                (activity as MainActivity).orderRequest.ReceiverMobileNumber = receiverPhone
                (activity as MainActivity).orderRequest.ReceiverAddress = receiverAddress
                (activity as MainActivity).orderRequest.ReceiverName = receiverName
                (activity as MainActivity).orderRequest.TotalDeliveryTime =
                    binding.pDetailTimeValueTv.text.toString()
                (activity as MainActivity).orderRequest.TotalDeliveryDistance =
                    binding.pDetailDistanceValueTv.text.toString()
                Navigation.findNavController(it)
                    .navigate(R.id.action_nav_parcel_detail_to_nav_goods_info)
            }
        }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel = ViewModelProvider(this).get(ParcelDetailViewModel::class.java)
        // TODO: Use the ViewModel
    }

    private fun initObservers(view: View) {
        showLoading()
        mViewModel.distanceMutableLiveData.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    hideLoading()
                    it.data.let { it1 ->
                        binding.pDetailDistanceValueTv.text = it1?.data?.distance
                        binding.pDetailTimeValueTv.text = it1?.data?.duration
                    }
                }
                Status.AUTHORISATION -> showSnackBar(
                    MessageType.ERROR,
                    it.errorMsg,
                    requireContext()
                )
                Status.AUTHORISATION_ERROR -> showSnackBar(
                    MessageType.ERROR,
                    it.errorMsg,
                    requireContext()
                )
                Status.BAD_REQUEST -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.NOT_FOUND -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.OTHER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.SERVER_ERROR -> showSnackBar(
                    MessageType.ERROR,
                    it.errorMsg,
                    requireContext()
                )
                else -> {
                    hideLoading()
                    showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                }
            }
        })
    }

    companion object {
        private val TAG = ParcelDetailFragment::class.java.simpleName
        fun newInstance(): ParcelDetailFragment {
            return ParcelDetailFragment()
        }
    }

}