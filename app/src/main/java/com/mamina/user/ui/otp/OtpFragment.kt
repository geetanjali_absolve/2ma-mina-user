package com.mamina.user.ui.otp

import android.app.Activity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.mamina.user.MaMinaApplication
import com.mamina.user.R
import com.mamina.user.constants.ApiConstants
import com.mamina.user.databinding.OtpFragmentBinding
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.ui.utils.BaseTextWatcher
import com.mamina.user.ui.utils.MessageType
import com.mamina.user.utils.SharedPrefsHelper
import com.mamina.user.utils.Status
import kotlinx.android.synthetic.main.otp_fragment.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

class OtpFragment : BaseFragment() {

    private var _binding: OtpFragmentBinding? = null
    private val binding get() = _binding!!

    private lateinit var mViewModel: OtpViewModel
    private var usrOtp: String = ""
    private var sharedPrefs: SharedPrefsHelper? = null

    override fun getRootView(): View? {
        return parent_otp
    }

    private var dialCode: String? = null
    private var phoneNumber: String? = null
    private var Email: String? = null
    private var isEmail: Boolean? = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = OtpFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mViewModel = ViewModelProvider(this).get(OtpViewModel::class.java)
        super.onViewCreated(view, savedInstanceState)

        initObservers(view)
        initEditorListener()

        sharedPrefs = (context?.applicationContext as MaMinaApplication).getSharedPrefInstance()


        isEmail = OtpFragmentArgs.fromBundle(requireArguments()).showEmailOtp


        if (isEmail == true) {
            binding.otpTitle.text = resources.getString(R.string.otp_title_email)
            binding.otpSubtitle.text = resources.getString(R.string.otp_subtitle_email)
        } else {
            binding.otpTitle.text = resources.getString(R.string.otp_title_mobile)
            binding.otpSubtitle.text = resources.getString(R.string.otp_subtitle_mobile)
        }

        val accessToken = sharedPrefs?.builder()?.read(SharedPrefsHelper.KEY_ACCESS_TOKEN, "").toString()
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[ApiConstants.KEY_AUTHORIZATION] = "Bearer $accessToken"

        binding.resendOtp.setOnClickListener {

            if (isEmail == true) {
                Email = OtpFragmentArgs.fromBundle(requireArguments()).emailMobile
                Log.i("OTP Fragment", "email$Email")

                mViewModel.resendOtpEmail(hashMap, Email)
            } else {
                dialCode = sharedPrefs?.builder()?.read(SharedPrefsHelper.KEY_DIAL_CODE, "")
                phoneNumber = sharedPrefs?.builder()?.read(SharedPrefsHelper.KEY_PHONE, "")
                Log.i("OTP Fragment", "phoneNumber$phoneNumber$dialCode")

                mViewModel.resendOtpPhone(hashMap, dialCode, phoneNumber)
            }

        }

        binding.otpSubmitButton.setOnClickListener {
            attemptEmailVerification(hashMap)
        }
    }

    @ExperimentalCoroutinesApi
    private fun attemptEmailVerification(hashMap: HashMap<String, String>) {
        if (isFormValid()) {
            edt_otp_one?.clearFocus()
            edt_otp_two?.clearFocus()
            edt_otp_three?.clearFocus()
            edt_otp_four?.clearFocus()
            hashMap?.let {
                if (isEmail == true) {
                    mViewModel.verifyEmailOtp(it, Email.toString(), usrOtp)
                } else {
                    mViewModel.verifyPhoneOtp(it, usrOtp)
                }
            }
        }
    }


    private fun initEditorListener() {
        edt_otp_one?.addTextChangedListener(object : BaseTextWatcher() {
            override fun onTextChanged(
                    start: Int,
                    before: Int,
                    count: Int,
                    s: CharSequence?
            ) {
                if (count == 1) edt_otp_two?.requestFocus()
            }
        })

        edt_otp_two?.addTextChangedListener(object : BaseTextWatcher() {
            override fun onTextChanged(
                    start: Int,
                    before: Int,
                    count: Int,
                    s: CharSequence?
            ) {
                if (count == 1) edt_otp_three?.requestFocus()
                if (count == 0) edt_otp_one?.requestFocus()
            }
        })

        edt_otp_three?.addTextChangedListener(object : BaseTextWatcher() {
            override fun onTextChanged(
                    start: Int,
                    before: Int,
                    count: Int,
                    s: CharSequence?
            ) {
                if (count == 1) edt_otp_four?.requestFocus()
                if (count == 0) edt_otp_two?.requestFocus()
            }
        })

        edt_otp_four?.addTextChangedListener(object : BaseTextWatcher() {
            override fun onTextChanged(
                    start: Int,
                    before: Int,
                    count: Int,
                    s: CharSequence?
            ) {
                if (count == 0) edt_otp_three.requestFocus()
//                if (count == 1)
            }
        })


        edt_otp_one?.setOnEditorActionListener(TextView.OnEditorActionListener { v: TextView?, actionId: Int, event: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                edt_otp_two?.requestFocus()
            }
            false
        })

        edt_otp_two?.setOnEditorActionListener(TextView.OnEditorActionListener { v: TextView?, actionId: Int, event: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                edt_otp_one?.requestFocus()
            }
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                edt_otp_three?.requestFocus()
            }
            false
        })

        edt_otp_three?.setOnEditorActionListener(TextView.OnEditorActionListener { v: TextView?, actionId: Int, event: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                edt_otp_two?.requestFocus()
            }
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                edt_otp_four?.requestFocus()
            }
            false
        })

        edt_otp_four?.setOnEditorActionListener(TextView.OnEditorActionListener { v: TextView?, actionId: Int, event: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                edt_otp_three?.requestFocus()
            }
            if (actionId == EditorInfo.IME_ACTION_GO) {
                hideKeyboard(edt_otp_four)
            }
            false
        })
    }


    private fun initObservers(view: View) {
        mViewModel.resendOtpMLiveData.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    Log.i("OTP Fragment", "Response:${it.data} ")
                }
                Status.AUTHORISATION -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.AUTHORISATION_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.BAD_REQUEST -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.NOT_FOUND -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.OTHER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.SERVER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                else -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
            }
        })

        mViewModel.verifyOtpMLiveData.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    Log.i("OTP Fragment", "Response:${it.data} ")
                    showSnackBar(MessageType.SUCCESS, it.data?.message, requireContext())
                    Navigation.findNavController(view).navigate(R.id.action_otp_fragment_to_login_fragment)
                }
                Status.AUTHORISATION -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.AUTHORISATION_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.BAD_REQUEST -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.NOT_FOUND -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.OTHER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.SERVER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                else -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
            }
        })
    }


    private fun isFormValid(): Boolean {
        val a: String = edt_otp_one.text.toString()
        val b: String = edt_otp_two.text.toString()
        val c: String = edt_otp_three.text.toString()
        val d: String = edt_otp_four.text.toString()
        if (TextUtils.isEmpty(a) && TextUtils.isEmpty(b) && TextUtils.isEmpty(c) && TextUtils.isEmpty(
                        d
                )
        ) {
            showSnackBar(MessageType.ERROR, getString(R.string.alert_verify_otp), requireContext())
            return false
        }
        // please dont do it above !!!
        usrOtp = a + b + c + d
        return true
    }


    companion object {
        fun newInstance(): OtpFragment {
            return OtpFragment()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}