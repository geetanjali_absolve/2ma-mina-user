package com.mamina.user.ui.utils

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mamina.user.R
import com.mamina.user.constants.ParcelType

/**
 *
 * A fragment that shows a list of items as a modal bottom sheet.
 *
 * You can show this modal bottom sheet from your activity like this:
 * <pre>
 * MaMinaDialogFragment.newInstance(30).show(getSupportFragmentManager(), "dialog");
</pre> *
 */
class MaMinaDialogFragment : BottomSheetDialogFragment() {
    private lateinit var closeImg: ImageView
    private lateinit var bottomSheetPlanImg: ImageView
    private lateinit var bottomSheetPlanTitle: TextView
    private lateinit var bottomSheetPlanDesc: TextView
    private lateinit var bottomSheetPlanPrice: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_ma_mina_dialog_list_dialog, container, false)
        val parcelType = arguments?.getInt(ARG_ITEM_COUNT)

        // initialisation of the variables
        closeImg = view.findViewById(R.id.bottom_sheet_close_img)
        bottomSheetPlanImg = view.findViewById(R.id.bottom_sheet_plan_img)
        bottomSheetPlanTitle = view.findViewById(R.id.bottom_sheet_plan_title)
        bottomSheetPlanDesc = view.findViewById(R.id.bottom_sheet_plan_desc)
        bottomSheetPlanPrice = view.findViewById(R.id.bottom_sheet_plan_price)

        // adding on click listeners
        closeImg.setOnClickListener(View.OnClickListener { view1: View? -> dismiss() })


        // set the content
        bottomSheetPlanImg.setImageDrawable(getPlanImage(parcelType!!))
        bottomSheetPlanTitle.setText(getPlanTitle(parcelType))
        bottomSheetPlanDesc.setText(getPlanDesc(parcelType))
        bottomSheetPlanPrice.setText(getPlanPrice(parcelType))
        return view
    }

    private fun getPlanPrice(parcelType: Int): String {
        when (parcelType) {
            ParcelType.NORMAL_DELIVERY -> return "R45"
            ParcelType.BAKKIE_DELIVERY -> return "R80"
            ParcelType.EXPRESS_DELIVERY -> return "R60"
        }
        return "Normal Delivery"
    }

    private fun getPlanDesc(parcelType: Int): String {
        when (parcelType) {
            ParcelType.NORMAL_DELIVERY -> return "Same Day Delivery"
            ParcelType.BAKKIE_DELIVERY -> return "Heavy Delivery"
            ParcelType.EXPRESS_DELIVERY -> return "Quick Delivery"
        }
        return "Same Day Delivery"
    }

    private fun getPlanTitle(parcelType: Int): String {
        when (parcelType) {
            ParcelType.NORMAL_DELIVERY -> return "Normal Delivery"
            ParcelType.BAKKIE_DELIVERY -> return "Bakkie Delivery"
            ParcelType.EXPRESS_DELIVERY -> return "Express Delivery"
        }
        return "Normal Delivery"
    }

    private fun getPlanImage(parcelType: Int): Drawable {
        when (parcelType) {
            ParcelType.NORMAL_DELIVERY -> return resources.getDrawable(R.drawable.img_normal)
            ParcelType.EXPRESS_DELIVERY -> return resources.getDrawable(R.drawable.img_express)
            ParcelType.BAKKIE_DELIVERY -> return resources.getDrawable(R.drawable.img_bakkie)
        }
        return resources.getDrawable(R.drawable.img_normal)
    } /*@Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        final RecyclerView recyclerView = (RecyclerView) view;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new ItemAdapter(getArguments().getInt(ARG_ITEM_COUNT)));
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        final TextView text;

        ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            // TODO: Customize the item layout
            super(inflater.inflate(R.layout.fragment_ma_mina_bottom_sheet, parent, false));
            text = itemView.findViewById(R.id.text);
        }
    }

    private class ItemAdapter extends RecyclerView.Adapter<ViewHolder> {

        private final int mItemCount;

        ItemAdapter(int itemCount) {
            mItemCount = itemCount;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.text.setText(String.valueOf(position));
        }

        @Override
        public int getItemCount() {
            return mItemCount;
        }

    }*/

    companion object {
        private val TAG = MaMinaDialogFragment::class.java.simpleName

        // TODO: Customize parameter argument names
        private val ARG_ITEM_COUNT: String = "item_count"

        // TODO: Customize parameters
        fun newInstance(parcelType: Int): MaMinaDialogFragment {
            Log.d(TAG, "newInstance: ")
            Log.d(TAG, "parcelType: $parcelType")
            val fragment = MaMinaDialogFragment()
            val args = Bundle()
            args.putInt(ARG_ITEM_COUNT, parcelType)
            fragment.arguments = args
            return fragment
        }
    }
}