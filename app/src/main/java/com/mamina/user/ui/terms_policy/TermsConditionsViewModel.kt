package com.mamina.user.ui.terms_policy

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mamina.WebService
import com.mamina.user.MaMinaApplication
import com.mamina.user.repository.remote.InitialRepository
import com.mamina.user.ui.base.BaseViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import org.jsoup.Jsoup
import retrofit2.Call
import retrofit2.Response

class TermsConditionsViewModel(application: Application) : BaseViewModel(application) {
    var webService: WebService = (application as MaMinaApplication).getWebServiceInstance()
    var initialRepository: InitialRepository = InitialRepository(webService)
    var getInfoMLiveData: MutableLiveData<String> =
            MutableLiveData()
    var isLoading: MutableLiveData<Boolean> = MutableLiveData()

    @ExperimentalCoroutinesApi
    fun getTermsAndCondition(
            header: Map<String, String>,
    ) {
        viewModelScope.launch {
            webService.getTermsConditions(header).enqueue(object :
                    retrofit2.Callback<ResponseBody> {
                override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>,
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        val result = response.body()!!.string()
                        val document = Jsoup.parse(result)
                        val element = document.select("body")
                        getInfoMLiveData.value = element.toString()
                        Log.i("TAG", "onResponse: $result")
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                }

            })
        }
    }

    @ExperimentalCoroutinesApi
    fun getPrivacyPolicy(
            header: Map<String, String>,
    ) {
        viewModelScope.launch {
            webService.getSitePolicy(header).enqueue(object :
                    retrofit2.Callback<ResponseBody> {
                override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>,
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        val result = response.body()!!.string()
                        val document = Jsoup.parse(result)
                        val element = document.select("body")
                        getInfoMLiveData.value = element.toString()
                        Log.i("TAG", "onResponse: $result")
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                }

            })
        }
    }


    companion object {
        private val TAG = TermsConditionsViewModel::class.java.simpleName
    }

}