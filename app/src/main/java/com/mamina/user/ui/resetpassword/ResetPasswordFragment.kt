package com.mamina.user.ui.resetpassword

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.mamina.user.R
import com.mamina.user.databinding.ResetPasswordFragmentBinding
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.ui.utils.MessageType
import com.mamina.user.utils.Status
import kotlinx.android.synthetic.main.reset_password_fragment.*

class ResetPasswordFragment : BaseFragment() {
    private lateinit var mViewModel: ResetPasswordViewModel
    private var _binding: ResetPasswordFragmentBinding? = null
    private val binding get() = _binding!!
    private var emailPhone: String = ""

    override fun getRootView(): View? {
        return ll_root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProvider(this).get(ResetPasswordViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        _binding = ResetPasswordFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        emailPhone = ResetPasswordFragmentArgs.fromBundle(requireArguments()).emailMobile

        setLoadingView(binding.resetLoading.root)

        initObserver(view)

        binding.resetPwdSubmitButton.setOnClickListener {
            hideKeyboard(it)
            if (TextUtils.isEmpty(binding.edtOtp.text.toString()) || TextUtils.isEmpty(binding.edtNewPassword.text.toString()) ||
                    TextUtils.isEmpty(binding.edtConfirmPassword.text.toString())) {
                showSnackbarError("Please fill all the required fields!")
            } else if (binding.edtConfirmPassword.text.toString() != binding.edtNewPassword.text.toString()) {
                showSnackbarError("Confirm Password and New Password should be same!")
            } else {
                showLoading()
                mViewModel.resetPassword(binding.edtOtp.text.toString(), emailPhone, binding.edtNewPassword.text.toString())
            }
        }

    }

    private fun initObserver(view: View) {
        mViewModel.resetPasswordMLiveData.observe(viewLifecycleOwner, {
            hideLoading()
            when (it.status) {
                Status.SUCCESS -> {
                    Log.i("OTP Fragment", "Response:${it.data}")
                    showSnackbarMessage(it.data?.message!!)
                    Navigation.findNavController(view).navigate(R.id.action_reset_fragment_to_login_fragment)
                }
                Status.AUTHORISATION -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.AUTHORISATION_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.BAD_REQUEST -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.NOT_FOUND -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.OTHER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.SERVER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                else -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
            }
        })
    }

}