package com.mamina.user.ui.bookings.model

data class BookingsResponse (
        var code: Int,
        var message: String,
        var status: Boolean,
        var data: Data
) {
    data class Data(
            var totalCount: Int,
            var pageSize: Int,
            var currentPage: Int,
            var totalPages: Int,
            var previousPage: String,
            var nextPage: String,
            var searchQuery: String,
            var dataList: MutableList<BookingInfoList>
    )
}