package com.mamina.user.ui.notification

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mamina.user.R
import com.mamina.user.models.response.GetNotificationData
import kotlinx.android.synthetic.main.item_notification.view.*
import java.text.SimpleDateFormat
import java.util.*

class NotificationAdapter : ListAdapter<GetNotificationData, NotificationAdapter.MiViewHolder>(
    ITEM_COMPARATOR
) {
    private var mItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onItemClick(item: GetNotificationData?, pos: Int)
    }

    fun setOnItemClickListener(mItemClickListener: OnItemClickListener) {
        this.mItemClickListener = mItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MiViewHolder {
        return MiViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_notification,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MiViewHolder, pos: Int) {
        var item = getItem(pos)
        val viewHolder = holder
        item?.let { itt ->
            viewHolder.bindView(itt, mItemClickListener)
        }

    }

    class MiViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindView(model: GetNotificationData, itemClickListener: OnItemClickListener?) {

            //                Glide.with(itemView.context)
//                    .load(WebUrl.BASE_FILE + model.fromUserProfilePic)
//                    .centerCrop()
//                    .skipMemoryCache(true)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(itemView.civ_doc)

//            if (!model.fromUserProfilePic.isNullOrEmpty()) {
//                Glide.with(itemView.context)
//                    .load(WebUrl.BASE_FILE + model.fromUserProfilePic)
//                    .centerCrop()
//                    .skipMemoryCache(true)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(itemView.civ_doc)
//            } else {
//                val bgColor = when (adapterPosition % 5) {
//                    0 -> Color.parseColor("#e5eef5")
//                    1 -> Color.parseColor("#eff8ff")
//                    2 -> Color.parseColor("#ffeffb")
//                    3 -> Color.parseColor("#dfebcc")
//                    4 -> Color.parseColor("#fff8ef")
//                    else -> R.color.colorLightestGrey
//                }
//                itemView.civ_doc?.setBackgroundColor(bgColor)
//            }


            model.text.let {
                itemView.tv_notification?.text = it
            }

            model.createdOn.let {

                val currentDate = Calendar.getInstance().time
                val df = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
                val notificationDate = df.parse(it)

                val diff = currentDate.time - notificationDate.time
                val seconds = diff / 1000
                val minutes = seconds / 60
                val hours = minutes / 60
                val days = hours / 24


//                val duration = it / 60
//
//                if (it < 60) {
//                    itemView.tv_duration?.text = "$it minute ago"
//                } else if (duration < 24) {
//                    if (duration > 1) {
//                        itemView.tv_duration?.text = "$duration hours ago"
//                    } else {
//                        itemView.tv_duration?.text = "$duration hour ago"
//                    }
//                } else {
//                    val days = it / (24 * 60)
//                    if (days > 1) {
//                        itemView.tv_duration?.text = "$days days ago"
//                    } else {
//                        itemView.tv_duration?.text = "$days day ago"
//                    }
//                }
            }

            itemView.iv_delete.setOnClickListener {
                itemClickListener?.onItemClick(model, adapterPosition)
            }

        }
    }

    companion object {
        private val ITEM_COMPARATOR = object : DiffUtil.ItemCallback<GetNotificationData>() {
            override fun areItemsTheSame(
                oldItem: GetNotificationData,
                newItem: GetNotificationData,
            ): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: GetNotificationData,
                newItem: GetNotificationData,
            ): Boolean = newItem == oldItem
        }
    }


}
