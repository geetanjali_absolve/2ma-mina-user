package com.mamina.user.ui.detail

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mamina.user.MaMinaApplication
import com.mamina.user.models.response.DistancePriceResponse
import com.mamina.user.models.response.WebResponse
import com.mamina.user.repository.remote.InitialRepository
import com.mamina.user.ui.base.BaseViewModel
import com.mamina.user.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class ParcelDetailViewModel(application: Application) : BaseViewModel(application) {
    var webService = (application as MaMinaApplication).getWebServiceInstance()
    var initialRepository: InitialRepository

    var distanceMutableLiveData: MutableLiveData<WebResponse<DistancePriceResponse>>

    init {
        initialRepository = InitialRepository(webService)
        distanceMutableLiveData = MutableLiveData()
    }

    @ExperimentalCoroutinesApi
    fun getDistancePrice(header: Map<String, String>,
                         senderLat: String,
                         senderLong: String,
                         recLat: String,
                         recLong: String,
                         deliveryTypeId: Int) {

        viewModelScope.launch {
            initialRepository.getLocationPrice(header, senderLat, senderLong, recLat, recLong, deliveryTypeId)
                    .onStart {

                    }.onCompletion {

                    }.catch { exception ->
                        distanceMutableLiveData.value = WebResponse(Status.OTHER_ERROR, null, exception.message)
                    }.collect {
                        distanceMutableLiveData.value = WebResponse(it.code, it, it.message)//WebResponse(it.code, it.data, it.message)
                    }
        }

    }
}