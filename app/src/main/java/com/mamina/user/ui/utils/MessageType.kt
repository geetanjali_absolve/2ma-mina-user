package com.mamina.user.ui.utils

/**
 * Created by Rohit Singh on 25,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
object MessageType {
    const val SUCCESS = 967
    const val ERROR = 620
    const val INFO = 63
}