package com.mamina.user.ui.otp

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.mamina.WebService
import com.mamina.user.MaMinaApplication
import com.mamina.user.models.response.ResendOtpResponse
import com.mamina.user.models.response.VerifyPhoneResponse
import com.mamina.user.models.response.WebResponse
import com.mamina.user.repository.remote.InitialRepository
import com.mamina.user.ui.base.BaseViewModel
import com.mamina.user.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class OtpViewModel(application: Application) : BaseViewModel(application) {
    var webService: WebService = (application as MaMinaApplication).getWebServiceInstance()
    var initialRepository: InitialRepository = InitialRepository(webService)

    var resendOtpMLiveData: MutableLiveData<WebResponse<ResendOtpResponse>> = MutableLiveData()
    var verifyOtpMLiveData: MutableLiveData<WebResponse<VerifyPhoneResponse>> = MutableLiveData()

    @ExperimentalCoroutinesApi
    fun resendOtpPhone(header: Map<String, String>, dialCode: String?, phone: String?) {

        val json = JsonObject()
        json.addProperty("dialCode", dialCode)
        json.addProperty("phoneNo", phone)

        viewModelScope.launch {
            initialRepository.resendOtpPhone(header, json)
                    .onStart {

                    }.onCompletion {

                    }.catch { exception ->
                        resendOtpMLiveData.value = WebResponse(Status.OTHER_ERROR, null, exception.message)

                    }.collect {
                        if(it.status)
                            resendOtpMLiveData.value= WebResponse(Status.SUCCESS, it, null)
                        else
                            resendOtpMLiveData.value= WebResponse(Status.SERVER_ERROR, null, it.message)
                    }
        }
    }

    @ExperimentalCoroutinesApi
    fun resendOtpEmail(header: Map<String, String>, email: String?) {

        val json = JsonObject()
        json.addProperty("email", email)

        viewModelScope.launch {
            initialRepository.resendOtpEmail(header, json)
                    .onStart {

                    }.onCompletion {

                    }.catch { exception ->
                        resendOtpMLiveData.value = WebResponse(Status.OTHER_ERROR, null, exception.message)

                    }.collect {
                        if(it.status)
                            resendOtpMLiveData.value= WebResponse(Status.SUCCESS, it, null)
                        else
                            resendOtpMLiveData.value= WebResponse(Status.SERVER_ERROR, null, it.message)
                    }
        }
    }


    @ExperimentalCoroutinesApi
    fun verifyEmailOtp(header: Map<String, String>,email: String, otp: String) {

        val json = JsonObject()
        json.addProperty("email", email)
        json.addProperty("otp", otp)

        viewModelScope.launch {
            initialRepository.verifyEmail(header, json)
                    .onStart {

                    }.onCompletion {

                    }.catch { exception ->
                        verifyOtpMLiveData.value = WebResponse(Status.OTHER_ERROR, null, exception.message)
                    }.collect {
                        verifyOtpMLiveData.value = WebResponse(it.code!!, it, it.message)
                    }
        }
    }

    @ExperimentalCoroutinesApi
    fun verifyPhoneOtp(header: Map<String, String>, code: String) {

        val json = JsonObject()
        json.addProperty("code", code)

        viewModelScope.launch {
            initialRepository.verifyPhone(header, json)
                    .onStart {

                    }.onCompletion {

                    }.catch { exception ->
                        verifyOtpMLiveData.value = WebResponse(Status.OTHER_ERROR, null, exception.message)
                    }.collect {
                        verifyOtpMLiveData.value = WebResponse(it.code!!, it, it.message)
                    }
        }
    }

}