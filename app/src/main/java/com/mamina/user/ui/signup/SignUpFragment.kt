package com.mamina.user.ui.signup

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.DatePicker
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.mamina.user.BuildConfig
import com.mamina.user.MaMinaApplication
import com.mamina.user.R
import com.mamina.user.constants.DocumentType
import com.mamina.user.databinding.SignUpFragmentBinding
import com.mamina.user.models.response.SignUpIndividualData
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.ui.utils.BaseTextWatcher
import com.mamina.user.ui.utils.CustomDropDownAdapter
import com.mamina.user.ui.utils.MessageType
import com.mamina.user.utils.CommonUtils
import com.mamina.user.utils.SharedPrefsHelper
import com.mamina.user.utils.Status
import com.yalantis.ucrop.UCrop
import java.io.File
import java.util.*

class SignUpFragment : BaseFragment() {
    private var _binding: SignUpFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var mViewModel: SignUpViewModel
    private val REQUEST_IMAGE_CAPTURE: Int = 200
    private val REQUEST_GALLERY_IMAGE: Int = 210
    private var fileName: String? = null
    private var snackBarPermission: Snackbar? = null
    private var setBitmapMaxWidthHeight: Boolean = false
    private val ASPECT_RATIO_X = 1
    private var ASPECT_RATIO_Y: Int = 1
    private var bitmapMaxWidth: Int = 1000
    private var bitmapMaxHeight: Int = 1000
    private val IMAGE_COMPRESSION = 80
    // image paths
    private var TYPE_SELECTION = 1
    private val LICENSE_IMAGE_REQUEST = 1
    private val VAT_IMAGE_REQUEST = 2
    private val CHAMBER_COMMERCE_IMAGE_REQUEST = 3
    private val AGREEMENT_IMAGE_REQUEST = 4
    private val ANY_PROOF_IMAGE_1_REQUEST = 6
    private val ANY_PROOF_IMAGE_2_REQUEST = 7
    var proofTypeId = 1
    private var sharedPrefs: SharedPrefsHelper? = null

    private var deviceToken: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = SignUpFragmentBinding.inflate(inflater, container, false)
        val view = binding.root

        setLoadingView(binding.signupLoading.root)

        val isSignUpType = arguments?.getBoolean("isSignUpType")
        // initialise
        mViewModel = ViewModelProvider(this).get(SignUpViewModel::class.java)
        sharedPrefs = (context?.applicationContext as MaMinaApplication).getSharedPrefInstance()

        initObserver(view)

        deviceToken = sharedPrefs?.builder()?.read(SharedPrefsHelper.KEY_DEVICE_ID, "")

        binding.ccpCountry.registerCarrierNumberEditText(binding.signUpPhoneEditText)

        binding.signUpPhoneEditText.addTextChangedListener(object : BaseTextWatcher() {
            override fun onTextChanged(start: Int, before: Int, count: Int, s: CharSequence?) {
                if (binding.ccpCountry.isValidFullNumber) {
                    binding.signUpPhoneEditText.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.ic_tick_green,
                            0
                    )
                } else {
                    binding.signUpPhoneEditText.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.ic_tick_red,
                            0
                    )
                }
            }
        })

        val proofAdapter = CustomDropDownAdapter(requireActivity(), DocumentType.list,"SignUp")
        binding.signUpAnyProofSpinner.adapter = proofAdapter

        binding.signUpAnyProofSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(viewGroup: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (DocumentType.list[position].value == "2") {
                    binding.signUpUploadAnyProof2EditText.visibility = View.VISIBLE
                } else {
                    binding.signUpUploadAnyProof2EditText.visibility = View.GONE
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        // hide the below fields for individual user
        if (isSignUpType == true) {
            binding.signUpBusinessLayout.visibility = View.GONE
            binding.signUpCompanyEditText.visibility = View.GONE
        } else {
            // for business user
            binding.signUpAnyProofSpinner.visibility = View.GONE
            binding.signUpUploadAnyProof1EditText.visibility = View.GONE
            binding.signUpUploadAnyProof2EditText.visibility = View.GONE
        }

        binding.signupDob.setOnClickListener {
            showDatePicker()
        }

        // add the click listener
        binding.signUpUploadLicenseEditText.setOnClickListener {
            TYPE_SELECTION = LICENSE_IMAGE_REQUEST
            if (Build.VERSION.SDK_INT >= 23) {
                Dexter.withActivity(activity)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                        .withListener(multiplePermissionsListener).check()
            } else {
                openImagePopupMenu()
            }
        }

        binding.signUpUploadVatEditText.setOnClickListener {
            TYPE_SELECTION = VAT_IMAGE_REQUEST
            if (Build.VERSION.SDK_INT >= 23) {
                Dexter.withActivity(activity)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                        .withListener(multiplePermissionsListener).check()
            } else {
                openImagePopupMenu()
            }
        }

        binding.signUpUploadChamberCommerceEditText.setOnClickListener {
            TYPE_SELECTION = CHAMBER_COMMERCE_IMAGE_REQUEST
            if (Build.VERSION.SDK_INT >= 23) {
                Dexter.withActivity(activity)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                        .withListener(multiplePermissionsListener).check()
            } else {
                openImagePopupMenu()
            }
        }

        binding.signUpUploadAgreementEditText.setOnClickListener {
            TYPE_SELECTION = AGREEMENT_IMAGE_REQUEST
            if (Build.VERSION.SDK_INT >= 23) {
                Dexter.withActivity(activity)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                        .withListener(multiplePermissionsListener).check()
            } else {
                openImagePopupMenu()
            }
        }

        binding.signUpUploadAnyProof1EditText.setOnClickListener {
            TYPE_SELECTION = ANY_PROOF_IMAGE_1_REQUEST
            if (Build.VERSION.SDK_INT >= 23) {
                Dexter.withActivity(activity)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                        .withListener(multiplePermissionsListener).check()
            } else {
                openImagePopupMenu()
            }
        }

        binding.signUpUploadAnyProof2EditText.setOnClickListener {
            TYPE_SELECTION = ANY_PROOF_IMAGE_2_REQUEST
            if (Build.VERSION.SDK_INT >= 23) {
                Dexter.withActivity(activity)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                        .withListener(multiplePermissionsListener).check()
            } else {
                openImagePopupMenu()
            }
        }

        binding.tvTermsOfUse.setOnClickListener {
            val actionSignUpFragmentToTermsConditionsFragment = SignUpFragmentDirections.actionSignUpFragmentToTermsFragment("Terms")
            Navigation.findNavController(view)
                    .navigate(actionSignUpFragmentToTermsConditionsFragment)
        }

        binding.tvPrivacyPolicy.setOnClickListener {
            val actionSignUpFragmentToTermsConditionsFragment = SignUpFragmentDirections.actionSignUpFragmentToTermsFragment("Policy")
            Navigation.findNavController(view)
                    .navigate(actionSignUpFragmentToTermsConditionsFragment)
        }

        binding.signUpSubmitButton.setOnClickListener {
            if (isSignUpType == true) {
                // for individual user
                Log.d(TAG, "onCreateView: Sign up is true")
                if (binding.signUpFnameEdittext.text.trim().isEmpty()) {
                    showSnackBar(MessageType.ERROR, "Please enter the first name", requireContext())
                } else if (binding.signUpLnameEditText.text.trim().isEmpty()) {
                    showSnackBar(MessageType.ERROR, "Please enter the last name", requireContext())
                } else if (binding.signUpEmailEditText.text.trim().isEmpty()) {
                    showSnackBar(MessageType.ERROR, "Please enter the email", requireContext())
                } else if (binding.signUpPhoneEditText.text.trim().isEmpty()) {
                    showSnackBar(MessageType.ERROR, "Please enter the phone number", requireContext())
                } else if (binding.signupDob.text.trim().isEmpty()) {
                    showSnackBar(MessageType.ERROR, "Please select DOB", requireContext())
                } else if (binding.signUpPasswordEditText.text.trim().isEmpty()) {
                    showSnackBar(MessageType.ERROR, "Please enter the password", requireContext())
                } else if (binding.signUpConfirmPasswordEditText.text.trim().isEmpty()) {
                    showSnackBar(MessageType.ERROR, "Please enter the confirm password", requireContext())
                } else if (binding.signUpPasswordEditText.text.trim() != binding.signUpConfirmPasswordEditText.text.trim()) {
                    showSnackBar(MessageType.ERROR, "Password and Confirm Password should be same", requireContext())
                } else if (binding.signUpUploadAnyProof1EditText.text.trim().isEmpty()) {
                    showSnackBar(MessageType.ERROR, "Please upload image of proof", requireContext())
                } else if (proofTypeId == 2) {
                    if (binding.signUpUploadAnyProof2EditText.text.trim().isEmpty()) {
                        showSnackBar(MessageType.ERROR, "Please upload image of proof", requireContext())
                    }
                } else {

                    proofTypeId = when (proofType) {
                        "ID Book" -> {
                            1
                        }
                        "ID Card" -> {
                            2
                        }
                        "Passport" -> {
                            3
                        }
                        else -> {
                            4
                        }
                    }

                    val fname = CommonUtils.prepareTextPart(binding.signUpFnameEdittext.text.toString().trim())
                    val lname = CommonUtils.prepareTextPart(binding.signUpLnameEditText.text.toString().trim())
                    // todo: remove the static dial code below
                    val dial = CommonUtils.prepareTextPart(binding.ccpCountry.selectedCountryCode)
                    val phone = CommonUtils.prepareTextPart(binding.signUpPhoneEditText.text.toString().trim())
                    val email = CommonUtils.prepareTextPart(binding.signUpEmailEditText.text.toString().trim())
                    val dob = CommonUtils.prepareTextPart(binding.signupDob.text.toString().trim())
                    val pass = CommonUtils.prepareTextPart(binding.signUpPasswordEditText.text.toString().trim())
                    // todo: remove the static user type
                    val user = CommonUtils.prepareTextPart("1")
                    // todo: modify the below static device type and device token
                    val device = CommonUtils.prepareTextPart("android")
                    val token = CommonUtils.prepareTextPart(deviceToken.toString())
                    val idProofTypeId = CommonUtils.prepareTextPart(proofTypeId.toString())
                    val idProof = CommonUtils.prepareFilePart(binding.signUpUploadAnyProof1EditText.text.toString().trim(), "IDProof")
                    val idCardBack = CommonUtils.prepareFilePart(binding.signUpUploadAnyProof2EditText.text.toString().trim(), "IDCardBack")

                    showLoading()
                    if (binding.signUpUploadAnyProof2EditText.text.isNullOrEmpty()) {
                        // todo: modify the below selfie pic
                        mViewModel.signUpIndividually(fname, lname, dial, phone, email,dob, pass, user,
                                device, token, idProofTypeId, idProof, null)
                    } else {
                        mViewModel.signUpIndividually(fname, lname, dial, phone, email,dob, pass, user,
                                device, token, idProofTypeId, idProof, idCardBack)
                    }
                }

            } else {
                // for business user
                if (binding.signUpFnameEdittext.text.trim().isEmpty() || binding.signUpLnameEditText.text.trim().isEmpty() ||
                        binding.signUpCompanyEditText.text.trim().isEmpty() || binding.signUpEmailEditText.text.trim().isEmpty() ||
                        binding.signUpPhoneEditText.text.trim().isEmpty() || binding.signUpPasswordEditText.text.trim().isEmpty() ||
                        binding.signUpConfirmPasswordEditText.text.trim().isEmpty() || binding.signUpWebsiteEditText.text.trim().isEmpty() ||
                        binding.signUpContactPersonEditText.text.trim().isEmpty() || binding.signUpLicenseNoEditText.text.trim().isEmpty() ||
                        binding.signUpVatEditText.text.trim().isEmpty() || binding.signUpExternalContractNumberEditText.text.trim().isEmpty() ||
                        binding.signUpUploadLicenseEditText.text.trim().isEmpty() || binding.signUpUploadVatEditText.text.trim().isEmpty() ||
                        binding.signUpUploadChamberCommerceEditText.text.trim().isEmpty() || binding.signUpUploadAgreementEditText.text.trim().isEmpty()) {
                    showSnackBar(MessageType.ERROR, "Please complete the required details!", requireContext())
                } else if (binding.signUpPasswordEditText.text.trim() != binding.signUpConfirmPasswordEditText.text.trim()) {
                    showSnackBar(MessageType.ERROR, "Password and Confirm Password should be same!", requireContext())
                } else {
                    val fname = CommonUtils.prepareTextPart(binding.signUpFnameEdittext.text.toString().trim())
                    val lname = CommonUtils.prepareTextPart(binding.signUpLnameEditText.text.toString().trim())
                    // todo: remove the static dial code below
                    val dial = CommonUtils.prepareTextPart(binding.ccpCountry.selectedCountryCode)
                    val phone = CommonUtils.prepareTextPart(binding.signUpPhoneEditText.text.toString().trim())
                    val email = CommonUtils.prepareTextPart(binding.signUpEmailEditText.text.toString().trim())
                    val pass = CommonUtils.prepareTextPart(binding.signUpPasswordEditText.text.toString().trim())
                    val website = CommonUtils.prepareTextPart(binding.signUpWebsiteEditText.text.toString().trim())
                    val contact = CommonUtils.prepareTextPart(binding.signUpContactPersonEditText.text.toString().trim())
                    val licenseNo = CommonUtils.prepareTextPart(binding.signUpLicenseNoEditText.text.toString().trim())
                    val vat = CommonUtils.prepareTextPart(binding.signUpVatEditText.text.toString().trim())
                    val external = CommonUtils.prepareTextPart(binding.signUpExternalContractNumberEditText.text.toString().trim())
                    // todo: remove the static user type
                    val user = CommonUtils.prepareTextPart("2")
                    // todo: modify the below static device type and device token
                    val device = CommonUtils.prepareTextPart("android")
                    val token = CommonUtils.prepareTextPart(deviceToken.toString())
                    val licenseFile = CommonUtils.prepareFilePart(binding.signUpUploadLicenseEditText.text.toString().trim(), "LicenceFilePath")
                    val vatFile = CommonUtils.prepareFilePart(binding.signUpUploadVatEditText.text.toString().trim(), "VATFilePath")
                    val commerceFile = CommonUtils.prepareFilePart(binding.signUpUploadChamberCommerceEditText.text.toString().trim(), "ChamberCommerceFilePath")
                    val agreementFile = CommonUtils.prepareFilePart(binding.signUpUploadAgreementEditText.text.toString().trim(), "AgreementFilePath")

                    showLoading()
                    mViewModel.signUpBusiness(fname, lname, dial, phone, email, pass, website, contact, licenseNo, vat, external,
                            user, device, token, licenseFile, vatFile, commerceFile, agreementFile)

                }
            }
        }

        return view
    }

    private fun initObserver(view: View) {
        mViewModel.resultSignUpIndividualUser.observe(viewLifecycleOwner, {
            hideLoading()
            when (it.status) {
                Status.SUCCESS -> {
                    saveUserInfo(it.data?.data)
                    val actionSignUpToOtpFragment = SignUpFragmentDirections.actionSignUpFragmentToOtpFragment(it.data?.data?.phoneNumber.toString())
                    actionSignUpToOtpFragment.emailMobile = it.data?.data?.phoneNumber.toString()
                    actionSignUpToOtpFragment.showEmailOtp = false
                    Navigation.findNavController(view).navigate(actionSignUpToOtpFragment)
                }
                Status.AUTHORISATION -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.AUTHORISATION_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.BAD_REQUEST -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.NOT_FOUND -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.OTHER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.SERVER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                else -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
            }
        })

        mViewModel.resultSignUpBusinessUser.observe(viewLifecycleOwner, {
            hideLoading()
            when (it.status) {
                Status.SUCCESS -> {
                    saveUserInfo(it.data?.data)
                    showSnackBar(MessageType.ERROR, it.data?.docuploadmessage, requireContext())
                }
                Status.AUTHORISATION -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.AUTHORISATION_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.BAD_REQUEST -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.NOT_FOUND -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.OTHER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                Status.SERVER_ERROR -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                else -> showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
            }
        })
    }

    private fun saveUserInfo(data: SignUpIndividualData?) {
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_FIRST_NAME, data?.firstName.toString())?.build()
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_LAST_NAME, data?.lastName.toString())?.build()
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_DIAL_CODE, data?.dialCode.toString())?.build()
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_PHONE, data?.phoneNumber.toString())?.build()
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_EMAIL, data?.email.toString())?.build()
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_USER_TYPE, data?.userTypeId.toString())?.build()
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_ACCESS_TOKEN, data?.accessToken.toString())?.build()

    }

    companion object {
        private val TAG = SignUpFragment.javaClass.simpleName
        fun newInstance(): SignUpFragment {
            return SignUpFragment()
        }

        var proofType = "ID Book"
    }

    override fun getRootView(): View? {
        return view
    }

    private fun setEditTextByType(typeSelection: Int, imagePath: String) {
        Log.d(TAG, "setEditTextByType: Type is: $typeSelection")
        when (typeSelection) {
            LICENSE_IMAGE_REQUEST -> {
                binding.signUpUploadLicenseEditText.setText(imagePath)
            }
            VAT_IMAGE_REQUEST -> {
                binding.signUpUploadVatEditText.setText(imagePath)
            }
            CHAMBER_COMMERCE_IMAGE_REQUEST -> {
                binding.signUpUploadChamberCommerceEditText.setText(imagePath)
            }
            AGREEMENT_IMAGE_REQUEST -> {
                binding.signUpUploadAgreementEditText.setText(imagePath)
            }
            /*SELFIE_IMAGE_REQUEST -> {
                binding.signUpUploadSelfieEditText.setText(imagePath)
            }*/
            ANY_PROOF_IMAGE_1_REQUEST -> {
                binding.signUpUploadAnyProof1EditText.setText(imagePath)
            }
            ANY_PROOF_IMAGE_2_REQUEST -> {
                binding.signUpUploadAnyProof2EditText.setText(imagePath)
            }
        }
    }

    private var multiplePermissionsListener: MultiplePermissionsListener =
            object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        openImagePopupMenu()
                    }
                    if (report.isAnyPermissionPermanentlyDenied) {
                        snackBarPermission = Snackbar.make(
                                getRootView()!!,
                                getString(R.string.title_camera_and_storage_access),
                                Snackbar.LENGTH_INDEFINITE
                        )
                                .setAction(getString(R.string.action_settings)) {
                                    // open system setting screen ...
                                    val intent = Intent()
                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                    val uri = Uri.fromParts("package", activity?.packageName, null)
                                    intent.data = uri
                                    activity?.startActivity(intent)
                                }
                        snackBarPermission?.show()
                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                        permissions: MutableList<PermissionRequest>,
                        token: PermissionToken,
                ) {
                    token.continuePermissionRequest()
                }
            }

    private fun openImagePopupMenu() {
        val dialog = Dialog(requireContext())
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Choose Image Source")
        builder.setItems(arrayOf<CharSequence?>("Camera", "Gallery")) { dialogInterface, i ->
            when (i) {
                0 -> openCamera()
                1 -> openGallery()
            }
        }
        builder.show()
        dialog.dismiss()
    }

    private fun openCamera() {
        fileName = "IMG_" + System.currentTimeMillis().toString() + ".jpg"
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath())
        if (activity?.packageManager?.let { intent.resolveActivity(it) } != null) {
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
        }
    }

    private fun getCacheImagePath(): Uri? {
        val path = File(activity?.externalCacheDir, "camera")
        if (!path.exists()) path.mkdirs()
        val image = File(path, fileName)
        return activity?.let {
            FileProvider.getUriForFile(Objects.requireNonNull(requireContext()),
                BuildConfig.APPLICATION_ID + ".provider", image);


        }
    }

    private fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, REQUEST_GALLERY_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_IMAGE_CAPTURE -> if (resultCode == Activity.RESULT_OK) {
                cropImage(getCacheImagePath())
            }
            REQUEST_GALLERY_IMAGE -> if (resultCode == Activity.RESULT_OK) {
                val imageUri = data?.data
                cropImage(imageUri)
            }
            UCrop.REQUEST_CROP -> if (resultCode == Activity.RESULT_OK) {
                handleUCropResult(data)
            }
            UCrop.RESULT_ERROR -> {
                val cropError = UCrop.getError(data!!)
                //  Log.e("TAG", "Crop error: $cropError")
                showSnackbarError("Crop error: $cropError")
            }
        }
    }

    private fun cropImage(sourceUri: Uri?) {
        val destinationUri = Uri.fromFile(
                File(
                        activity?.cacheDir,
                        queryName(activity?.contentResolver!!, sourceUri!!)
                )
        )
        val options = UCrop.Options()
        options.setCompressionQuality(IMAGE_COMPRESSION)
        options.setToolbarColor(ContextCompat.getColor(requireContext(), R.color.primary_color))
        options.setStatusBarColor(ContextCompat.getColor(requireContext(), R.color.primary_color))
        options.setActiveWidgetColor(ContextCompat.getColor(requireContext(),
                R.color.primary_color))
        options.withAspectRatio(ASPECT_RATIO_X.toFloat(), ASPECT_RATIO_Y.toFloat())
        if (setBitmapMaxWidthHeight) options.withMaxResultSize(bitmapMaxWidth, bitmapMaxHeight)
        UCrop.of(sourceUri, destinationUri).withOptions(options).start(requireActivity(), this)
    }

    private fun handleUCropResult(data: Intent?) {
        if (data == null) {
            return
        }
        val resultUri = UCrop.getOutput(data)
        Log.e("TAG", "Crop : $resultUri")

        setEditTextByType(TYPE_SELECTION, resultUri?.path.toString())
    }

    private fun queryName(resolver: ContentResolver, uri: Uri): String? {
        val returnCursor: Cursor = resolver.query(uri, null, null, null, null)!!
        val nameIndex: Int = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        val name: String = returnCursor.getString(nameIndex)
        returnCursor.close()
        return name
    }

    private fun showDatePicker() {
        val initYear: Int
        val initMonth: Int
        val initDay: Int
        val c = Calendar.getInstance()
        val selectedDob: String = binding.signupDob.text.toString()
        if (TextUtils.isEmpty(selectedDob)) {
            initYear = c[Calendar.YEAR]
            initMonth = c[Calendar.MONTH]
            initDay = c[Calendar.DAY_OF_MONTH]
        } else {
            val userSelected = selectedDob.split("-".toRegex()).toTypedArray()
            initYear = userSelected[2].toInt()
            initDay = userSelected[0].toInt()
            initMonth = userSelected[1].toInt() - 1
        }
        val datePickerDialog: DatePickerDialog
        datePickerDialog = DatePickerDialog(
                requireActivity(), R.style.PickerStyle,
                { view1: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                    val dayOfMonthh = String.format(Locale.getDefault(), "%02d", dayOfMonth)
                    val monthOfYearr = String.format(Locale.getDefault(), "%02d", monthOfYear + 1)
                    val x = "$dayOfMonthh-$monthOfYearr-$year"
                    binding.signupDob.setText(x)
                    binding.signupDob.setCompoundDrawablesWithIntrinsicBounds(
                            R.drawable.ic_calendar,
                            0,
                            R.drawable.ic_tick_green,
                            0
                    )
                },
                initYear,
                initMonth,
                initDay
        )
        datePickerDialog.datePicker.maxDate = c.timeInMillis - 568025136000L // -18yrs
        datePickerDialog.setOnShowListener {
            val greenColor = ContextCompat.getColor(activity as Context, R.color.primary_color)
            datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(greenColor)
            datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(greenColor)
        }
        datePickerDialog.show()
    }


    override fun onDestroy() {
        _binding = null
        if (snackBarPermission != null) {
            if (snackBarPermission!!.isShown) {
                snackBarPermission?.dismiss()
            }
        }
        super.onDestroy()
    }
}