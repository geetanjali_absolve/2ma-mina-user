package com.mamina.user.ui.notification

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.mamina.user.MaMinaApplication
import com.mamina.user.R
import com.mamina.user.constants.ApiConstants
import com.mamina.user.models.response.GetNotificationData
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.utils.SharedPrefsHelper
import com.mamina.user.utils.Status
import kotlinx.android.synthetic.main.notification_fragment.*

class NotificationFragment : BaseFragment() {
    private lateinit var viewModel: NotificationViewModel
    private lateinit var notificationAdapter: NotificationAdapter
    val headerMap = HashMap<String, String>()
    private var sharedPrefs: SharedPrefsHelper? = null

    override fun getRootView(): View? {
        return fl_root_notification
    }

    private var accessToken: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(NotificationViewModel::class.java)
        notificationAdapter = NotificationAdapter()

        sharedPrefs = (context?.applicationContext as MaMinaApplication).getSharedPrefInstance()

        accessToken = sharedPrefs?.builder()?.read(SharedPrefsHelper.KEY_ACCESS_TOKEN, "").toString()


        headerMap[ApiConstants.KEY_AUTHORIZATION] = "Bearer $accessToken"

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.notification_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()

        initObserver()

        viewModel.getUserNotifications(headerMap)

    }

    private fun initRecyclerView() {
        rv_notification?.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            adapter = notificationAdapter
        }

        notificationAdapter.setOnItemClickListener(object :
                NotificationAdapter.OnItemClickListener {
            override fun onItemClick(item: GetNotificationData?, pos: Int) {
                viewModel.itemToBeDeleted = pos
                viewModel.deleteNotification(headerMap, item!!.id.toString())
            }
        })

        fbtn_delete.setOnClickListener {
            viewModel.deleteNotification(headerMap, "")
        }
    }

    private fun initObserver() {
        viewModel.isLoading.observe(viewLifecycleOwner,
                Observer {
                    if (it) progress_bar?.show()
                    else progress_bar?.hide()
                })

        viewModel.isListEmpty.observe(viewLifecycleOwner,
                Observer {
                    if (it)
                        group_empty_view?.visibility = View.VISIBLE
                    rv_notification?.visibility = View.GONE
                    fbtn_delete?.visibility = View.INVISIBLE
                })


        viewModel.getUserNotificationMLiveData.observe(viewLifecycleOwner, {
            Log.i("Home Fragment", "Response:${it.status}")

            when (it.status) {
                Status.SUCCESS -> {
                    Log.i("Home Fragment", "Response:${it.data}")
                    notificationAdapter.submitList(it.data?.data!!)
                    viewModel.listOfNotifications.value = it.data?.data
                }
                Status.AUTHORISATION -> showSnackbarError(it.errorMsg.toString())
                Status.AUTHORISATION_ERROR -> showSnackbarError(it.errorMsg.toString())
                Status.BAD_REQUEST -> showSnackbarError(it.errorMsg.toString())
                Status.NOT_FOUND -> {
                    group_empty_view.visibility = View.VISIBLE
                    notificationAdapter.submitList(null)
                }
                Status.OTHER_ERROR -> showSnackbarError(it.errorMsg.toString())
                Status.SERVER_ERROR -> showSnackbarError(it.errorMsg.toString())
                else -> showSnackbarError(it.errorMsg.toString())
            }
        })

        viewModel.resultDeleteNotification.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    val tempList = notificationAdapter.currentList.toMutableList()
                    if (tempList.isNotEmpty() && tempList.size > viewModel.itemToBeDeleted) {
                        tempList.removeAt(viewModel.itemToBeDeleted)
                        viewModel.listOfNotifications.value = tempList
                        viewModel.itemToBeDeleted = 100000
                        showSnackbarMessage(it.data?.message.toString())
                    }
                }
                Status.AUTHORISATION -> showSnackbarError(it.errorMsg.toString())
                Status.AUTHORISATION_ERROR -> showSnackbarError(it.errorMsg.toString())
                Status.BAD_REQUEST -> showSnackbarError(it.errorMsg.toString())
                Status.NOT_FOUND -> showSnackbarError(it.errorMsg.toString())
                Status.OTHER_ERROR -> showSnackbarError(it.errorMsg.toString())
                Status.SERVER_ERROR -> showSnackbarError(it.errorMsg.toString())
                else -> showSnackbarError(it.errorMsg.toString())
            }
        })

        viewModel.listOfNotifications.observe(viewLifecycleOwner, Observer {
            Log.d("TAG", "initObserver: "+it)
            if (it.isNotEmpty()) {
                group_empty_view.visibility = View.GONE
                notificationAdapter.submitList(it)
            } else {
                group_empty_view.visibility = View.VISIBLE
                notificationAdapter.submitList(null)
            }
        })

    }

}