package com.mamina.user.ui.home

import android.Manifest
import android.app.Activity
import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.directions.route.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.internal.ConnectionCallbacks
import com.google.android.gms.common.api.internal.OnConnectionFailedListener
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.mamina.user.BuildConfig
import com.mamina.user.MaMinaApplication
import com.mamina.user.MainActivity
import com.mamina.user.R
import com.mamina.user.constants.ApiConstants
import com.mamina.user.constants.ParcelType
import com.mamina.user.databinding.FragmentHomeBinding
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.ui.utils.MaMinaDialogFragment
import com.mamina.user.ui.utils.MessageType
import com.mamina.user.utils.SharedPrefsHelper
import com.mamina.user.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.IOException
import java.util.*
import kotlin.collections.HashMap


// API Key: AIzaSyDtv5XEoWvtFJdRs8ufcI_yj9JClT69NTU

class HomeFragment : BaseFragment(), OnMapReadyCallback, LocationListener,
    OnConnectionFailedListener, ConnectionCallbacks, RoutingListener {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var googleMap: GoogleMap
    private lateinit var geocoder: Geocoder
    private lateinit var navHomeToParcelDetailAction: View.OnClickListener
    private lateinit var currentLocationFab: FloatingActionButton
    private var senderLatLng: LatLng? = LatLng(32.2694705, 75.6343459)//null
    private var receiverLatLng: LatLng? = LatLng(30.6858989, 76.7215911)
    private val locationPermissionGranted = false
    private val polylineOptions = PolylineOptions()
    private var polylines: ArrayList<Polyline>? = null
    private val SENDER_EDIT_TEXT_REQUEST_CODE = 22
    private val RECEIVER_EDIT_TEXT_REQUEST_CODE = 67
    private var deliveryNormalPrice: Int = 0
    private var deliveryExpressPrice: Int = 0
    private var deliveryBakkiePrice: Int = 0
    private lateinit var normalDesc: String
    private lateinit var expressDesc: String
    private lateinit var bakkieDesc: String
    private val startMarker = MarkerOptions()
    private val endMarker = MarkerOptions()
    private var sharedPrefs: SharedPrefsHelper? = null
    var address: String = ""

    /*
     * ENTRY POINT TO THE FUSED LOCATION PROVIDER API
     * FusedLocationProviderClient - Main class for receiving location updates
     * LocationRequest - Requirements for the location updates, i.e. how often
     * you should receive updates, the priority, etc.
     */
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationManager: LocationManager

    /*
     * LocationCallback - Called when FusedLocationProviderClient has a new Location.
     */
    private val locationCallback: LocationCallback? = null

    /*
     * Used only for local storage of the last known location. Usually, this would be saved to your
     * database, but because this is a simplified sample without a full database, we only need the
     * last location to create a Notification if the user navigates away from the app.
     * */
    private val currentLocation: Location? = null

    // A default location and default zoom to use when location permission is not granted
    private val mDefaultLocation: LatLng = LatLng(-33.8523341, 151.2106085)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!Places.isInitialized()) {
            Places.initialize(requireContext(), getString(R.string.map_key), Locale.US)
        }
        // fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity)

    }

    @ExperimentalCoroutinesApi
    private fun initObservers() {
        val accessToken = sharedPrefs?.read(SharedPrefsHelper.KEY_ACCESS_TOKEN, "").toString()
        val hashMap: HashMap<String, String> = HashMap()
        hashMap[ApiConstants.KEY_AUTHORIZATION] = "Bearer $accessToken"

        homeViewModel.getPriceDetail(hashMap)
        homeViewModel.priceDetailMutableLiveData?.observe(requireActivity(), {

            when (it.status) {
                Status.SUCCESS -> {
                    it.data?.let { it1 ->
                        binding.tvDeliveryNormal.text = it1.data[0].deliveryType
                        binding.tvDeliveryTime.text = it1.data[0].description
                        normalDesc = it1.data[0].description
                        deliveryNormalPrice = it1.data[0].price
                        binding.tvDeliveryExpress.text = it1.data[1].deliveryType
                        binding.tvDeliveryTimeExpress.text = it1.data[1].description
                        expressDesc = it1.data[1].description
                        deliveryExpressPrice = it1.data[1].price
                        binding.tvDeliveryBakkie.text = it1.data[2].deliveryType
                        binding.tvDeliveryTimeBakkie.text = it1.data[2].description
                        bakkieDesc = it1.data[2].description
                        deliveryBakkiePrice = it1.data[2].price
                    }
                }
                else -> {
                    showSnackBar(MessageType.ERROR, it.errorMsg, requireContext())
                }
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root = binding.root
        setLoadingView(binding.homeLoading.root)

        geocoder = Geocoder(activity, Locale.getDefault())

        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireActivity())

        sharedPrefs = (context?.applicationContext as MaMinaApplication).getSharedPrefInstance()

        val mapFragment =
            childFragmentManager.findFragmentById(R.id.google_map) as SupportMapFragment // findFragmentById(R.id.google_map)

        mapFragment.getMapAsync(this)

        mapFragment.getMapAsync { googleMap: GoogleMap? ->
            // when map is loaded
            googleMap?.setOnMapClickListener { latLng: LatLng? ->
                // when user will tap on map, we will create marker
                val markerOptions = MarkerOptions()
                // set position of marker
                markerOptions.position(latLng!!)
                // set title of marker
                markerOptions.title(latLng.latitude.toString() + " : " + latLng.longitude)
                // remove all marker
                googleMap.clear()
                // zoom animate to marker
                googleMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        latLng, 10f
                    )
                )
                // add marker on map
                googleMap.addMarker(markerOptions)
                // set google map config based on our requirement
                googleMap.isMyLocationEnabled = true
                googleMap.isTrafficEnabled = false
                googleMap.isBuildingsEnabled = false

                googleMap.resetMinMaxZoomPreference()
                googleMap.uiSettings.isZoomGesturesEnabled = false
                googleMap.uiSettings.isZoomControlsEnabled = false
                googleMap.uiSettings.isMyLocationButtonEnabled = true
            }
        }
        // initialisation
        currentLocationFab = root.findViewById(R.id.floatingActionButton)
        polylineOptions.color(resources.getColor(R.color.primary_color))
        polylineOptions.width(5f)

        initObservers()
        getLocation()

        // below are click listeners
        binding.normalInfoImg.setOnClickListener {
            MaMinaDialogFragment.newInstance(ParcelType.NORMAL_DELIVERY)
                .show(requireActivity().supportFragmentManager, "normalDialog")
        }
        binding.expressInfoImg.setOnClickListener {
            MaMinaDialogFragment.newInstance(ParcelType.EXPRESS_DELIVERY)
                .show(requireActivity().supportFragmentManager, "expressDialog")
        }
        binding.bakkieInfoImg.setOnClickListener {
            MaMinaDialogFragment.newInstance(ParcelType.BAKKIE_DELIVERY)
                .show(requireActivity().supportFragmentManager, "bakkieDialog")
        }
        navHomeToParcelDetailAction =
            Navigation.createNavigateOnClickListener(R.id.action_nav_home_to_nav_parcel_detail)

        currentLocationFab.setOnClickListener {

            getLocationPermission()
            // check permission

        }
        binding.normalLayout.setOnClickListener {
            if (binding.homeSenderEditText.text.isNullOrEmpty() || binding.homeReceiverEditText.text.isNullOrEmpty()) {
                showSnackBar(
                    MessageType.ERROR,
                    "Please enter receiver address to proceed further!",
                    requireContext()
                )
            } else {
                (activity as MainActivity).orderRequest.DeliveryTypeId = 1
                (activity as MainActivity).orderRequest.deliveryPrice = deliveryNormalPrice
                (activity as MainActivity).orderRequest.description = normalDesc

                Navigation.findNavController(it).navigate(R.id.action_nav_home_to_nav_parcel_detail)
            }
        }
        binding.expressLayout.setOnClickListener {
            if (binding.homeSenderEditText.text.isNullOrEmpty() || binding.homeReceiverEditText.text.isNullOrEmpty()) {
                showSnackBar(
                    MessageType.ERROR,
                    "Please enter receiver address to proceed further!",
                    requireContext()
                )
            } else {
                (activity as MainActivity).orderRequest.DeliveryTypeId = 2
                (activity as MainActivity).orderRequest.deliveryPrice = deliveryExpressPrice
                (activity as MainActivity).orderRequest.description = expressDesc

                Navigation.findNavController(it).navigate(R.id.action_nav_home_to_nav_parcel_detail)
            }
        }
        binding.bakkieLayout.setOnClickListener {
            if (binding.homeSenderEditText.text.isNullOrEmpty() || binding.homeReceiverEditText.text.isNullOrEmpty()) {
                showSnackBar(
                    MessageType.ERROR,
                    "Please enter receiver address to proceed further!",
                    requireContext()
                )
            } else {
                (activity as MainActivity).orderRequest.DeliveryTypeId = 3
                (activity as MainActivity).orderRequest.deliveryPrice = deliveryBakkiePrice
                (activity as MainActivity).orderRequest.description = bakkieDesc

                Navigation.findNavController(it).navigate(R.id.action_nav_home_to_nav_parcel_detail)
            }
        }

        binding.homeSenderEditText.setOnClickListener {
            getPlaces(SENDER_EDIT_TEXT_REQUEST_CODE)
        }

        binding.homeReceiverEditText.setOnClickListener {
            getPlaces(RECEIVER_EDIT_TEXT_REQUEST_CODE)
        }

        return root
    }

    private fun getLocationPermission() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            // when permission granted
            getLocation()
        } else {
            // when permission denied
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf<String?>(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    val builder = AlertDialog.Builder(requireContext())
                    builder.setTitle("Confirm")
                    builder.setMessage("Are you sure to close app?")
                    builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                        requireActivity().finishAffinity()
                        System.exit(0)
                    }
                    builder.setNegativeButton(android.R.string.no) { dialog, which ->
                    }
                    builder.show()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    private fun getPlaces(requestCode: Int) {
        Log.d(TAG, "onClick tapped: ")
        if (!Places.isInitialized()) {
            Places.initialize(requireContext(), BuildConfig.MAP_KEY)
        }
        val fields = listOf(
            Place.Field.ID,
            Place.Field.NAME,
            Place.Field.LAT_LNG,
            Place.Field.ADDRESS_COMPONENTS
        )


        Log.d(TAG, "onCreateView: HomeFragment: ")
        Log.d(TAG, "onCreateView: Fields: " + fields.toList().toString())
        Log.d(TAG, "Fields Name: " + fields[0].name)
        Log.d(TAG, "Fields: Describe Contents: " + fields[0].describeContents())
        val intent =
            Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
//                    .setLocationBias()
                .build(requireContext())

        startActivityForResult(intent, requestCode)
    }

    private fun getLocation() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationProviderClient.lastLocation.addOnCompleteListener { task ->
            // initialise location
            val location = task.result
            if (location != null) {
                // initialise geocoder
                geocoder = Geocoder(activity, Locale.getDefault())
                // initiate address list
                try {
                    if (address == "") {
                        val addresses =
                            geocoder.getFromLocation(location.latitude, location.longitude, 1)
                        val senderAddress = addresses[0].getAddressLine(0)

                        binding.homeSenderEditText.setText(senderAddress)


                        googleMap.animateCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                LatLng(
                                    location.latitude,
                                    location.longitude
                                ), 13.0F
                            )
                        )
                        val cameraPosition: CameraPosition = CameraPosition.Builder()
                            .target(
                                LatLng(
                                    location.latitude,
                                    location.longitude
                                )
                            ) // Sets the center of the map to location user
                            .zoom(10f) // Sets the zoom
                            .bearing(90f) // Sets the orientation of the camera to east
                            .tilt(20f) // Sets the tilt of the camera to 30 degrees
                            .build() // Creates a CameraPosition from the builder

                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                        senderLatLng = LatLng(location.latitude, location.longitude)

                        (activity as MainActivity).orderRequest.SenderPlaceId = ""
                        (activity as MainActivity).orderRequest.SenderAddress = senderAddress
                        (activity as MainActivity).orderRequest.SenderLat =
                            location.latitude.toString()
                        (activity as MainActivity).orderRequest.SenderLong =
                            location.longitude.toString()
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onMapReady(gMap: GoogleMap) {
        googleMap = gMap

        // declare bounds object to fit whole route in screen
        val LatLongB = LatLngBounds.builder()


        // add markers
        val sydney = LatLng(-34.0, 151.0)
        val opera = LatLng(-33.9320447, 151.1597271)
        googleMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        googleMap.addMarker(MarkerOptions().position(opera).title("Opera House"))


        val url = getUrl(sydney, opera)
        /*async {
            val result = URL(url).readText()
            uiThread {
                // When API call is done, create parser and convert into JsonObjec
                val parser: Parser = Parser()
                val stringBuilder: StringBuilder = StringBuilder(result)
                val json: JsonObject = parser.parse(stringBuilder) as JsonObject
                // get to the correct element in JsonObject
                val routes = json.array<JsonObject>("routes")
                val points = routes!!["legs"]["steps"][0] as JsonArray<JsonObject>
                // For every element in the JsonArray, decode the polyline string and pass all points to a List
                val polypts = points.flatMap { decodePoly(it.obj("polyline")?.string("points")!!) }
                // Add  points to polyline and bounds
                polylineOptions.add(sydney)
                LatLongB.include(sydney)
                for (point in polypts) {
                    options.add(point)
                    LatLongB.include(point)
                }
                options.add(opera)
                LatLongB.include(opera)
                // build bounds
                val bounds = LatLongB.build()
                // add polyline to the map
                mMap!!.addPolyline(options)
                // show map with route centered
                mMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
            }
        }*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_CANCELED) {
            if (requestCode == SENDER_EDIT_TEXT_REQUEST_CODE) {
                if (resultCode == RESULT_OK) {
                    val place = data?.let { Autocomplete.getPlaceFromIntent(it) }
                    Log.i(
                        TAG,
                        "Place: " + place?.name + ", " + place?.id + ", " + place?.address + ", " + place?.addressComponents
                    )
                    val latLng = place?.latLng
                    val addresses =
                        latLng?.latitude?.let { geocoder.getFromLocation(it, latLng.longitude, 1) }
                    address = addresses?.get(0)?.getAddressLine(0)!!//place?.addressComponents
                    binding.homeSenderEditText.setText(address)

                    //order request data
                    (activity as MainActivity).orderRequest.SenderPlaceId = place?.id.toString()
                    (activity as MainActivity).orderRequest.SenderAddress = address.toString()
                    (activity as MainActivity).orderRequest.SenderLat =
                        place?.latLng?.latitude.toString()
                    (activity as MainActivity).orderRequest.SenderLong =
                        place?.latLng?.longitude.toString()

                    if (polylines != null) {
                        googleMap.clear()


                        for (line in polylines!!) {
                            line.remove()
                        }
                        polylines!!.clear()
                        Log.d(TAG, "onActivityResult: not null")

                    }else{
                        Log.d(TAG, "onActivityResult: null")
                    }

                    senderLatLng = place?.latLng

                    googleMap.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            latLng, 10f
                        )
                    )


                    Log.i(TAG, "Sender Lat Long is " + senderLatLng.toString())
                }
            } else if (requestCode == RECEIVER_EDIT_TEXT_REQUEST_CODE) {
                if (resultCode == RESULT_OK) {
                    val place = data?.let { Autocomplete.getPlaceFromIntent(it) }
                    Log.i(
                        TAG,
                        "Place: " + place?.name + ", " + place?.id + ", " + place?.address + ", " + place?.addressComponents
                    )
                    Log.i(
                        TAG,
                        "onActivityResult: Rec Latitude-Longitude: " + place?.latLng.toString()
                    )
                    val latLng = place?.latLng
                    val addresses =
                        latLng?.latitude?.let { geocoder.getFromLocation(it, latLng.longitude, 1) }
                    val address = addresses?.get(0)?.getAddressLine(0)//place?.addressComponents
                    binding.homeReceiverEditText.setText(address)

                    //order request data
                    (activity as MainActivity).orderRequest.ReceiverAddress = address.toString()
                    (activity as MainActivity).orderRequest.ReceiverPlaceId = place?.id.toString()
                    (activity as MainActivity).orderRequest.ReceiverLat =
                        place?.latLng?.latitude.toString()
                    (activity as MainActivity).orderRequest.ReceiverLong =
                        place?.latLng?.longitude.toString()
                    receiverLatLng = place?.latLng
                    route()
                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // todo: handle the error
                val status = data?.let { Autocomplete.getStatusFromIntent(it) }
                Log.d(TAG, "onActivityResult: Error occurred: " + status?.statusMessage)
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // the user cancelled the operation

            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            when {
                grantResults.isEmpty() -> {
                    /*
                     * if user interaction was interrupted, the permission request is cancelled and you receive empty arrays
                     */
                }
                grantResults[0] == PackageManager.PERMISSION_GRANTED -> {
                    /* permission granted */
                }
                else -> {
                    // Permission denied

                    // Notify the user via a snackbar that they have rejected the core permission for the ap,
                    // which makes the Activity useless. In a real app, core permissions would typically be best
                    // requested during a welcome-screen flow.

                    // Additionally, it is important to remember that a permission might have been rejected
                    // without asking the user for permission (device policy or "Never ask again" prompt). Therefore,
                    // a user interface affordance is typically implemented when permissions are denied. Otherwise,
                    // your app could appear unresponsive to touches or interactions which have required permissions.
                    Log.d(
                        TAG,
                        "onRequestPermissionsResult: Location permission denied. Please go to Settings to enable the permission"
                    )


                    /*showSnackBar("Location permission denied. Please go to Settings to enable the permission", "Go",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    // Build intent that displays the App settings screen.
                                    Intent intent = new Intent();
                                    intent.setAction(
                                            Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package",
                                            BuildConfig.APPLICATION_ID, null);
                                    intent.setData(uri);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            });*/
                }
            }
        }
    }

    private fun getUrl(from: LatLng, to: LatLng): String {
        val origin = "origin=" + from.latitude + "," + from.longitude
        val destination = "destination=" + to.latitude + "," + to.longitude
        val sensor = "sensor=false"
        val params = "$origin&$destination&$sensor"
        return "https://maps.googleapis.com/maps/api/directions/json?$params"
    }

    override fun onLocationChanged(location: Location) {}
    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onProviderDisabled(provider: String) {}

    override fun getRootView(): View? {
        return view;
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    companion object {
        private val TAG = HomeFragment::class.java.simpleName
        private const val LOCATION_PERMISSION_REQUEST_CODE = 822
        private const val DEFAULT_ZOOM = 15

        fun newInstance(): HomeFragment? {
            return HomeFragment()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    fun route() {
        if (senderLatLng == null || receiverLatLng == null) {
            if (senderLatLng == null) {
                if (binding.homeSenderEditText.length() > 0) {
                    showSnackBar(MessageType.ERROR, "Please choose your location", requireContext())
                }
            }
            if (receiverLatLng == null) {
                if (binding.homeReceiverEditText.text.isNotEmpty()) {
                    showSnackBar(
                        MessageType.ERROR,
                        "Please choose a destination.",
                        requireContext()
                    )
                }
            }
        } else {
            val routing = Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .waypoints(senderLatLng, receiverLatLng)
                .key(BuildConfig.MAP_KEY)
                .build()
            routing.execute()
        }
    }

    override fun onResume() {
        super.onResume()
        getLocationPermission()

    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("\n On connection failed")
    }

    override fun onRoutingFailure(routeExc: RouteException?) {
        showSnackBar(MessageType.ERROR, routeExc?.localizedMessage, requireContext())
    }

    override fun onRoutingStart() {

    }

    override fun onRoutingSuccess(routeArray: ArrayList<Route>?, shortestRouteIndex: Int) {
        val center = CameraUpdateFactory.newLatLng(senderLatLng)
        val zoom = CameraUpdateFactory.zoomTo(16f)
        if (polylines != null) {
            for (line in polylines!!) {
                line.remove()
            }
            polylines!!.clear()
        }
        val polyOptions = PolylineOptions()
        var polylineStartLatLng: LatLng? = null
        var polylineEndLatLng: LatLng? = null


        polylines = ArrayList()
        //add route(s) to the map using polyline
        //add route(s) to the map using polyline
        for (i in 0 until (routeArray?.size!!)) {
            if (i == shortestRouteIndex) {
                polyOptions.color(ResourcesCompat.getColor(resources, R.color.primary_color, null))
                polyOptions.width(7f)
                polyOptions.addAll(routeArray[shortestRouteIndex].points)
                val polyline: Polyline = googleMap.addPolyline(polyOptions)
                polylineStartLatLng = polyline.points[0]
                val k: Int = polyline.points.size
                polylineEndLatLng = polyline.points[k - 1]
                polylines?.add(polyline)
            } else {
            }
        }

        //Add Marker on route starting position

        //Add Marker on route starting position

        startMarker.position(polylineStartLatLng!!)
        startMarker.title("My Location")
        googleMap.addMarker(startMarker)

        //Add Marker on route ending position

        //Add Marker on route ending position

        endMarker.position(polylineEndLatLng!!)
        endMarker.title("Destination")
        googleMap.addMarker(endMarker)

    }

    override fun onRoutingCancelled() {
    }

    override fun onConnected(bundle: Bundle?) {
    }

    override fun onConnectionSuspended(status: Int) {
    }

}