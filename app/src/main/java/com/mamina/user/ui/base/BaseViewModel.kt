package com.mamina.user.ui.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
 * Created by Rohit Singh on 11,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    private var _showProgress = MutableLiveData<Boolean>()
    val showProgress: LiveData<Boolean>
    get() = _showProgress

    init {
        _showProgress.value = false
    }

    fun showProgress(){
        _showProgress.value= true
    }

    fun hideProgress() {
        _showProgress.value = false;
    }

    /*//    @Bindable
    fun getLoading(): Boolean? {
        return isLoading
    }

    fun setLoading(loading: Boolean?) {
        isLoading = loading
        //        notifyPropertyChanged(BR.loading);
    }*/ /*    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>();

    public void setLoading(Boolean isLoadingParam) {

//        isLoading.setValue(isLoadingParam);
        isLoading.postValue(isLoadingParam);
    }

    public MutableLiveData<Boolean> getLoading() {
        return isLoading;
    }*/
}