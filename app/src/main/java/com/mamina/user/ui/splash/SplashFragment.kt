package com.mamina.user.ui.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.mamina.user.MaMinaApplication
import com.mamina.user.R
import com.mamina.user.databinding.SplashFragmentBinding
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.utils.SharedPrefsHelper
import kotlinx.coroutines.*

class SplashFragment : BaseFragment() {
    private var _binding: SplashFragmentBinding? = null
    private val binding get() = _binding!!
    private var sharedPrefs: SharedPrefsHelper? = null
    private var DELAY_INTERVAL = 3000L

    override fun getRootView(): View? {
        return view
    }
    private var mViewModel: SplashViewModel? = null
    private var isUserSignIn: Boolean? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        _binding = SplashFragmentBinding.inflate(inflater, container, false)
        // initialising components
        val view = binding.root

        mViewModel = ViewModelProvider(this).get(SplashViewModel::class.java)
        // TODO: Use the ViewModel
        sharedPrefs = (context?.applicationContext as MaMinaApplication).getSharedPrefInstance()

        isUserSignIn = sharedPrefs?.builder()?.read(SharedPrefsHelper.KEY_IS_SIGN_IN, false)

        // IO,Main,Default
        CoroutineScope(Dispatchers.Default).launch {
            delay(DELAY_INTERVAL)
            withContext(Dispatchers.Main) {
                when (isUserSignIn) {
                    true -> {
                         lifecycleScope.launchWhenResumed {
                            findNavController().navigate(R.id.action_login_fragment_to_main_activity)
                        }
                    }
                        false -> {

                        lifecycleScope.launchWhenResumed {
                            findNavController().navigate(R.id.action_splash_fragment_to_login_fragment)
                        }
                    }
                }
            }
        }

        return view
    }

    companion object {
        fun newInstance(): SplashFragment {
            return SplashFragment()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}