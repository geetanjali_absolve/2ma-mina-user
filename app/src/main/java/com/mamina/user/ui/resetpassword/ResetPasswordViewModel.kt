package com.mamina.user.ui.resetpassword

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.mamina.WebService
import com.mamina.user.MaMinaApplication
import com.mamina.user.models.response.GeneralResponse
import com.mamina.user.models.response.WebResponse
import com.mamina.user.repository.remote.InitialRepository
import com.mamina.user.ui.base.BaseViewModel
import com.mamina.user.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class ResetPasswordViewModel(application: Application) : BaseViewModel(application) {
    var webService: WebService = (application as MaMinaApplication).getWebServiceInstance()
    var initialRepository: InitialRepository = InitialRepository(webService)
    var resetPasswordMLiveData: MutableLiveData<WebResponse<GeneralResponse>> = MutableLiveData()

    @ExperimentalCoroutinesApi
    fun resetPassword(otp: String?, email: String?, password: String?) {

        val json = JsonObject()
        json.addProperty("otp", otp)
        json.addProperty("emailPhone", email)
        json.addProperty("newPassword", password)

        viewModelScope.launch {
            initialRepository.resetPassword(json)
                    .onStart {

                    }.onCompletion {

                    }.catch { exception ->
                        resetPasswordMLiveData.value = WebResponse(Status.OTHER_ERROR, null, exception.message)

                    }.collect {
                        if (it.status)
                            resetPasswordMLiveData.value = WebResponse(Status.SUCCESS, it, null)
                        else
                            resetPasswordMLiveData.value = WebResponse(Status.SERVER_ERROR, null, it.message)
                    }
        }
    }

}