package com.mamina.user.ui.detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.mamina.WebService
import com.mamina.user.MaMinaApplication
import com.mamina.user.models.response.GeneralResponse
import com.mamina.user.models.response.GetRequestDetailsResponse
import com.mamina.user.models.response.WebResponse
import com.mamina.user.repository.remote.InitialRepository
import com.mamina.user.utils.ErrorHandler
import com.mamina.user.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class OrderDetailViewModel(application: Application) : AndroidViewModel(application) {
    private var initialRepository: InitialRepository
    private var webService: WebService
    var resultCancelParcelDelivery: MutableLiveData<WebResponse<GeneralResponse>>
    var resultParcelDeliveryDetails: MutableLiveData<WebResponse<GetRequestDetailsResponse>>
    var isLoading: MutableLiveData<Boolean>

    init {
        webService = (application as MaMinaApplication).getWebServiceInstance()
        initialRepository = InitialRepository(webService)
        resultCancelParcelDelivery = MutableLiveData()
        resultParcelDeliveryDetails = MutableLiveData()
        isLoading = MutableLiveData()
    }

    @ExperimentalCoroutinesApi
    fun getDeliveryRequestDetails(
            header: Map<String, String>,
            requestId: Int) {

        viewModelScope.launch {
            initialRepository.getRequestDetails(header, requestId)
                    .onStart {
                        /* emit loading state */
                        isLoading.value = true
                    }
                    .onCompletion {
                        isLoading.value = false
                    }
                    .catch { exception ->
                        /* emit error state */
                        val errorMsg: String? = ErrorHandler.reportError(exception)
                        resultParcelDeliveryDetails.value =
                                WebResponse(Status.SERVER_ERROR, null, errorMsg)
                    }
                    .collect {
                        if (it.status) resultParcelDeliveryDetails.value =
                                WebResponse(Status.SUCCESS, it, null)
                        else resultParcelDeliveryDetails.value =
                                WebResponse(Status.SERVER_ERROR, it, it.message)
                    }
        }
    }


    @ExperimentalCoroutinesApi
    fun cancelParcelDelivery(
            header: Map<String, String>,
            requestId: Int) {



        viewModelScope.launch {
            initialRepository.cancelOrderByUser(header, requestId)
                    .onStart {
                        /* emit loading state */
                        isLoading.value = true
                    }
                    .onCompletion {
                        isLoading.value = false
                    }
                    .catch { exception ->
                        /* emit error state */
                        val errorMsg: String? = ErrorHandler.reportError(exception)
                        resultCancelParcelDelivery.value =
                                WebResponse(Status.SERVER_ERROR, null, errorMsg)
                    }
                    .collect {
                        if (it.status) resultCancelParcelDelivery.value =
                                WebResponse(Status.SUCCESS, it, null)
                        else resultCancelParcelDelivery.value =
                                WebResponse(Status.SERVER_ERROR, it, it.message)
                    }
        }
    }

}