package com.mamina.user.ui.InviteFriends

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mamina.user.R

class InviteFriends : Fragment() {

    companion object {
        fun newInstance() = InviteFriends()
    }

    private lateinit var viewModel: InviteFriendsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.invite_friends_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(InviteFriendsViewModel::class.java)
        // TODO: Use the ViewModel
    }

}