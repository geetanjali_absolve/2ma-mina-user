package com.mamina.user.ui.base

import androidx.appcompat.app.AppCompatActivity

/**
 * Created by Rohit Singh on 24,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
open class BaseActivity : AppCompatActivity()