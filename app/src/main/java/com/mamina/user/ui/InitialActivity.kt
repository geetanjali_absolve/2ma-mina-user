package com.mamina.user.ui

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.navigation.fragment.NavHostFragment
import com.mamina.user.MaMinaApplication
import com.mamina.user.R
import com.mamina.user.ui.base.BaseActivity
import com.mamina.user.ui.splash.SplashFragment
import com.mamina.user.utils.SharedPrefsHelper

class InitialActivity : BaseActivity() {
    private var sharedPrefs: SharedPrefsHelper? = null
    var keyIdentifier: String=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_initial)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        sharedPrefs = (applicationContext as MaMinaApplication).getSharedPrefInstance()



        keyIdentifier = intent.getStringExtra("keyIdentifier").toString()
        Log.d(ContentValues.TAG, "onCreatekeyIdentifier: "+keyIdentifier)
        val navHost = supportFragmentManager.findFragmentById(R.id.initial_activity_container) as NavHostFragment?
        val navController = navHost!!.navController

        val navInflater = navController.navInflater
        val graph = navInflater.inflate(R.navigation.nav_logged_out)

        if (keyIdentifier == "logout") {
            graph.startDestination = R.id.login_fragment
        } else {
            graph.startDestination = R.id.splash_fragment
        }

        navController.graph = graph
}
}