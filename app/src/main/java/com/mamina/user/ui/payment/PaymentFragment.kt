package com.mamina.user.ui.payment

import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.braintreepayments.api.dropin.DropInActivity
import com.braintreepayments.api.dropin.DropInRequest
import com.braintreepayments.api.dropin.DropInResult
import com.braintreepayments.api.dropin.utils.PaymentMethodType
import com.mamina.user.R
import com.mamina.user.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_payment.*


class PaymentFragment : BaseFragment() {
    private var clientToken: String = "AXWyVsYc1hQnAhTa-gZc8mCaNoWbP7nhi6Yxvma4K9wOKh84a_xq4KdKrA1TpKnti6t7sy3i3bH-QJL-"
    private var REQUEST_CODE: Int = 111

    override fun getRootView(): View? {
        return fl_root_payment
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_pay.setOnClickListener {
            val dropInRequest = DropInRequest()
                    .tokenizationKey("sandbox_f252zhq7_hh4cpc39zq4rgjcg")
            startActivityForResult(dropInRequest.getIntent(requireContext()), REQUEST_CODE)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === REQUEST_CODE) {
            when {
                resultCode === RESULT_OK -> {
                    val result: DropInResult? = data?.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT)
                    // use the result to update your UI and send the payment method nonce to your server
                }
                resultCode === RESULT_CANCELED -> {
                    // the user canceled
                }
                else -> {
                    // handle errors here, an exception may be available in
                    val error = data?.getSerializableExtra(DropInActivity.EXTRA_ERROR) as Exception
                }
            }
        }
    }

    private fun getPaymentNonce(){
        DropInResult.fetchDropInResult(requireActivity(), clientToken, object : DropInResult.DropInResultListener {
            override fun onError(exception: Exception) {
                // an error occurred
            }

            override fun onResult(result: DropInResult) {
                if (result.getPaymentMethodType() != null) {
                    // use the icon and name to show in your UI
                    val icon = result.getPaymentMethodType()!!.getDrawable()
                    val name = result.getPaymentMethodType()!!.getLocalizedName()
                    val paymentMethodType = result.getPaymentMethodType()
                    if (paymentMethodType === PaymentMethodType.GOOGLE_PAYMENT) {
                        // The last payment method the user used was Google Pay.
                        // The Google Pay flow will need to be performed by the
                        // user again at the time of checkout.
                    } else {
                        // Use the payment method show in your UI and charge the user
                        // at the time of checkout.
                        val paymentMethod = result.getPaymentMethodNonce()
                        Log.i("TAG", "onResult:  $paymentMethod")
                    }
                } else {
                    // there was no existing payment method
                }
            }
        })
    }

}