package com.mamina.user.ui.login

import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.mamina.user.MaMinaApplication
import com.mamina.user.R
import com.mamina.user.databinding.LoginFragmentBinding
import com.mamina.user.models.response.LoginData
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.ui.utils.MessageType
import com.mamina.user.utils.CommonUtils
import com.mamina.user.utils.SharedPrefsHelper
import com.mamina.user.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi


class LoginFragment : BaseFragment() {
    private lateinit var loginViewModel: LoginViewModel

    private lateinit var loginToForgetAction: View.OnClickListener
    private lateinit var loginToChooseSignUpAction: View.OnClickListener

    private var _binding: LoginFragmentBinding? = null
    private val binding get() = _binding!!

    private var sharedPrefs: SharedPrefsHelper? = null

    // configure google sign in
    private val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
    private var googleSignInClient: GoogleSignInClient? = null
    private lateinit var auth: FirebaseAuth
    private var deviceToken: String? = null

    override fun onStart() {
        super.onStart()
        val account = GoogleSignIn.getLastSignedInAccount(requireActivity())
        if (account != null) {
            // user is logged in; can redirect the user to another page
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = LoginFragmentBinding.inflate(inflater, container, false)

        // initialising components
        val view = binding.root
        setLoadingView(binding.loginLoading.root)
        loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        sharedPrefs = (context?.applicationContext as MaMinaApplication).getSharedPrefInstance()
        auth = FirebaseAuth.getInstance()
        deviceToken = sharedPrefs?.builder()?.read(SharedPrefsHelper.KEY_DEVICE_ID, "")

        // defining navigation click listeners
        loginToForgetAction = Navigation.createNavigateOnClickListener(R.id.action_login_fragment_to_forget_fragment)
        loginToChooseSignUpAction = Navigation.createNavigateOnClickListener(R.id.action_login_fragment_to_choose_sign_up_fragment)
        googleSignInClient = GoogleSignIn.getClient(requireActivity(), gso)

        // on click listener
        binding.loginBtn.setOnClickListener {
            hideKeyboard(it)
            if (binding.loginEmailEditText.text.isEmpty()) {
                showSnackBar(MessageType.ERROR, resources.getString(R.string.email_empty), requireContext())
            } else if (binding.loginPasswordEditText.text.isEmpty()) {
                showSnackBar(MessageType.ERROR, resources.getString(R.string.password_empty), requireContext())
            } else if (!CommonUtils.isEmailValid(binding.loginEmailEditText.text.toString())) {
                showSnackBar(MessageType.ERROR, resources.getString(R.string.email_validation), requireContext())
            } else {
                initObserver(view, binding.loginEmailEditText.text.trim().toString(), binding.loginPasswordEditText.text.trim().toString())
            }
        }

        binding.loginGmailButton.setOnClickListener {
            val signInIntent = googleSignInClient?.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }

        // adding click listener
        binding.loginForgetPasswordTv.setOnClickListener(loginToForgetAction)
        binding.loginDontHaveAccount.setOnClickListener(loginToChooseSignUpAction)
        binding.loginSignUpTv.setOnClickListener(loginToChooseSignUpAction)

        /* end of click listeners */
        return view
    }

    @ExperimentalCoroutinesApi
    private fun initObserver(view: View, email: String, password: String) {
        showLoading()
        loginViewModel.loginResponseLiveData(email, password, "android", deviceToken.toString())
        loginViewModel.userMutableLiveData.observe(requireActivity(), { user ->
            when (user.status) {
                Status.SUCCESS -> {
                    hideLoading()
                    Log.i(TAG, "initObserver: " + user.data)
//                    if (user.data?.data?.applicationStatus == "Verified") {
                    saveUserInfo(user.data?.data)
                    Navigation.findNavController(view).navigate(R.id.action_login_fragment_to_main_activity)
//                    } else {
//                        showAlertDialog(requireContext(), user.data?.data?.appStatusMessage!!)
//                    }
                }
                Status.EMAIL_NOT_VERIFIED -> {
                    hideLoading()
                    saveUserInfo(user.data?.data)
                    val actionLoginFragmentToOtpFragment = LoginFragmentDirections.actionLoginFragmentToOtpFragment(user.data?.data?.email.toString())
                    actionLoginFragmentToOtpFragment.showEmailOtp = true
                    actionLoginFragmentToOtpFragment.emailMobile = user.data?.data?.email.toString()
                    findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToOtpFragment(user.data?.data?.email.toString()))
                  //  Navigation.findNavController(view).navigate(actionLoginFragmentToOtpFragment)
                }
                else -> {
                    hideLoading()
                    showSnackBar(MessageType.ERROR, "Error: " + user.errorMsg, requireContext())
                }
            }
        })

    }

    private fun saveUserInfo(data: LoginData?) {
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_IS_SIGN_IN, true)?.build()
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_FIRST_NAME, data?.firstName.toString())?.build()
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_PROFILE_PIC, data?.ProfilePic.toString())?.build()
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_LAST_NAME, data?.lastName.toString())?.build()
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_DIAL_CODE, data?.dialCode.toString())?.build()
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_PHONE, data?.phoneNumber.toString())?.build()
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_EMAIL, data?.email.toString())?.build()
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_USER_TYPE, data?.userTypeId.toString())?.build()
        sharedPrefs?.builder()?.write(SharedPrefsHelper.KEY_ACCESS_TOKEN, data?.accessToken.toString())?.build()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    val builder = AlertDialog.Builder(requireContext())
                    builder.setTitle("Confirm")
                    builder.setMessage("Are you sure to close app?")
                    builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                        requireActivity().finishAffinity()
                        System.exit(0)
                    }
                    builder.setNegativeButton(android.R.string.no) { dialog, which ->
                    }
                    builder.show()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }


    override fun getRootView(): View? {
        return view
    }

    companion object {
        private const val TAG = "LoginFragment"
        private const val RC_SIGN_IN = 8423
        fun newInstance() = LoginFragment()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // result from google's launch intent
        if (requestCode == RC_SIGN_IN && data != null) {

            if (resultCode == RESULT_OK) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    val account = task.getResult(ApiException::class.java)
                    Log.d(TAG, "firebaseAuthWithGoogle: onActivityResult: " + account.id)
                    Log.d(TAG, "firebaseAuthWithGoogle: Display Name: " + account.displayName)
                    Log.d(TAG, "firebaseAuthWithGoogle: Email: " + account.email)
                    Log.d(TAG, "firebaseAuthWithGoogle: Photo URL: " + account.photoUrl)
                    showSnackBar(MessageType.SUCCESS, "Your name is " + account.displayName, requireContext())

                } catch (e: ApiException) {
                    // Google Sign in failed; update UI
                    showSnackBar(MessageType.ERROR, "Google Sign In failed" + e.status.statusMessage, requireContext())
                }
            } else if (resultCode == RESULT_CANCELED) {
                showSnackBar(MessageType.ERROR, "Google Sign In result cancelled", requireContext())
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}