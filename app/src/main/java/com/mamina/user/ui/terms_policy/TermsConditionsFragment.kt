package com.mamina.user.ui.terms_policy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.lifecycle.ViewModelProvider
import com.mamina.user.MaMinaApplication
import com.mamina.user.R
import com.mamina.user.constants.ApiConstants
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.utils.SharedPrefsHelper
import kotlinx.android.synthetic.main.terms_conditions_fragment.*

class TermsConditionsFragment : BaseFragment() {
    private lateinit var viewModel: TermsConditionsViewModel
    val headerMap = HashMap<String, String>()
    private var sharedPrefs: SharedPrefsHelper? = null

    private var accessToken: String = ""
    private var viewType: String = ""

    override fun getRootView(): View? {
        return fl_root_terms
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(TermsConditionsViewModel::class.java)
        sharedPrefs = (context?.applicationContext as MaMinaApplication).getSharedPrefInstance()

        accessToken =
                sharedPrefs?.builder()?.read(SharedPrefsHelper.KEY_ACCESS_TOKEN, "").toString()
        headerMap[ApiConstants.KEY_AUTHORIZATION] = "Bearer $accessToken"

        viewType = TermsConditionsFragmentArgs.fromBundle(requireArguments()).viewType
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.terms_conditions_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setLoadingView(view_loading)

        initObserver()

        showLoading()
        if (viewType == "Terms") {
            info_title.text = getString(R.string.title_terms)
            viewModel.getTermsAndCondition(headerMap)
        } else {
            info_title.text = getString(R.string.title_privacy)
            viewModel.getPrivacyPolicy(headerMap)
        }
    }

    private fun initObserver() {
        viewModel.getInfoMLiveData.observe(viewLifecycleOwner, {
            hideLoading()
            textView.text = HtmlCompat.fromHtml(it, 0)
        })
    }


}