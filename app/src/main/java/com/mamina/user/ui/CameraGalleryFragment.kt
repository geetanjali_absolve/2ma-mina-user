package com.mamina.user.ui

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.camera.core.*
import androidx.camera.extensions.HdrImageCaptureExtender
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import com.mamina.user.R
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutionException
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class CameraGalleryFragment : Fragment() {
    private var mViewModel: CameraGalleryViewModel? = null
    private val REQUEST_CODE_PERMISSIONS = 1001
    private val REQUIRED_PERMISSIONS: Array<String> = arrayOf("android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE")
    private val executor: Executor = Executors.newSingleThreadExecutor()
    private var cameraPreviewView: PreviewView? = null
    private var imageView: ImageView? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.camera_gallery_fragment, container, false)
        cameraPreviewView = view.findViewById(R.id.camera)
        imageView = view.findViewById(R.id.capture_img_view)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel = ViewModelProvider(this).get(CameraGalleryViewModel::class.java)
        // TODO: Use the ViewModel
    }

    private fun startCamera(context: Context) {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(context)
        cameraProviderFuture.addListener({
            try {
                val cameraProvider = cameraProviderFuture.get()
                bindPreview(cameraProvider)
            } catch (e: ExecutionException) {
                // No errors need to be handled for this Future.
                // This should never be reached.
            } catch (e: InterruptedException) {
            }
        }, ContextCompat.getMainExecutor(activity))
    }

    fun bindPreview(cameraProvider: ProcessCameraProvider) {
        val preview = Preview.Builder()
                .build()
        val cameraSelector = CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build()
        val imageAnalysis = ImageAnalysis.Builder()
                .build()
        val builder = ImageCapture.Builder()

        //Vendor-Extensions (The CameraX extensions dependency in build.gradle)
        val hdrImageCaptureExtender = HdrImageCaptureExtender.create(builder)

        // Query if extension is available (optional).
        if (hdrImageCaptureExtender.isExtensionAvailable(cameraSelector)) {
            // Enable the extension if available.
            hdrImageCaptureExtender.enableExtension(cameraSelector)
        }
        val imageCapture = builder
                .setTargetRotation(requireActivity().getWindowManager().defaultDisplay.rotation)
                .build()
        preview.setSurfaceProvider(cameraPreviewView?.getSurfaceProvider())
        val camera = cameraProvider.bindToLifecycle(this as LifecycleOwner, cameraSelector, preview, imageAnalysis, imageCapture)
        imageView?.setOnClickListener(View.OnClickListener {
            val mDateFormat = SimpleDateFormat("yyyyMMddHHmmss", Locale.US)
            val file = File(activity?.getCacheDir()?.path, mDateFormat.format(Date()) + ".jpg") // getBatchDirectoryName()
            val outputFileOptions = ImageCapture.OutputFileOptions.Builder(file).build()
            imageCapture.takePicture(outputFileOptions, executor, object : ImageCapture.OnImageSavedCallback {
                override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                    Handler().post { Toast.makeText(activity, "Image Saved successfully", Toast.LENGTH_SHORT).show() }
                }

                override fun onError(error: ImageCaptureException) {
                    error.printStackTrace()
                }
            })
        })
    }

    companion object {
        fun newInstance(): CameraGalleryFragment {
            return CameraGalleryFragment()
        }
    }
}