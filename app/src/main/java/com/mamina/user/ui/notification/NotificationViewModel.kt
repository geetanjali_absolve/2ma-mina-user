package com.mamina.user.ui.notification

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mamina.WebService
import com.mamina.user.MaMinaApplication
import com.mamina.user.models.response.GeneralResponse
import com.mamina.user.models.response.GetNotificationData
import com.mamina.user.models.response.GetNotificationResponse
import com.mamina.user.models.response.WebResponse
import com.mamina.user.repository.remote.InitialRepository
import com.mamina.user.ui.base.BaseViewModel
import com.mamina.user.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class NotificationViewModel(application: Application) : BaseViewModel(application) {
    var webService: WebService = (application as MaMinaApplication).getWebServiceInstance()
    var initialRepository: InitialRepository = InitialRepository(webService)
    var itemToBeDeleted = 100000
    var getUserNotificationMLiveData: MutableLiveData<WebResponse<GetNotificationResponse>> =
        MutableLiveData()
    var listOfNotifications: MutableLiveData<MutableList<GetNotificationData>> = MutableLiveData()
    var isLoading: MutableLiveData<Boolean> = MutableLiveData()
    var resultDeleteNotification: MutableLiveData<WebResponse<GeneralResponse>> = MutableLiveData()
    var isListEmpty: MutableLiveData<Boolean> = MutableLiveData()

    @ExperimentalCoroutinesApi
    fun getUserNotifications(
        header: Map<String, String>,
    ) {
        viewModelScope.launch {
            initialRepository.getUserNotifications(
                header
            )
                .onStart {
                    isLoading.value = true
                }
                .onCompletion {
                    isLoading.value = false
                }
                .catch { exception ->
                    Log.i(TAG, "Exception:" + exception.message)
                    getUserNotificationMLiveData.value =
                        WebResponse(Status.NOT_FOUND, null, exception.message)
                }
                .collect {
                    Log.d(TAG, "getUserNotificationStatus: " + it.status)
                    if (it.status)
                        getUserNotificationMLiveData.value = WebResponse(Status.SUCCESS, it, null)
                    else
                        getUserNotificationMLiveData.value =
                            WebResponse(Status.SERVER_ERROR, null, it.message)
                }
        }
    }

    @ExperimentalCoroutinesApi
    fun deleteNotification(header: Map<String, String>, notificationId: String) {
        viewModelScope.launch {
            initialRepository.deleteNotification(header, notificationId)
                .onStart {
                    /* emit loading state */
                    isLoading.value = true
                }
                .onCompletion {
                    isLoading.value = false
                }
                .catch { exception ->
                    /* emit error state */
                    resultDeleteNotification.value =
                        WebResponse(Status.NOT_FOUND, null, exception.message)
                }
                .collect {
                    if (it.status) resultDeleteNotification.value =
                        WebResponse(Status.SUCCESS, it, null)
                    else resultDeleteNotification.value =
                        WebResponse(Status.SERVER_ERROR, it, it.message)
                }
        }
    }


    companion object {
        private val TAG = NotificationViewModel::class.java.simpleName
    }
}