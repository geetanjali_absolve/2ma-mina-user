package com.mamina.user.ui.ratings

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.auth0.android.jwt.JWT
import com.mamina.user.MaMinaApplication
import com.mamina.user.R
import com.mamina.user.constants.ApiConstants
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.utils.SharedPrefsHelper
import com.mamina.user.utils.Status
import kotlinx.android.synthetic.main.ratings_fragment.*

class RatingsFragment : BaseFragment() {
    private lateinit var viewModel: RatingsViewModel
    val headerMap = HashMap<String, String>()
    private var sharedPrefs: SharedPrefsHelper? = null
    private var accessToken: String = ""
    private var userId: String = ""
    private var orderId: String = ""
    private var driverId: String = ""
    private var ratingValue = 0.0F

    override fun getRootView(): View? {
        return fl_root_ratings
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(RatingsViewModel::class.java)
        sharedPrefs = (context?.applicationContext as MaMinaApplication).getSharedPrefInstance()

        accessToken =
                sharedPrefs?.builder()?.read(SharedPrefsHelper.KEY_ACCESS_TOKEN, "").toString()
        headerMap[ApiConstants.KEY_AUTHORIZATION] = "Bearer $accessToken"

        val jwt = JWT(accessToken)

        userId = jwt.getClaim("UserId").asString().toString()

        Log.i("TAG", "onCreate: $userId")

        orderId = RatingsFragmentArgs.fromBundle(requireArguments()).orderId
        driverId = RatingsFragmentArgs.fromBundle(requireArguments()).driverId

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.ratings_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setLoadingView(ratings_loading)

        initObserver()

        ratingBar.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            ratingValue = rating
        }

        btn_ratings.setOnClickListener {
            if (ratingValue == 0.0F || TextUtils.isEmpty(edt_comment.text.toString())) {
                showSnackbarError("Please fill all the required fields!")
            } else {
                showLoading()
                viewModel.rateDelivery(headerMap, userId, driverId.toInt(), orderId.toInt(), edt_comment.text.toString(),
                        ratingValue.toInt())
            }
        }

    }

    private fun initObserver(){
        viewModel.resultChangePasswordMLiveData.observe(viewLifecycleOwner,{
            when (it.status) {
                Status.SUCCESS -> {
                    Log.i("Ratings Fragment", "Response:${it.data}")
                    showAlertDialog(requireContext(), it.data?.message!!)
                }
                Status.AUTHORISATION -> showSnackbarError(it.errorMsg.toString())
                Status.AUTHORISATION_ERROR -> showSnackbarError(it.errorMsg.toString())
                Status.BAD_REQUEST -> showSnackbarError(it.errorMsg.toString())
                Status.NOT_FOUND -> showSnackbarError(it.errorMsg.toString())
                Status.OTHER_ERROR -> showSnackbarError(it.errorMsg.toString())
                Status.SERVER_ERROR -> showSnackbarError(it.errorMsg.toString())
                else -> showSnackbarError(it.errorMsg.toString())
            }

        })
    }


}