package com.mamina.user.ui.profile

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mamina.WebService
import com.mamina.user.MaMinaApplication
import com.mamina.user.models.response.GeneralResponse
import com.mamina.user.models.response.GetIndividualUserDetailsResponse
import com.mamina.user.models.response.WebResponse
import com.mamina.user.repository.remote.InitialRepository
import com.mamina.user.ui.base.BaseViewModel
import com.mamina.user.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody

class ProfileViewModel(application: Application) : BaseViewModel(application) {
    var webService: WebService = (application as MaMinaApplication).getWebServiceInstance()
    var initialRepository: InitialRepository = InitialRepository(webService)
    var updateIndividualProfileMLiveData: MutableLiveData<WebResponse<GeneralResponse>> =
            MutableLiveData()
    var getIndividualDetailsMLiveData: MutableLiveData<WebResponse<GetIndividualUserDetailsResponse>> =
            MutableLiveData()


    @ExperimentalCoroutinesApi
    fun getIndividualUserProfile(header: Map<String, String>, userId: String) {
        viewModelScope.launch {
            initialRepository.getIndividualUserProfile(header, userId)
                    .onStart {

                    }.onCompletion {

                    }.catch { exception ->
                        getIndividualDetailsMLiveData.value =
                                WebResponse(Status.OTHER_ERROR, null, exception.message)
                    }.collect {
                        if (it.status)
                            getIndividualDetailsMLiveData.value = WebResponse(Status.SUCCESS, it, null)
                        else
                            getIndividualDetailsMLiveData.value =
                                    WebResponse(Status.SERVER_ERROR, null, it.message)
                    }
        }
    }

    @ExperimentalCoroutinesApi
    fun updateIndividualProfile(header: Map<String, String>,
                                firstName: RequestBody,
                                lastName: RequestBody,
                                dial: RequestBody,
                                phone: RequestBody,
                                email: RequestBody,
                                dob: RequestBody,
                                user: RequestBody,
                                idProofType: RequestBody,
                                idProof: MultipartBody.Part,
                                profile: MultipartBody.Part,
                                idCardBack: MultipartBody.Part?) {
        viewModelScope.launch {
            initialRepository.updateIndividualUserProfile(
                    header,
                    firstName,
                    lastName,
                    dial,
                    phone,
                    email,
                    dob,
                    user,
                    idProofType,
                    idProof,
                    profile,
                    idCardBack)
                    .onStart {
//                        isLoading.value = true
//                        isViewEnable.value = false
                    }
                    .onCompletion {
//                        isLoading.value = false
//                        isViewEnable.value = true
                    }
                    .catch { exception ->
//                        / emit error state /
                        Log.i(TAG, "Exception:" + exception.message)
                        updateIndividualProfileMLiveData.value = WebResponse(Status.OTHER_ERROR, null, exception.message)
                    }
                    .collect {
                        Log.i(TAG, "signupIndividually: $it")
                        updateIndividualProfileMLiveData.value = WebResponse(it.code, it, it.message)//WebResponse(it.code, it.data, it.message)
                    }
        }
    }

    companion object {
        private val TAG = ProfileViewModel::class.java.simpleName
    }
}