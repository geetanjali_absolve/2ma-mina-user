package com.mamina.user.ui.forget

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.mamina.WebService
import com.mamina.user.MaMinaApplication
import com.mamina.user.models.response.ForgotPasswordResponse
import com.mamina.user.models.response.WebResponse
import com.mamina.user.repository.remote.InitialRepository
import com.mamina.user.ui.base.BaseViewModel
import com.mamina.user.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class ForgetViewModel(application: Application) : BaseViewModel(application) {
    var webService: WebService = (application as MaMinaApplication).getWebServiceInstance()
    var initialRepository: InitialRepository = InitialRepository(webService)
    var forgotPasswordMLiveData: MutableLiveData<WebResponse<ForgotPasswordResponse>> = MutableLiveData()

    @ExperimentalCoroutinesApi
    fun forgotPassword(email: String?) {

        val json = JsonObject()
        json.addProperty("emailPhone", email)

        viewModelScope.launch {
            initialRepository.forgotPassword(json)
                    .onStart {

                    }.onCompletion {

                    }.catch { exception ->
                        forgotPasswordMLiveData.value = WebResponse(Status.OTHER_ERROR, null, exception.message)

                    }.collect {
                        if(it.status)
                            forgotPasswordMLiveData.value= WebResponse(Status.SUCCESS, it, null)
                        else
                            forgotPasswordMLiveData.value= WebResponse(Status.SERVER_ERROR, null, it.message)
                    }
        }
    }

}