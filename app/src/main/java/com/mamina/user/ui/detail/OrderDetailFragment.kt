package com.mamina.user.ui.detail

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.mamina.user.MaMinaApplication
import com.mamina.user.R
import com.mamina.user.constants.ApiConstants
import com.mamina.user.databinding.FragmentOrderDetailBinding
import com.mamina.user.ui.base.BaseFragment
import com.mamina.user.ui.utils.MessageType
import com.mamina.user.utils.SharedPrefsHelper
import com.mamina.user.utils.Status
import kotlinx.android.synthetic.main.fragment_order_detail.*


class OrderDetailFragment : BaseFragment() {
    private var sharedPrefs: SharedPrefsHelper? = null
    private lateinit var orderDetailViewModel: OrderDetailViewModel
    private var _binding: FragmentOrderDetailBinding? = null
    private val binding get() = _binding!!
    private var mViewPagerAdapter: ViewPagerAdapter? = null

    override fun getRootView(): View? {
        return parent_order_detail
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sharedPrefs = (context.applicationContext as MaMinaApplication).getSharedPrefInstance()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentOrderDetailBinding.inflate(inflater, container, false)
        val view = binding.root
        setLoadingView(binding.orderDetailsLoading.root)
        orderDetailViewModel = ViewModelProvider(this).get(OrderDetailViewModel::class.java)
        val accessToken = sharedPrefs?.builder()?.read(SharedPrefsHelper.KEY_ACCESS_TOKEN, "").toString()
        val headerMap = HashMap<String, String>()
        headerMap[ApiConstants.KEY_AUTHORIZATION] = "Bearer $accessToken"
//        headerMap["Authorization"] = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOiI5MmM5ZmFkZi1hM2MzLTRiZWMtYjFjZC0yZjdiNjVmNTc4YzEiLCJEZXZpY2VUb2tlbiI6IjEyMzQ1Njc4IiwibmJmIjoxNjE2NDcxODYyLCJleHAiOjE2MTY1NTgyNjIsImlhdCI6MTYxNjQ3MTg2Mn0.aGF9L5cwGtbCwjdLwRQsgYcyDQoPNJbur-V44aph_F8"

        initObserver(view)

        val bookingsModel = OrderDetailFragmentArgs.fromBundle(requireArguments()).bookingInfoList
        var imagesArray: ArrayList<String> = ArrayList()
        imagesArray.add(bookingsModel.parcelImgBefore)
        imagesArray.add(bookingsModel.parcelImgAfter)

        mViewPagerAdapter = ViewPagerAdapter(requireContext(), imagesArray)
        binding.viewPager.adapter = mViewPagerAdapter

        orderDetailViewModel.getDeliveryRequestDetails(headerMap, bookingsModel.request.requestId)

        binding.tvParcelName.text = bookingsModel.parcelName
        binding.tvParcelNotes.text = bookingsModel.parcelNotes
        binding.pDetailTypeValueTv.text = bookingsModel.deliveryType
        binding.tvReceiverName.text = bookingsModel.request.receiverName
        binding.tvReceiverPhone.text = bookingsModel.request.receiverMobileNumber
        binding.tvReceiverAddress.text = bookingsModel.request.receiverAddress
        binding.tvOrderPrice.text = "R ${bookingsModel.price}"

        binding.btnCancel.setOnClickListener {

            Log.d(TAG, "requestId: "+bookingsModel.request.requestId)
            orderDetailViewModel.cancelParcelDelivery(headerMap, bookingsModel.request.requestId)
        }

        return view
    }

    private fun initObserver(view: View) {
        orderDetailViewModel.isLoading.observe(viewLifecycleOwner,
                Observer {
                    if (it) showLoading()
                    else hideLoading()
                })

        orderDetailViewModel.resultParcelDeliveryDetails.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    Log.i("OrderDetailFragment", "initObserver: " + it.data?.data)
                    binding.tvDriverName.text = it.data?.data?.driverInfo?.name
                    binding.tvDriverPhone.text = it.data?.data?.driverInfo?.phoneNumber
                    binding.tvModel.text = it.data?.data?.driverInfo?.vehicleBrand
                    binding.tvColor.text = it.data?.data?.driverInfo?.vehicleColor
                    binding.tvNumber.text = it.data?.data?.driverInfo?.registrationNumber

                    when (it.data?.data?.listDeliveryDetails?.last()?.statusId) {
                        3 -> {
                            binding.orderStatusLine1.animate()
                                    .translationY(210.0F)
                                    .setDuration(1500)
                                    .setListener(object : AnimatorListenerAdapter() {
                                        override fun onAnimationEnd(animation: Animator?) {
                                            super.onAnimationEnd(animation)
                                            binding.viewOrderConfirmed.setBackgroundResource(R.drawable.shape_status_completed)
                                        }
                                    })
                        }
                        4 -> {
                            binding.orderStatusLine2.animate()
                                    .translationY(210.0F)
                                    .setDuration(1500)
                                    .setListener(object : AnimatorListenerAdapter() {
                                        override fun onAnimationEnd(animation: Animator?) {
                                            super.onAnimationEnd(animation)
                                            binding.viewOrderProcessed.setBackgroundResource(R.drawable.shape_status_completed)
                                        }
                                    })
                        }
                        5 -> {
                            binding.orderStatusLine3.animate()
                                    .translationY(210.0F)
                                    .setDuration(1500)
                                    .setListener(object : AnimatorListenerAdapter() {
                                        override fun onAnimationEnd(animation: Animator?) {
                                            super.onAnimationEnd(animation)
                                            binding.viewOrderArrived.setBackgroundResource(R.drawable.shape_status_completed)
                                        }
                                    })
                        }
                        6 -> {
                            binding.orderStatusLine4.animate()
                                    .translationY(210.0F)
                                    .setDuration(1500)
                                    .setListener(object : AnimatorListenerAdapter() {
                                        override fun onAnimationEnd(animation: Animator?) {
                                            super.onAnimationEnd(animation)
                                            binding.viewOrderDelivered.setBackgroundResource(R.drawable.shape_status_completed)
                                        }
                                    })
                        }
                    }
                }
                Status.SERVER_ERROR -> {
                    if (it.errorMsg == "Unauthorised User") {
                        showUnauthorizedUserErrorDialog()
                    } else {
                        showSnackBar(MessageType.ERROR, it.errorMsg.toString(), requireContext())
                    }
                }
            }
        })

        orderDetailViewModel.resultCancelParcelDelivery.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    basicAlert(view,it.data?.message!!)
                }
                Status.SERVER_ERROR -> {
                    if (it.errorMsg == "Unauthorised User") {
                        showUnauthorizedUserErrorDialog()
                    } else {
                        showSnackBar(MessageType.ERROR, it.errorMsg.toString(), requireContext())
                    }
                }
            }
        })

    }

    fun basicAlert(view: View, message: String) {
        val dialogBuilder = AlertDialog.Builder(requireContext())
        dialogBuilder.setMessage("" + message)
            .setCancelable(false)
            .setPositiveButton("ok") { dialog, id ->
                Navigation.findNavController(view).navigateUp();
             //   findNavController().popBackStack()

            }
        val alert = dialogBuilder.create()

        alert.show()


    }


}