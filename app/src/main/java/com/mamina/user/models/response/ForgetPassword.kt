package com.mamina.user.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Rohit Singh on 15,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
class ForgetPassword {
    @SerializedName("otp")
    @Expose
    private var otp: Int? = null
}