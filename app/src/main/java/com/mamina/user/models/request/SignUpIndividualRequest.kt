package com.mamina.user.models.request

/**
 * Created by Rohit Singh on 09,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
data class SignUpIndividualRequest(
        val FirstName: String?,
        val LastName: String?,
        val DialCode: String?,
        val PhoneNumber: String?,
        val Email: String?,
        val Password: String?,
        val UserTypeId: Int = 1,
        val DeviceType: String?,
        val DeviceToken: String?
)