package com.mamina.user.models.response

import com.google.gson.annotations.SerializedName

data class SignUpBusinessUserResponse (
        @SerializedName("status") var status: Boolean,

        @SerializedName("error") var error: String? = null,

        @SerializedName("data") var data: SignUpIndividualData,

        @SerializedName("message") var message: String,

        @SerializedName("docuploadmessage") var docuploadmessage: String,

        @SerializedName("code") var code: Int
)