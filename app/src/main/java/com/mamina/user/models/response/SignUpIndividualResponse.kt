package com.mamina.user.models.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Rohit Singh on 09,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
data class SignUpIndividualResponse(
        @SerializedName("status") var status: Boolean,

        @SerializedName("error") var error: String? = null,

        @SerializedName("data") var data: SignUpIndividualData,

        @SerializedName("message") var message: String,

        @SerializedName("code") var code: Int
)