package com.mamina.user.models.local

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.parcel.Parcelize


/**
 * Created by Rohit Singh on 18,March,2021
 * Absolve Tech company,
 * Mohali, India.
 */

// this class is used to pass the data from home fragment to parcel detail fragment
@Parcelize
class ParcelInfoData(val senderCoordinate: LatLng, val receiverCoordinate: LatLng, val categoryId: String) : Parcelable