package com.mamina.user.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Rohit Singh on 09,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
class LoginData {
    @SerializedName("firstName")
    @Expose
    var firstName: String? = null

    @SerializedName("lastName")
    @Expose
    var lastName: String? = null

    @SerializedName("dialCode")
    @Expose
    var dialCode: String? = null

    @SerializedName("phoneNumber")
    @Expose
    var phoneNumber: String? = null

    @SerializedName("isPhoneNoVerified")
    @Expose
    var isPhoneNoVerified: Boolean? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("isEmailConfirmed")
    @Expose
    var isEmailConfirmed: Boolean? = null

    @SerializedName("userTypeId")
    @Expose
    var userTypeId: Int? = null

    @SerializedName("accessToken")
    @Expose
    var accessToken: String? = null

    @SerializedName("countryCode")
    @Expose
    var countryCode: String? = null

    @SerializedName("profilePic")
    @Expose
    var ProfilePic: String? = null

    @SerializedName("applicationStatusId")
    @Expose
    var applicationStatusId: Int? = null

    @SerializedName("applicationStatus")
    @Expose
    var applicationStatus: String? = null

    @SerializedName("appStatusMessage")
    @Expose
    var appStatusMessage: String? = null
}