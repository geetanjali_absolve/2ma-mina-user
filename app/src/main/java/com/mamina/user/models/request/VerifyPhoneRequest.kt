package com.mamina.user.models.request

/**
 * Created by Rohit Singh on 09,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
class VerifyPhoneRequest(private val code: String?)