package com.mamina.user.models.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetNotificationData (
    @SerializedName("createdOn") var createdOn: String,
    @SerializedName("id") var id: String,
    @SerializedName("requestId") var requestId: Int,
    @SerializedName("text") var text: String,
    @SerializedName("toDriverId") var toDriverId: Int,
    @SerializedName("toUserId") var toUserId: String,
    @SerializedName("type") var type: String,
    @SerializedName("typeId") var typeId: Int,
): Parcelable