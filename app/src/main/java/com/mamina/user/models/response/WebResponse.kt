package com.mamina.user.models.response

/**
 * Created by Rohit Singh on 16,March,2021
 * Absolve Tech company,
 * Mohali, India.
 */
class WebResponse<T>(status: Int, data: T?, errorMsg: String?) {
    var status: Int
    var data: T?
    var errorMsg: String? = null

    init {
        this.status = status
        this.data = data
        this.errorMsg = errorMsg
    }
}