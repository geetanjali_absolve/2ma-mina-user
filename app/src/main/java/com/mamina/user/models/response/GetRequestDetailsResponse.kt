package com.mamina.user.models.response

data class GetRequestDetailsResponse(
        var code: Int,
        var message: String,
        var error: String,
        var status: Boolean,
        var data: Data
) {
    data class Data(
            var driverInfo: DriverInfoData,
            var listDeliveryDetails: MutableList<GetRequestData>
    )
}