package com.mamina.user.models.response

data class DistancePrice(
    var deliveryType: String,
    var description: String,
    var distance: String,
    var duration: String,
    var price: Int
)