package com.mamina.user.models.response

data class PriceDetail(
    var code: Int,
    var data: List<DeliveryData>,
    var message: String,
    var status: Boolean
)