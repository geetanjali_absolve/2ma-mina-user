package com.mamina.user.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Rohit Singh on 09,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
class SignUpIndividualData {
    @SerializedName("firstName")
    @Expose
    val firstName: String? = null

    @SerializedName("lastName")
    @Expose
    val lastName: String? = null

    @SerializedName("dialCode")
    @Expose
    val dialCode: String? = null

    @SerializedName("phoneNumber")
    @Expose
    val phoneNumber: String? = null

    @SerializedName("isPhoneNoVerified")
    @Expose
    val isPhoneNoVerified: Boolean? = null

    @SerializedName("email")
    @Expose
    val email: String? = null

    @SerializedName("isEmailConfirmed")
    @Expose
    val isEmailConfirmed: Boolean? = null

    @SerializedName("userTypeId")
    @Expose
    val userTypeId: Int? = null

    @SerializedName("accessToken")
    @Expose
    val accessToken: String? = null

    @SerializedName("idProof")
    @Expose
    val idProof: String? = null

    @SerializedName("iDCardBack")
    @Expose
    val idCardBack: String? = null

    @SerializedName("screenId")
    @Expose
    val screenId: Int? = null

    @SerializedName("loginType")
    @Expose
    val loginType: Any? = null

    @SerializedName("profilePic")
    @Expose
    val profilePic: String? = null

    @SerializedName("agreementFilePath")
    @Expose
    val agreementFilePath: String? = null

    @SerializedName("chamberCommerceFilePath")
    @Expose
    val chamberCommerceFilePath: String? = null

    @SerializedName("contactPerson")
    @Expose
    val contactPerson: String? = null

    @SerializedName("externalNumber")
    @Expose
    val externalNumber: String? = null

    @SerializedName("licenceFilePath")
    @Expose
    val licenceFilePath: String? = null

    @SerializedName("licenceNumber")
    @Expose
    val licenceNumber: String? = null

    @SerializedName("vat")
    @Expose
    val vat: String? = null

    @SerializedName("vatFilePath")
    @Expose
    val vatFilePath: String? = null

    @SerializedName("website")
    @Expose
    val website: String? = null
}