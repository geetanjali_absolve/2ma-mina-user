package com.mamina.user.models.response

import com.google.gson.annotations.SerializedName

data class DeliveryRequestData (
        @SerializedName("requestId") var requestId: Int,
        @SerializedName("senderId") var senderId: String,
        @SerializedName("senderAddress") var senderAddress: String,
        @SerializedName("senderPlaceId") var senderPlaceId: String,
        @SerializedName("senderLat") var senderLat: Double,
        @SerializedName("senderLong") var senderLong: Double,
        @SerializedName("receiverName") var receiverName: String,
        @SerializedName("receiverAddress") var receiverAddress: String,
        @SerializedName("dialCode") var dialCode: Int,
        @SerializedName("receiverMobileNumber") var receiverMobileNumber: String,
        @SerializedName("receiverLat") var receiverLat: Double,
        @SerializedName("receiverLong") var receiverLong: Double,
        @SerializedName("receiverPlaceId") var receiverPlaceId: String,
        @SerializedName("deliveryTypeId") var deliveryTypeId: Int,
        @SerializedName("totalDeliveryTime") var totalDeliveryTime: String,
        @SerializedName("totalDeliveryDistance") var totalDeliveryDistance: String,
)