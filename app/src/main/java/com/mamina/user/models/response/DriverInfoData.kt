package com.mamina.user.models.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DriverInfoData (
        @SerializedName("name") var name: String,
        @SerializedName("phoneNumber") var phoneNumber: String,
        @SerializedName("vehicleBrand") var vehicleBrand: String,
        @SerializedName("vehicleColor") var vehicleColor: String,
        @SerializedName("registrationNumber") var registrationNumber: String
): Parcelable