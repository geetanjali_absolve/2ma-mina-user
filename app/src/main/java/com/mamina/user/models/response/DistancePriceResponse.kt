package com.mamina.user.models.response

data class DistancePriceResponse(
        var code: Int,
        var data: DistancePrice,
        var message: String,
        var status: Boolean
)