package com.mamina.user.models.request

/**
 * Created by Rohit Singh on 15,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
class ForgetPasswordRequest(private var emailPhone: String?) {
    fun getEmailPhone(): String? {
        return emailPhone
    }

    fun setEmailPhone(value: String?) {
        emailPhone = value
    }
}