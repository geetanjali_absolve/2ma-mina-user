package com.mamina.user.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Rohit Singh on 15,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
class ForgetPasswordResponse {
    @SerializedName("status")
    @Expose
    var status: Boolean? = null

    @SerializedName("data")
    @Expose
    var forgetPasswordData: ForgetPassword? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("code")
    @Expose
    var code: Int? = null

}