package com.mamina.user.models.response

import com.google.gson.annotations.SerializedName

data class DeliveryRequestResponse(
        @SerializedName("status") var status: Boolean,

        @SerializedName("error") var error: String? = null,

        @SerializedName("data") var data: DeliveryRequestData,

        @SerializedName("message") var message: String,

        @SerializedName("code") var code: Int
)