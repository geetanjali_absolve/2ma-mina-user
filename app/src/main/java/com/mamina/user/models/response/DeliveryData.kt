package com.mamina.user.models.response

data class DeliveryData(
    var deliveryType: String,
    var description: String,
    var price: Int,
    var typeId: Int
)