package com.mamina.user.models.request

data class OrderRequest(
        var RequestId: String = "0",
        var deliveryPrice: Int = 0,
        var description: String = "",
        var ParcelName: String = "",
        var ParcelNotes: String = "",
        var ImgBeforePacking: String = "",
        var ImgAfterPacking: String = "",
        var SenderLat: String = "",
        var SenderLong: String = "",
        var SenderPlaceId: String = "",
        var SenderAddress: String = "",
        var ReceiverName: String = "",
        var ReceiverEmail: String = "",
        var ReceiverPlaceId: String = "",
        var ReceiverAddress: String = "",
        var DialCode: String = "",
        var ReceiverMobileNumber: String = "",
        var ReceiverLat: String = "",
        var ReceiverLong: String = "",
        var DeliveryTypeId: Int = 0,
        var TotalDeliveryTime: String = "",
        var TotalDeliveryDistance: String = ""
)