package com.mamina.user.models.response

data class GetNotificationResponse(
        var code: Int,
        var message: String,
        var error: String,
        var status: Boolean,
        var data: MutableList<GetNotificationData>,
)