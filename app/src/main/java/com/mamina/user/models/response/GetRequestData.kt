package com.mamina.user.models.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetRequestData(
        @SerializedName("driverId") var driverId: Int,
        @SerializedName("requestId") var requestId: Int,
        @SerializedName("statusId") var statusId: Int,
        @SerializedName("requestStatus") var requestStatus: String,
        @SerializedName("driverLat") var driverLat: Double,
        @SerializedName("driverLong") var driverLong: Double,
        @SerializedName("statusUpdateDate") var statusUpdateDate: String,
        @SerializedName("statusUpdateTime") var statusUpdateTime: String,
        @SerializedName("deliveryDateTimeUTC") var deliveryDateTimeUTC: String
): Parcelable