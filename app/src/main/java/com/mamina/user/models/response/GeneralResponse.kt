package com.mamina.user.models.response

/**
 * Created by Rohit Singh on 16,March,2021
 * Absolve Tech company,
 * Mohali, India.
 */
data class GeneralResponse(
        val code: Int,
        val data: Data,
        val message: String,
        val error: String,
        val status: Boolean
) {
    class Data()
}