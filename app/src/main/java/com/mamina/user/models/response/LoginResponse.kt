package com.mamina.user.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Rohit Singh on 09,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */

class LoginResponse {
    @SerializedName("status")
    @Expose
    var status: Boolean? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("error")
    @Expose
    var error: String? = null

    @SerializedName("data")
    @Expose
    var data: LoginData? = null

    @SerializedName("code")
    @Expose
    var code: Int? = null
}