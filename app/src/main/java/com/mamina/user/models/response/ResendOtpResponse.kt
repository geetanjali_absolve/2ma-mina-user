package com.mamina.user.models.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Rohit Singh on 09,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
data class ResendOtpResponse(
        @SerializedName("status") var status: Boolean,

        @SerializedName("data") var data: Data,

        @SerializedName("message") var message: String,

        @SerializedName("code") var code: Int
) {
    data class Data(val otpcode: Int)
}

