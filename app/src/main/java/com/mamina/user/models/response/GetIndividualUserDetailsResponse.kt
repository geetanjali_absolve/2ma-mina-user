package com.mamina.user.models.response

data class GetIndividualUserDetailsResponse(
    var code: Int,
    var message: String,
    var error: String,
    var status: Boolean,
    var data: Data,
) {
    data class Data(
        var firstName: String,
        var lastName: String,
        var dialCode: String,
        var phoneNumber: String,
        var profilePic: String,
        var dob: String,
        var email: String,
        var isPhoneNoVerified: Boolean,
        var isEmailConfirmed: Boolean,
        var idTypeId: Int,
        var applicationStatus: Int,
        var idTypeValue: String,
        var idProof: String,
        var idCardBack: String,
    )

}