package com.mamina.user.models.response

import com.google.gson.annotations.SerializedName

data class ForgotPasswordResponse(
        @SerializedName("status") var status: Boolean,

        @SerializedName("data") var data: Data,

        @SerializedName("message") var message: String,

        @SerializedName("code") var code: Int
) {
    data class Data(val otp: Int)
}