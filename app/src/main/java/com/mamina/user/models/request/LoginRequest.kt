package com.mamina.user.models.request

/**
 * Created by Rohit Singh on 09,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
data class LoginRequest(private var emailPhone: String, private var password: String, private var deviceType: String, private var deviceToken: String)