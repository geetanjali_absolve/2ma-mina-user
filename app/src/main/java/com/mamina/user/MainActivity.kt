package com.mamina.user

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.bumptech.glide.Glide
import com.google.android.material.navigation.NavigationView
import com.mamina.user.constants.ApiConstants
import com.mamina.user.models.request.OrderRequest
import com.mamina.user.ui.InitialActivity
import com.mamina.user.ui.base.BaseActivity
import com.mamina.user.utils.SharedPrefsHelper
import de.hdodenhof.circleimageview.CircleImageView


class MainActivity : BaseActivity() {
    lateinit var orderRequest: OrderRequest
    private var locationPermissionGranted: Boolean? = false
    private lateinit var mAppBarConfiguration: AppBarConfiguration
    private var sharedPrefs: SharedPrefsHelper? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sharedPrefs = (applicationContext as MaMinaApplication).getSharedPrefInstance()

        val toolbar = findViewById<Toolbar?>(R.id.toolbar)
        setSupportActionBar(toolbar)
        orderRequest = OrderRequest()
        val drawer = findViewById<DrawerLayout?>(R.id.drawer_layout)
        val navigationView = findViewById<NavigationView?>(R.id.nav_view)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_booking, R.id.nav_completed, R.id.nav_profile, R.id.nav_settings, R.id.nav_notification,
                R.id.nav_logout)
                .setOpenableLayout(drawer)
                .build()

        navigationView.menu.findItem(R.id.nav_logout)
            .setOnMenuItemClickListener {
                logout()
                drawer.close()
                true
            }


        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration)
        NavigationUI.setupWithNavController(navigationView, navController)

        val headerView: View = navigationView.getHeaderView(0)
        val navUsername = headerView.findViewById(R.id.tvName) as TextView
        val navDP =headerView.findViewById(R.id.ivDp) as CircleImageView
        navUsername.text = ""+sharedPrefs?.read(SharedPrefsHelper.KEY_FIRST_NAME, "").toString()+" "+sharedPrefs?.read(SharedPrefsHelper.KEY_LAST_NAME, "").toString()
        Glide.with(this)
            .load(ApiConstants.BASE_FILE + sharedPrefs?.read(SharedPrefsHelper.KEY_PROFILE_PIC, "").toString())
            .centerInside()
            .into(navDP)

        getLocationPermission()
    }

    private fun logout() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Alert")
        builder.setMessage("Are you sure to logout?")

        builder.setPositiveButton(android.R.string.yes) { dialog, which ->

            sharedPrefs!!.clear()
            val intent = Intent(this, InitialActivity::class.java)
            intent.putExtra("keyIdentifier", "logout")
            startActivity(intent)

        }

        builder.setNegativeButton(android.R.string.no) { dialog, which ->

        }

        builder.show()
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        return (NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp())
    }

    fun getLocationPermission() {
        val permissions = arrayOf<String?>(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION)
        if (ContextCompat.checkSelfPermission(this.applicationContext, FINE_LOCATION.toString()) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.applicationContext, COARSE_LOCATION.toString()) == PackageManager.PERMISSION_GRANTED) {
                locationPermissionGranted = true
            } else {
                ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST_CODE)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationPermissionGranted = false
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty()) {
                var i = 0
                while (i < grantResults.size) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        locationPermissionGranted = false
                        return
                    }
                    i++
                }
                locationPermissionGranted = true
            }
        }
    }




    companion object {
        private val TAG = MainActivity::class.java.simpleName
        private val FINE_LOCATION: String? = Manifest.permission.ACCESS_FINE_LOCATION
        private val COARSE_LOCATION: String? = Manifest.permission.ACCESS_COARSE_LOCATION
        private const val LOCATION_PERMISSION_REQUEST_CODE = 612
    }


}