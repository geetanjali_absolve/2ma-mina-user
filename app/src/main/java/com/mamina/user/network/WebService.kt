package com.mamina

import com.google.gson.JsonObject
import com.mamina.user.constants.ApiConstants
import com.mamina.user.models.request.LoginRequest
import com.mamina.user.models.response.*
import com.mamina.user.ui.bookings.model.BookingsResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface WebService {

    @Headers("content-type:application/json")
    @POST(ApiConstants.LOGIN_URL)
    suspend fun login(
//            @HeaderMap token: Map<String, String>,
            @Body input: LoginRequest
    ): LoginResponse

    @Headers("content-type:application/json")
    @GET(ApiConstants.GET_PRICE_DETAIL)
    suspend fun priceDetails(
            @HeaderMap token: Map<String, String>,
    ): PriceDetail


    @Headers("content-type:application/json")
    @GET(ApiConstants.REQUEST_DISTANCE_PRICE)
    suspend fun requestDistancePrice(
            @HeaderMap token: Map<String, String>,
            @Query("oLat") senderLat: String,
            @Query("oLon") senderLong: String,
            @Query("dLat") receiverLat: String,
            @Query("dLon") receiverLong: String,
            @Query("deliveryTypeId") deliveryTypeId: Int): DistancePriceResponse

    @Headers("content-type:application/json")
    @GET(ApiConstants.GET_REQUEST_DETAILS)
    suspend fun getRequestDetails(
            @HeaderMap token: Map<String, String>,
            @Query("requestId") requestId: Int): GetRequestDetailsResponse

    // verify phone number
    @Headers("content-type:application/json")
    @POST(ApiConstants.VERIFY_PHONE)
    suspend fun verifyPhone(
            @HeaderMap token: Map<String, String>,
            @Body inn: JsonObject
    ): VerifyPhoneResponse

    @Headers("content-type:application/json")
    @POST(ApiConstants.VERIFY_EMAIL)
    suspend fun verifyEmail(
            @HeaderMap token: Map<String, String>,
            @Body inn: JsonObject
    ): VerifyPhoneResponse

    // otp verification
    @Headers("content-type:application/json")
    @POST(ApiConstants.RESEND_PHONE_OTP)
    suspend fun resendOtpPhone(
            @HeaderMap token: Map<String, String>,
            @Body inn: JsonObject
    ): ResendOtpResponse

    @Headers("content-type:application/json")
    @POST(ApiConstants.RESEND_EMAIL_OTP)
    suspend fun resendOtpEmail(
            @HeaderMap token: Map<String, String>,
            @Body inn: JsonObject
    ): ResendOtpResponse

    @Headers("content-type:application/json")
    @POST(ApiConstants.FORGET_PASSWORD)
    suspend fun forgotPassword(
            @Body inn: JsonObject
    ): ForgotPasswordResponse

    @Headers("content-type:application/json")
    @POST(ApiConstants.RESET_PASSWORD)
    suspend fun resetPassword(
            @Body inn: JsonObject
    ): GeneralResponse

    @Multipart
    @POST(ApiConstants.INDIVIDUAL_SIGN_UP)
    suspend fun signUpUser(
            @Part("FirstName") FirstName: RequestBody,
            @Part("LastName") LastName: RequestBody,
            @Part("DialCode") DialCode: RequestBody,
            @Part("PhoneNumber") PhoneNumber: RequestBody,
            @Part("Email") Email: RequestBody,
            @Part("DOB") DOB: RequestBody,
            @Part("Password") Password: RequestBody,
            @Part("UserTypeId") UserTypeId: RequestBody,
            @Part("DeviceType") DeviceType: RequestBody,
            @Part("DeviceToken") DeviceToken: RequestBody,
            @Part("IDProofTypeId") IDProofTypeId: RequestBody,
            @Part idProof: MultipartBody.Part,
            @Part idCardBack: MultipartBody.Part?
    ): SignUpIndividualResponse

    @Multipart
    @POST(ApiConstants.BUSINESS_SIGN_UP)
    suspend fun signUpBusiness(
            @Part("FirstName") FirstName: RequestBody,
            @Part("LastName") LastName: RequestBody,
            @Part("DialCode") DialCode: RequestBody,
            @Part("PhoneNumber") PhoneNumber: RequestBody,
            @Part("Email") Email: RequestBody,
            @Part("Password") Password: RequestBody,
            @Part("Website") Website: RequestBody,
            @Part("ContactPerson") ContactPerson: RequestBody,
            @Part("LicenceNumber") LicenceNumber: RequestBody,
            @Part("VAT") VAT: RequestBody,
            @Part("ExternalNumber") ExternalNumber: RequestBody,
            @Part("UserTypeId") UserTypeId: RequestBody,
            @Part("DeviceType") DeviceType: RequestBody,
            @Part("DeviceToken") DeviceToken: RequestBody,
            @Part license: MultipartBody.Part,
            @Part vat: MultipartBody.Part,
            @Part commerce: MultipartBody.Part,
            @Part agreement: MultipartBody.Part
    ): SignUpBusinessUserResponse

    @Headers("content-type:application/json")
    @POST(ApiConstants.REQUEST_DELIVERY)
    suspend fun requestCreate(
            @HeaderMap token: Map<String, String>,
            @Body inn: JsonObject,
    ): DeliveryRequestResponse

    @Multipart
    @POST(ApiConstants.REQUEST_PARCEL)
    suspend fun requestDelivery(
            @HeaderMap token: Map<String, String>,
            @Part("RequestId") requestId: RequestBody,
            @Part("ParcelName") parcelName: RequestBody,
            @Part("ParcelNotes") parcelNotes: RequestBody,
            @Part imgBefore: MultipartBody.Part,
            @Part imgAfter: MultipartBody.Part
    ): GeneralResponse

    @Multipart
    @POST(ApiConstants.REQUEST_PARCEL_DELIVERY)
    suspend fun requestDelivery(
            @HeaderMap token: Map<String, String>,
            @Part("RequestId") requestId: RequestBody,
            @Part("ParcelName") parcelName: RequestBody,
            @Part("ParcelNotes") parcelNotes: RequestBody,
            @Part("SenderLat") senderLat: RequestBody,
            @Part("SenderLong") senderLong: RequestBody,
            @Part("SenderPlaceId") senderPlaceId: RequestBody,
            @Part("SenderAddress") senderAddress: RequestBody,
            @Part("ReceiverName") receiverName: RequestBody,
            @Part("ReceiverEmail") receiverEmail: RequestBody,
            @Part("ReceiverPlaceId") receiverPlaceId: RequestBody,
            @Part("ReceiverAddress") receiverAddress: RequestBody,
            @Part("DialCode") dialCode: RequestBody,
            @Part("ReceiverMobileNumber") phone: RequestBody,
            @Part("ReceiverLat") receiverLat: RequestBody,
            @Part("ReceiverLong") receiverLong: RequestBody,
            @Part("DeliveryTypeId") deliveryType: RequestBody,
            @Part("TotalDeliveryTime") deliveryTime: RequestBody,
            @Part("TotalDeliveryDistance") totalDistance: RequestBody,
            @Part imgBefore: MultipartBody.Part,
            @Part imgAfter: MultipartBody.Part
    ): DeliveryRequestResponse

    @GET(ApiConstants.GET_USER_ORDERS)
    fun fetchUserOrders(
            @HeaderMap token: Map<String, String>,
            @Query("requestStatusId") requestStatusId: Int,
            @Query("pageNumber") pageNo: Int,
            @Query("pageSize") pageSize: Int = 10
    ): Call<BookingsResponse>

    @Headers("content-type:application/json")
    @POST(ApiConstants.CANCEL_USER_ORDER)
    suspend fun cancelUserOrder(
        @HeaderMap token: Map<String, String>,
        @Query("requestId") driverId: Int): GeneralResponse



    //get driver notification
    @Headers("content-type:application/json")
    @GET(ApiConstants.GET_USER_NOTIFICATION)
    suspend fun getUserNotification(
            @HeaderMap token: Map<String, String>): GetNotificationResponse

    // delete driver notification
    @DELETE(ApiConstants.DELETE_USER_NOTIFICATION)
    suspend fun deleteNotifications(
            @HeaderMap token: Map<String, String>,
            @Query("notifId") notificationId: String
    ): GeneralResponse

    @Headers("content-type:application/json")
    @GET(ApiConstants.GET_INDIVIDUAL_PROFILE)
    suspend fun getIndividualUserProfile(
            @HeaderMap token: Map<String, String>,
            @Query("userId") requestId: String): GetIndividualUserDetailsResponse

    @Multipart
    @POST(ApiConstants.UPDATE_INDIVIDUAL_PROFILE)
    suspend fun updateIndividualProfile(
            @HeaderMap token: Map<String, String>,
            @Part("FirstName") FirstName: RequestBody,
            @Part("LastName") LastName: RequestBody,
            @Part("DialCode") DialCode: RequestBody,
            @Part("PhoneNumber") PhoneNumber: RequestBody,
            @Part("Email") Email: RequestBody,
            @Part("DOB") DOB: RequestBody,
            @Part("UserTypeId") UserTypeId: RequestBody,
            @Part("IDProofTypeId") IDProofTypeId: RequestBody,
            @Part idProof: MultipartBody.Part,
            @Part profile: MultipartBody.Part,
            @Part idCardBack: MultipartBody.Part?
    ): GeneralResponse

    //change password details
    @Headers("content-type:application/json")
    @POST(ApiConstants.CHANGE_PASSWORD)
    suspend fun changePassword(
            @HeaderMap token: Map<String, String>,
            @Body inn: JsonObject
    ): GeneralResponse

    //give delivery ratings
    @Headers("content-type:application/json")
    @POST(ApiConstants.RATE_DELIVERY)
    suspend fun rateDriver(
            @HeaderMap token: Map<String, String>,
            @Body inn: JsonObject
    ): GeneralResponse

    //get privacy policy
    @Headers("content-type:application/json")
    @GET(ApiConstants.SITE_POLICY)
    fun getSitePolicy(
            @HeaderMap token: Map<String, String>): Call<ResponseBody>

    // get terms conditions
    @Headers("content-type:application/json")
    @GET(ApiConstants.TERMS_CONDITIONS)
    fun getTermsConditions(
            @HeaderMap token: Map<String, String>): Call<ResponseBody>

}