package com.mamina.user.utils

class Constants {
    companion object{
        val normal = "Normal"
        val express = "Express"
        val bakkie = "Bakkie"
        val withoutPacking = "withoutPacking"
        val withPacking = "withPacking"
        val REQUEST_STORAGE = 1000
        var CHOOSE_IMG = "Choose Image"
        var GALLERY = "Gallery"
        var CAMERA = "Camera"
        var CANCEL = "Cancel"
        var IMAGE = "Image"
    }
}