package com.mamina.user.utils

/**
 * Created by Rohit Singh on 16,March,2021
 * Absolve Tech company,
 * Mohali, India.
 */
object Status {
    const val SUCCESS = 200
    const val BAD_REQUEST = 400
    const val AUTHORISATION = 401
    const val AUTHORISATION_ERROR = 403
    const val NOT_FOUND = 404
    const val EMAIL_NOT_VERIFIED = 412
    const val SERVER_ERROR = 500
    const val OTHER_ERROR = 999
}