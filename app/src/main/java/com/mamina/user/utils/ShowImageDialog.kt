package com.mamina.user.utils

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ShowImageDialog(
    val activity: Activity
) {

    companion object {
        val GALLERY = 2
        val CAMERA = 3
        var imageFile: File? = null
    }

    var imagePathList = ArrayList<String>()

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun handlePictureDialogWithPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity.packageManager.checkPermission(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    activity.packageName
                )
                != PackageManager.PERMISSION_GRANTED ||
                activity.packageManager.checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, activity.packageName)
                != PackageManager.PERMISSION_GRANTED
                ||
                activity.packageManager.checkPermission(Manifest.permission.CAMERA, activity.packageName)
                != PackageManager.PERMISSION_GRANTED
            ) {
                activity.requestPermissions(
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ), Constants.REQUEST_STORAGE
                )
            } else {
                showPictureDialog()
            }

        } else {
            showPictureDialog()
        }
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun showPictureDialog() {
        val pictureDialog = AlertDialog.Builder(activity)
        pictureDialog.setTitle(Constants.CHOOSE_IMG)
        val pictureDialogItems = arrayOf<String>(Constants.CAMERA, Constants.GALLERY, Constants.CANCEL)
        pictureDialog.setItems(
            pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> takePhotoFromCamera(activity)
                1 -> choosePhotoFromGallary(activity)
                2 -> Dialog(pictureDialog)
            }
        }
        pictureDialog.show()
    }


    fun choosePhotoFromGallary(activity: Activity) {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        activity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY)

    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun Dialog(dialog: AlertDialog.Builder) {
        dialog.setOnDismissListener(object : DialogInterface.OnDismissListener {
            override fun onDismiss(dialog: DialogInterface) {
                dialog.dismiss()
            }
        })
    }

    fun takePhotoFromCamera(activity: Activity) {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (cameraIntent.resolveActivity(activity.packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile(activity)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            if (photoFile != null) {
                val photoURI = FileProvider.getUriForFile(activity, "com.mamina.user.fileprovider", photoFile)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                activity.startActivityForResult(cameraIntent, CAMERA)
            }

        }

    }

    @Throws(IOException::class)
    fun createImageFile(activity: Activity): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName: String = "JPEG_" + timeStamp + "_"
        val storageDir: File? = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if (!storageDir!!.exists()) storageDir.mkdirs()
        val imageFile = File.createTempFile(imageFileName, ".jpg", storageDir)
        imageFilePath = imageFile.absolutePath
        return imageFile
    }

    lateinit var imageFilePath: String

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        when (requestCode) {
            Constants.REQUEST_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
                ) {

                    showPictureDialog()
                    //call your action
                } else {
                    Toast.makeText(activity, "permission denied", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?,imageView: ImageView) {

        when (requestCode) {

            GALLERY -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        if (data!!.clipData != null) {
                            var mClipData = data.clipData
                            val count =
                                data.clipData!!.itemCount //evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
                            for (i in 0..count - 1) {
                                var imageUri = data.clipData!!.getItemAt(i).uri

                                var str = CompressImage(activity).compressImage(imageUri.toString())
                                imagePathList.add(str)
                                imageView.setImageURI(imageUri)
                            }
                        } else {
                            var mImageUri = data.data;
                            var str = CompressImage(activity).compressImage(data.data.toString())
                            imagePathList.add(str)
                            imageView.setImageURI(mImageUri)
                        }
                    }
                }
            }
            CAMERA -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        var dd = imageFilePath
                        val file = File(dd)
                        val compressionRatio = 30

                        var out: FileOutputStream? = null
                        val filename = getFilename()
                        try {
                            out = FileOutputStream(filename)
                            var bitmap = BitmapFactory.decodeFile(file.path)
                            //          write the compressed bitmap at the destination specified by filename.
                            bitmap = rotateImages(imageFilePath, bitmap)
                            if (bitmap != null) {
                                bitmap.compress(Bitmap.CompressFormat.JPEG, compressionRatio, out)
                                imagePathList.add(filename)
                                imageView.setImageBitmap(bitmap)
                            }
                        } catch (e: FileNotFoundException) {
                            e.printStackTrace()
                        }
                    }
                }

            }
        }
    }

    fun getFilename(): String {
        val file = File(Environment.getExternalStorageDirectory().path, "MyFolder/Images")
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.absolutePath + "/" + System.currentTimeMillis() + ".jpg"

    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }

    fun rotateImages(file: String, bitmap: Bitmap): Bitmap? {

        val ei = ExifInterface(file)
        val orientation = ei.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_UNDEFINED
        )
        var rotatedBitmap: Bitmap? = null
        when (orientation) {

            ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap = rotateImage(bitmap, 90F)

            ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap = rotateImage(bitmap, 180F)

            ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap = rotateImage(bitmap, 270F)

            ExifInterface.ORIENTATION_NORMAL -> bitmap
            else -> rotatedBitmap = bitmap
        }
        return rotatedBitmap
    }

}


