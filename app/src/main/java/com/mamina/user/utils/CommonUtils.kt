package com.mamina.user.utils

import android.app.Activity
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File


/**
 * Created by Rohit Singh on 11,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
object CommonUtils {
    // email validation
    fun isEmailValid(email: String?): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }



    fun getRealPathFromURI(contentURI: Uri, context: Context): String {
        val result: String
        val cursor: Cursor? = context.contentResolver.query(contentURI, null, null, null, null)
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.path.toString()
        } else {
            cursor.moveToFirst()
            val idx: Int = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }

    fun prepareImagePart(filie: String, name: String): MultipartBody.Part {
        val file = File(filie)
        val requestFile = file.asRequestBody("image/jpg".toMediaTypeOrNull())
        return MultipartBody.Part.Companion.createFormData(name, file.name, requestFile)
    }



    fun prepareFilePart(filie: String, name: String): MultipartBody.Part {
        val file = File(filie)
        val requestFile = file.asRequestBody("application/pdf".toMediaTypeOrNull())
        return MultipartBody.Part.Companion.createFormData(name, file.name, requestFile)
    }

    fun prepareTextPart(name: String): RequestBody {
        return name.toRequestBody("text/plain".toMediaTypeOrNull())
    }
}