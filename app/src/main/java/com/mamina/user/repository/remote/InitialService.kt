package com.mamina.user.repository.remote

import com.mamina.user.constants.ApiConstants
import com.mamina.user.models.request.ForgetPasswordRequest
import com.mamina.user.models.request.LoginRequest
import com.mamina.user.models.request.ResendOtpRequest
import com.mamina.user.models.request.VerifyPhoneRequest
import com.mamina.user.models.response.ForgetPasswordResponse
import com.mamina.user.models.response.LoginResponse
import com.mamina.user.models.response.ResendOtpResponse
import com.mamina.user.models.response.VerifyPhoneResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Rohit Singh on 09,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
interface InitialService {
    // login
    @Headers("Content-Type: application/json")
    @POST(ApiConstants.LOGIN_URL)
    fun login(@Body loginRequest: LoginRequest): Call<LoginResponse>

    // sign up for individual user
    @Multipart
    @POST(ApiConstants.INDIVIDUAL_SIGN_UP)
    fun signUpIndividually(@Part("FirstName") FirstName: RequestBody,
                           @Part("LastName") LastName: RequestBody,
                           @Part("DialCode") DialCode: RequestBody,
                           @Part("PhoneNumber") PhoneNumber: RequestBody,
                           @Part("Email") Email: RequestBody,
                           @Part("Password") Password: RequestBody,
                           @Part("UserTypeId") UserTypeId: RequestBody,
                           @Part("DeviceType") DeviceType: RequestBody,
                           @Part("DeviceToken") DeviceToken: RequestBody,
                           @Part SelfiePic: MultipartBody.Part,
                           @Part IDProof: MultipartBody.Part):
            Call<ResponseBody>

    // sign up for business
    /*@Multipart
    @POST(ApiConstants.BUSINESS_SIGN_UP)
    Call<Sign>*/
    // verify phone number
    @POST(ApiConstants.VERIFY_PHONE)
    fun verifyPhone(@Body verifyPhoneRequest: VerifyPhoneRequest): Call<VerifyPhoneResponse>

    // otp verification
    @POST(ApiConstants.RESEND_PHONE_OTP)
    fun resendOtp(@Body resendOtpRequest: ResendOtpRequest): Call<ResendOtpResponse>

    // forget password
    @POST(ApiConstants.FORGET_PASSWORD)
    fun forgetPassword(@Body forgetPasswordRequest: ForgetPasswordRequest): Call<ForgetPasswordResponse>
}