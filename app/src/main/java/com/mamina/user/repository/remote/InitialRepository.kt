package com.mamina.user.repository.remote

import com.google.gson.JsonObject
import com.mamina.WebService
import com.mamina.user.models.request.LoginRequest
import com.mamina.user.models.response.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.MultipartBody
import okhttp3.RequestBody


/**
 * Created by Rohit Singh on 09,February,2021
 * Absolve Tech company,
 * Mohali, India.
 */
class InitialRepository(val webService: WebService) {


    @ExperimentalCoroutinesApi
    suspend fun requestParcelDelivery(
            header: Map<String, String>,
            requestId: RequestBody,
            parcelName: RequestBody,
            parcelNotes: RequestBody,
            senderLat: RequestBody,
            senderLong: RequestBody,
            senderPlaceId: RequestBody,
            senderAddress: RequestBody,
            receiverName: RequestBody,
            receiverEmail: RequestBody,
            receiverPlaceId: RequestBody,
            receiverAddress: RequestBody,
            dialCode: RequestBody,
            phone: RequestBody,
            receiverLat: RequestBody,
            receiverLong: RequestBody,
            deliveryType: RequestBody,
            deliveryTime: RequestBody,
            distance: RequestBody,
            imgBefore: MultipartBody.Part,
            imgAfter: MultipartBody.Part
    ): Flow<DeliveryRequestResponse> {
        return flow {
            val result =
                    webService.requestDelivery(header, requestId, parcelName, parcelNotes, senderLat, senderLong, senderPlaceId, senderAddress, receiverName,
                            receiverEmail, receiverPlaceId, receiverAddress, dialCode, phone, receiverLat, receiverLong, deliveryType,
                            deliveryTime, distance, imgBefore, imgAfter
                    )
            emit(result)
        }.flowOn(Dispatchers.IO)
    }


    @ExperimentalCoroutinesApi
    suspend fun signUp(
            firstName: RequestBody,
            lastName: RequestBody,
            dial: RequestBody,
            phone: RequestBody,
            email: RequestBody,
            dob: RequestBody,
            pass: RequestBody,
            user: RequestBody,
            deviceType: RequestBody,
            deviceToken: RequestBody,
            idProofType: RequestBody,
            idProof: MultipartBody.Part,
            idCardBack: MultipartBody.Part?
    ): Flow<SignUpIndividualResponse> {
        return flow {
            val result =
                    webService.signUpUser(
                            firstName,
                            lastName,
                            dial,
                            phone,
                            email,
                            dob,
                            pass,
                            user,
                            deviceType,
                            deviceToken,
                            idProofType,
                            idProof,
                            idCardBack)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun signUpBusiness(
            firstName: RequestBody,
            lastName: RequestBody,
            dial: RequestBody,
            phone: RequestBody,
            email: RequestBody,
            pass: RequestBody,
            website: RequestBody,
            contactPerson: RequestBody,
            licenseNo: RequestBody,
            vat: RequestBody,
            externalNumber: RequestBody,
            user: RequestBody,
            deviceType: RequestBody,
            deviceToken: RequestBody,
            licenseFile: MultipartBody.Part,
            vatFile: MultipartBody.Part,
            commerceFile: MultipartBody.Part,
            agreementFile: MultipartBody.Part
    ): Flow<SignUpBusinessUserResponse> {
        return flow {
            val result =
                    webService.signUpBusiness(
                            firstName,
                            lastName,
                            dial,
                            phone,
                            email,
                            pass,
                            website,
                            contactPerson,
                            licenseNo,
                            vat,
                            externalNumber,
                            user,
                            deviceType,
                            deviceToken,
                            licenseFile,
                            vatFile,
                            commerceFile,
                            agreementFile)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }


    @ExperimentalCoroutinesApi
    suspend fun login(
            loginRequest: LoginRequest
    ): Flow<LoginResponse> {
        return flow {
            val result = webService.login(loginRequest)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun getPriceDetail(
            header: Map<String, String>
    ): Flow<PriceDetail> {
        return flow {
            val result = webService.priceDetails(header)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun getLocationPrice(
            header: Map<String, String>,
            senderLat: String,
            senderLong: String,
            recLat: String,
            recLong: String,
            deliveryTypeId: Int,
    ): Flow<DistancePriceResponse> {
        return flow {
            val result = webService.requestDistancePrice(header, senderLat, senderLong, recLat, recLong, deliveryTypeId)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun getRequestDetails(
            header: Map<String, String>,
            requestId: Int,
    ): Flow<GetRequestDetailsResponse> {
        return flow {
            val result = webService.getRequestDetails(header, requestId)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun resendOtpPhone(header: Map<String, String>, jsonObject: JsonObject)
            : Flow<ResendOtpResponse> {
        return flow {
            val result = webService.resendOtpPhone(header, jsonObject)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun resendOtpEmail(header: Map<String, String>, jsonObject: JsonObject)
            : Flow<ResendOtpResponse> {
        return flow {
            val result = webService.resendOtpEmail(header, jsonObject)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun verifyPhone(header: Map<String, String>, jsonObject: JsonObject)
            : Flow<VerifyPhoneResponse> {
        return flow {
            val result = webService.verifyPhone(header, jsonObject)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun verifyEmail(header: Map<String, String>, jsonObject: JsonObject)
            : Flow<VerifyPhoneResponse> {
        return flow {
            val result = webService.verifyEmail(header, jsonObject)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun forgotPassword(jsonObject: JsonObject)
            : Flow<ForgotPasswordResponse> {
        return flow {
            val result = webService.forgotPassword(jsonObject)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun resetPassword(jsonObject: JsonObject)
            : Flow<GeneralResponse> {
        return flow {
            val result = webService.resetPassword(jsonObject)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun requestDelivery(
            header: Map<String, String>,
            jsonObject: JsonObject
    ): Flow<DeliveryRequestResponse> {
        return flow {
            val result =
                    webService.requestCreate(header, jsonObject)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun requestParcel(
            header: Map<String, String>,
            requestId: RequestBody,
            parcelName: RequestBody,
            parcelNotes: RequestBody,
            imgBefore: MultipartBody.Part,
            imgAfter: MultipartBody.Part
    ): Flow<GeneralResponse> {
        return flow {
            val result =
                    webService.requestDelivery(header, requestId, parcelName, parcelNotes, imgBefore, imgAfter)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun cancelOrderByUser(
            header: Map<String, String>,
            requestId: Int,
    ): Flow<GeneralResponse> {
        return flow {
            val result = webService.cancelUserOrder(header, requestId)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun getUserNotifications(
            header: Map<String, String>,
    ): Flow<GetNotificationResponse> {
        return flow {
            val result =
                    webService.getUserNotification(
                            header)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun deleteNotification(
            token: Map<String, String>, notifId: String,
    ): Flow<GeneralResponse> {
        return flow {
            emit(webService.deleteNotifications(token, notifId))
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun getIndividualUserProfile(
            header: Map<String, String>,
            userId: String,
    ): Flow<GetIndividualUserDetailsResponse> {
        return flow {
            val result = webService.getIndividualUserProfile(header, userId)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun updateIndividualUserProfile(
            header: Map<String, String>,
            firstName: RequestBody,
            lastName: RequestBody,
            dial: RequestBody,
            phone: RequestBody,
            email: RequestBody,
            user: RequestBody,
            dob: RequestBody,
            idProofType: RequestBody,
            idProof: MultipartBody.Part,
            profile: MultipartBody.Part,
            idCardBack: MultipartBody.Part?
    ): Flow<GeneralResponse> {
        return flow {
            val result =
                    webService.updateIndividualProfile(
                            header,
                            firstName,
                            lastName,
                            dial,
                            phone,
                            email,
                            dob,
                            user,
                            idProofType,
                            idProof,
                            profile,
                            idCardBack)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun changePassword(
            header: Map<String, String>,
            jsonObject: JsonObject,
    ): Flow<GeneralResponse> {
        return flow {
            val result =
                    webService.changePassword(
                            header,
                            jsonObject)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    @ExperimentalCoroutinesApi
    suspend fun rateDelivery(
            header: Map<String, String>,
            jsonObject: JsonObject,
    ): Flow<GeneralResponse> {
        return flow {
            val result =
                    webService.rateDriver(
                            header,
                            jsonObject)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

}